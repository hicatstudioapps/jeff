package com.dao;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

import java.io.IOException;

public class Main {

    public static void main(String [] arg){
        Schema schema= new Schema(1,"es.mrjeff.app.android.jeff.dao");
        schema.enableKeepSectionsByDefault();
        Entity entity= schema.addEntity("JeffCache");
        entity.addIdProperty();
        entity.addStringProperty("products");
        entity.addStringProperty("order");
        entity.addStringProperty("jeff_data");

        try {
            DaoGenerator daoGenerator= new DaoGenerator();
            daoGenerator.generateAll(schema,arg[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
