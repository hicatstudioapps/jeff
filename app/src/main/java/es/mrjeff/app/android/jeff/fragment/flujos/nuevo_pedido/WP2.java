package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

import es.mrjeff.app.android.jeff.fragment.NuevoPedidoDatosGenerales;
import me.panavtec.wizard.WizardPage;

/**
 * Created by CQ on 13/10/2016.
 */

public class WP2 extends WizardPage<Fragment> {
    @Override
    public Fragment createFragment() {
        return new NuevoPedidoDatosGenerales();
    }

    @Override
    public void setupActionBar(ActionBar supportActionBar) {
        super.setupActionBar(supportActionBar);
    }
}
