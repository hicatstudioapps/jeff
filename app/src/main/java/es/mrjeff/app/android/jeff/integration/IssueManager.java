package es.mrjeff.app.android.jeff.integration;

import android.content.Context;

import java.io.IOException;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.model.issues.IssueSend;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by CQ on 05/12/2016.
 */

public class IssueManager {

    public static int registerIncident(String stageId, IssueSend issueSend,String autorization){

        Call<ResponseBody> call= JeffAplication.apiCalls.registerIssue(stageId,issueSend,autorization);
        try {
            Response<ResponseBody> responseBody= call.execute();
            if(responseBody.code()==200)
                return 1;
                else return -1;
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
