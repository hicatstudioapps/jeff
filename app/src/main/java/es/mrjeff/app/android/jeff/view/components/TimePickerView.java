package es.mrjeff.app.android.jeff.view.components;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.adapter.time_picker.TimePickerDateAdapter;
import es.mrjeff.app.android.jeff.model.time_table.DateTimeTable;
import es.mrjeff.app.android.jeff.model.time_table.Timetable;

/**
 * Created by CQ on 10/11/2016.
 */

public class TimePickerView extends RelativeLayout implements TimePickerDateAdapter.SelectedDataDate{

    List<Timetable> dateTimeTable;
    @BindView(R.id.rv_date)
    RecyclerView rv_date;
    @BindView(R.id.rv_hour)
    RecyclerView rv_hour;
    Context context;
    String dateSelected;

    public int getMin_day() {
        return min_day;
    }

    int min_day;

    public String getHourSelected() {
        return hourSelected;
    }

    String hourSelected;

    public String getDateSelected() {
        return dateSelected;
    }

    public List<Timetable> getDateTimeTable() {
        return dateTimeTable;
    }

    public void setDateTimeTable(List<Timetable> dateTimeTable) {
        this.dateTimeTable = dateTimeTable;
    }

    public RecyclerView getRv_date() {
        return rv_date;
    }

    public void setRv_date(RecyclerView rv_date) {
        this.rv_date = rv_date;
    }

    public RecyclerView getRv_hour() {
        return rv_hour;
    }

    public void setRv_hour(RecyclerView rv_hour) {
        this.rv_hour = rv_hour;
    }

    public TimePickerDateAdapter getDateTimePickerDateAdapter() {
        return dateTimePickerDateAdapter;
    }

    public void setDateTimePickerDateAdapter(TimePickerDateAdapter dateTimePickerDateAdapter) {
        this.dateTimePickerDateAdapter = dateTimePickerDateAdapter;
    }

    private TimePickerDateAdapter dateTimePickerDateAdapter;

    public TimePickerView(Context context) {
        super(context);
        this.context= context;
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.time_picker,this,false);
        addView(view);
        ButterKnife.bind(this);
    }

    public TimePickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context= context;
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.time_picker,this,false);
        addView(view);
        ButterKnife.bind(this);
        rv_date.setLayoutManager(new LinearLayoutManager(context));
        rv_hour.setLayoutManager(new LinearLayoutManager(context));
    }

    public TimePickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context= context;
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.time_picker,this,false);
        addView(view);
        ButterKnife.bind(this);
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    boolean type= false;

    public TimePickerView initData(List<Timetable> dateTimeTable, boolean type){
        this.dateTimeTable=dateTimeTable;
        this.type=type;
        dateTimePickerDateAdapter=  new TimePickerDateAdapter(dateTimeTable,rv_hour,context,type,this);
        dateTimePickerDateAdapter.setType(type);
        rv_date.setAdapter(dateTimePickerDateAdapter);
            dateSelected= dateTimeTable.get(0).getDay();
            hourSelected= dateTimeTable.get(0).getPickupTimetable().split(";")[0];
            min_day= Integer.parseInt(dateTimeTable.get(0).getMinTiming());
        return this;
    }


    @Override
    public void onDateSelected(String data) {
        dateSelected=data;
    }

    @Override
    public void onHourSelected(String data) {
        hourSelected=data;
    }

    @Override
    public void onMinDay(int day) {
        min_day=day;
    }
}
