
package es.mrjeff.app.android.jeff.model.nuevo.customer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer_ {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("last_order_id")
    @Expose
    private Object lastOrderId;
    @SerializedName("last_order_date")
    @Expose
    private Object lastOrderDate;
    @SerializedName("orders_count")
    @Expose
    private Integer ordersCount;
    @SerializedName("total_spent")
    @Expose
    private String totalSpent;
    @SerializedName("avatar_url")
    @Expose
    private String avatarUrl;
    @SerializedName("billing_address")
    @Expose
    private es.mrjeff.app.android.jeff.model.nuevo.order.BillingAddress billingAddress;
    @SerializedName("shipping_address")
    @Expose
    private es.mrjeff.app.android.jeff.model.nuevo.order.ShippingAddress shippingAddress;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getLastOrderId() {
        return lastOrderId;
    }

    public void setLastOrderId(Object lastOrderId) {
        this.lastOrderId = lastOrderId;
    }

    public Object getLastOrderDate() {
        return lastOrderDate;
    }

    public void setLastOrderDate(Object lastOrderDate) {
        this.lastOrderDate = lastOrderDate;
    }

    public Integer getOrdersCount() {
        return ordersCount;
    }

    public void setOrdersCount(Integer ordersCount) {
        this.ordersCount = ordersCount;
    }

    public String getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(String totalSpent) {
        this.totalSpent = totalSpent;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public es.mrjeff.app.android.jeff.model.nuevo.order.BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(es.mrjeff.app.android.jeff.model.nuevo.order.BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public es.mrjeff.app.android.jeff.model.nuevo.order.ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(es.mrjeff.app.android.jeff.model.nuevo.order.ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

}
