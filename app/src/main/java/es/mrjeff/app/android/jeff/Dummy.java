package es.mrjeff.app.android.jeff;

import java.util.ArrayList;
import java.util.List;

import es.mrjeff.app.android.jeff.model.Pedido;

/**
 * Created by CQ on 05/11/2016.
 */

public class Dummy {

    public static List<Pedido> getDummyContent(){
        List<Pedido> result= new ArrayList<>();
        for (int i=0;i<10;i++)
            result.add(new Pedido("",false,false,false));
        return result;
    }
}
