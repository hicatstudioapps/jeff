package es.mrjeff.app.android.jeff.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import es.mrjeff.app.android.jeff.database.connect.MrJeffDaraSource;
import es.mrjeff.app.android.jeff.database.util.ConstantsDB;

/**
 * Created by linovm on 18/10/15.
 */
public class ConfigRepository extends MrJeffDaraSource {

    public static final String USER ="USER";


    public ConfigRepository(Context context){
        super(context);
    }


    public String getValueConfig(String name){
        String result = null;
        String [] args = {name};
        String [] columns = {ConstantsDB.COLUMN_CONFIG.VALUE.getName()};
        Cursor c =database.query(ConstantsDB.TABLE_CONFIG_NAME,
                columns,
                ConstantsDB.COLUMN_CONFIG.NAME + " = ?",
                args,
                null,null,null);

        Boolean haveResult = c != null && c.getCount() > 0;
        if(haveResult){
            c.moveToNext();
            result = c.getString(0);
        }

        return result;
    }


    public void updateRegister(String name, String value){
        //Nuestro contenedor de valores
        ContentValues values = new ContentValues();
        values.put(ConstantsDB.COLUMN_CONFIG.VALUE.getName(),value);

        String selection = ConstantsDB.COLUMN_CONFIG.NAME.getName() + " = ?";
        String[] selectionArgs = {name};

        database.update(ConstantsDB.TABLE_CONFIG_NAME, values, selection, selectionArgs);
    }


}
