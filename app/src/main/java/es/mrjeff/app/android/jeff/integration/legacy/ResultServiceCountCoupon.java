package es.mrjeff.app.android.jeff.integration.legacy;

/**
 * Created by linovm on 4/12/15.
 */
public class ResultServiceCountCoupon {

    private String estado;
    private String mensaje;
    private Integer couponusage;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getCouponusage() {
        return couponusage;
    }

    public void setCouponusage(Integer couponusage) {
        this.couponusage = couponusage;
    }
}
