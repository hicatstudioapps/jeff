package es.mrjeff.app.android.jeff.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by CQ on 16/11/2016.
 */

public class LoginData {


    @SerializedName("username")
    @Expose
    String email;

    @SerializedName("password")
    @Expose
    String password;

    public LoginData(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
