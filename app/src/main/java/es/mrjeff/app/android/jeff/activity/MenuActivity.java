package es.mrjeff.app.android.jeff.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.integration.OrderProductManager;

/**
 * Created by Paulino on 12/06/2016.
 */
public abstract class MenuActivity  extends ConnectionActivity
        implements NavigationView.OnNavigationItemSelectedListener  {

    private ImageView imageMenu;
    private TextView textName;
    private TextView textEmail;

    public DrawerLayout getDrawer() {
        return drawer;
    }

    private DrawerLayout drawer;
    private Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    private TextView title;
    private TextView steps;

    public ActionBarDrawerToggle getToggle() {
        return toggle;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        title=(TextView)findViewById(R.id.title);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        if(!AuthenticationOpenBravoStorage.getInstance(this).isLogin()){
//            Intent myIntent = new Intent(this, LoggingActivity.class);
//            startActivity(myIntent);
//        } else{
            initComponent(navigationView);

//        }

    }

    private void initComponent( NavigationView navigationView ){
        imageMenu =(ImageView) navigationView.findViewById(R.id.imageView);
        textName =(TextView) navigationView.findViewById(R.id.textViewName);
        textEmail = (TextView) navigationView.findViewById(R.id.textViewEmail);
   }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id != positionMenu()) {
            if (id == R.id.orders) {
                Intent intent = new Intent(this,OrdersActivity.class);
                startActivity(intent);
                finish();
            } else if (id == R.id.rate) {
//                Intent intent = new Intent(this,ReviewsActivity.class);
//                startActivity(intent);
//                finish();
            } else if (id == R.id.logout) {
                getSharedPreferences("login",MODE_PRIVATE).edit().clear().commit();
                startActivity(new Intent(this,LoggingActivity.class));
                finish();
//                AuthenticationOpenBravoStorage.getInstance(this).logOut();
            }
            else if (id == R.id.precios) {
                Intent intent = new Intent(this,PriceActivity.class);
                startActivity(intent);
                finish();
            }
            else if (id == R.id.nuevo_pedido) {
                OrderProductManager.getInstance(null).clear();
                Intent intent = new Intent(this,NuevoPedidoActivity.class);
                startActivity(intent);
                finish();
            }
            else if (id == R.id.B2B) {
                OrderProductManager.getInstance(null).clear();
                Intent intent = new Intent(this,NuevoPedidoActivity.class);
                intent.putExtra("type",1);
                startActivity(intent);
                finish();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setTitle(String title){
        this.title.setText(title.toString());
    }

    public void setStep(String step){
       // this.steps.setText(step.toString());
    }
    protected abstract int positionMenu();
}
