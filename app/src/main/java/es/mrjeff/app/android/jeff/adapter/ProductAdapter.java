package es.mrjeff.app.android.jeff.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.integration.OrderProductManager;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product_;
import es.mrjeff.app.android.jeff.util.Utils;

/**
 * Created by admin on 02/02/2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    Context context;
    List<Product_> products;
    UpdateSumary updateSumary;
    OrderProductManager orderProductManager;


    public ProductAdapter(Context context, List<Product_> products, UpdateSumary updateSumary) {
        this.context = context;
        this.products = products;
        this.updateSumary=updateSumary;
        orderProductManager= OrderProductManager.getInstance(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.producthome, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product_ product = products.get(position);
        holder.number.setText(orderProductManager.countProduct(product.getId())+"");
        if(orderProductManager.countProduct(product.getId())>0)
            holder.buttominus.setVisibility(View.VISIBLE);
        else
            holder.buttominus.setVisibility(View.INVISIBLE);
        if (new MrJeffImageStorage().images.get(toHex(product.getUrlImage())) != null) {
            int res = new MrJeffImageStorage().images.get(toHex(product.getUrlImage()));
            Glide.with(context)
                    .load(res)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageProduct);
            int w = Utils.getScreenWidth((AppCompatActivity) context);
            int alto = (int) (w / 2f);
            ViewGroup.LayoutParams lp = holder.imageProduct.getLayoutParams();
            lp.width = w;
            lp.height = alto;
            holder.imageProduct.setLayoutParams(lp);
            holder.sombra.setLayoutParams(lp);
        } else
            Glide.with(context)
                    .load(product.getUrlImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageProduct);
        holder.price.setText(product.getPrice());
        holder.title.setText(product.getTitle());
    }

    @Override
    public int getItemCount() {
        return products.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageProduct)
        ImageView imageProduct;
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.buttominus)
        ImageView buttominus;
        @BindView(R.id.buttonplus)
        ImageView buttonplus;
        @BindView(R.id.number)
        TextView number;
        @BindView(R.id.panelproduct)
        RelativeLayout panelproduct;
        @BindView(R.id.sombra)
        View sombra;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            title.setTypeface(JeffAplication.rr);
            price.setTypeface(JeffAplication.rr);
            number.setTypeface(JeffAplication.rb);
        }

        @OnClick({R.id.buttominus, R.id.buttonplus})
        public void onClick(View view) {
            int value= Integer.valueOf(number.getText().toString());

            int product_id=products.get(getAdapterPosition()).getId();
            switch (view.getId()) {
                case R.id.buttominus:
                    orderProductManager.removeProductElement(product_id);
                    if(orderProductManager.countProduct(product_id)>0)
                        number.setText(orderProductManager.countProduct(product_id)+"");
                    else{
                        number.setText("0");
                        view.setVisibility(View.INVISIBLE);}
                    updateSumary.updateSumary(-(Float.parseFloat(products.get(getAdapterPosition()).getPrice())));
                    break;
                case R.id.buttonplus:
                    buttominus.setVisibility(View.VISIBLE);
                    orderProductManager.addProductElement(products.get(getAdapterPosition()).getId());
                    number.setText(orderProductManager.countProduct(product_id)+"");
                    updateSumary.updateSumary((Float.parseFloat(products.get(getAdapterPosition()).getPrice())));
                    break;
            }
        }
    }

    private String toHex(String arg) {
        String result = String.format("%040x", new BigInteger(1, arg.getBytes(/*YOUR_CHARSET?*/)));
        int pos = result.length() - 60 > 0 ? result.length() - 60 : 0;
        result = result.substring(pos);
        Log.d("IAMGE", result + ";" + arg);
        return result;
    }

    public class MrJeffImageStorage {


        public Map<String, Integer> images = new HashMap<String, Integer>();


        {
            images.put("64726f6964362b2f7061636b2d6e6f6c61766f5f6e65774033782e6a7067", R.drawable.i1463011086192);
            images.put("686f746f732f616e64726f6964362b2f5061636b2532303678362e6a7067", R.drawable.i1463011085344);
            images.put("686f746f732f616e64726f6964362b2f5061636b46616d696c792e6a7067", R.drawable.i1463011086683);
            images.put("616e64726f6964362b2f5061636b2532306578656375746976652e6a7067", R.drawable.i1463011085814);
            images.put("64726f696450686f746f2f7375697466697665616e64726f69642e6a7067", R.drawable.i1463011091595);
            images.put("686f746f732f616e64726f6964362b2f31356b672d6170702d322e6a7067", R.drawable.i1463011070252);
            images.put("50686f746f732f616e64726f6964362b2f386b672d6170702d322e6a7067", R.drawable.i1463011071571);
            images.put("5350686f746f732f626f6c73616970686f6e65362b32322d30312e706e67", R.drawable.i1463011075653);
            images.put("61707050686f746f732f616e64726f6964362b2f5472616a65312e6a7067", R.drawable.i1463011092332);
            images.put("6f746f732f616e64726f6964362b2f3525323063616d697361732e6a7067", R.drawable.i1463011071088);
            images.put("746f732f616e64726f6964362b2f313525323063616d697361732e6a7067", R.drawable.i1463011063093);
            images.put("61707050686f746f732f616e64726f6964362b2f43616d6973612e6a7067", R.drawable.i1463011077436);
            images.put("616e64726f6964362b2f5665737469646f2532306669657374612e6a7067", R.drawable.i1463011092924);
            images.put("50686f746f732f616e64726f6964362b2f416d65726963616e612e6a7067", R.drawable.i1463011074000);
            images.put("707050686f746f732f616e64726f6964362b2f5665737469646f2e6a7067", R.drawable.i1463011093306);
            images.put("7050686f746f732f616e64726f6964362b2f50616e74616c6f6e2e6a7067", R.drawable.i1463011089424);
            images.put("746f732f616e64726f6964362b2f61627269676f2d6170702d322e6a7067", R.drawable.i1463011072541);
            images.put("6f746f732f616e64726f6964362b2f506c756d612d6170702d322e6a7067", R.drawable.i1463011090505);
            images.put("2f61707050686f746f732f616e64726f6964362b2f426c7573612e6a7067", R.drawable.i1463011075169);
            images.put("2f616e64726f6964362b2f476162617264696e612d6170702d322e6a7067", R.drawable.i1463011083207);
            images.put("707050686f746f732f616e64726f6964362b2f436f72626174612e6a7067", R.drawable.i1463011079048);
            images.put("2f416e64726f696450686f746f2f68616e646b657263686965662e6a7067", R.drawable.i1463011083938);
            images.put("7050686f746f732f616e64726f6964362b2f50616a61726974612e6a7067", R.drawable.i1463011089034);
            images.put("6d2f61707050686f746f732f616e64726f6964362b2f506f6c6f2e6a7067", R.drawable.i1463011091186);
            images.put("2e636f6d2f416e64726f696450686f746f2f756e69666f726d652e706e67", R.drawable.i1463011092707);
            images.put("2f616e64726f6964362b2f636861717565746f6e2d6170702d322e6a7067", R.drawable.i1463011078227);
            images.put("2f61707050686f746f732f616e64726f6964362b2f46616c64612e6a7067", R.drawable.i1463011081314);
            images.put("7050686f746f732f616e64726f6964362b2f43686171756574612e6a7067", R.drawable.i1463011077814);
            images.put("50686f746f732f616e64726f6964362b2f416c666f6d627261732e6a7067", R.drawable.i1463011073595);
            images.put("707050686f746f732f616e64726f6964362b2f45647265646f6e2e6a7067", R.drawable.i1463011080922);
            images.put("6f6964362b2f4a7565676f2532306465253230736162616e61732e6a7067", R.drawable.ii11463011084171);
            images.put("726f6964362b2f666561746865722d64757665742d6170702d322e6a7067", R.drawable.i1463011081724);
            images.put("707050686f746f732f616e64726f6964362b2f546f616c6c61732e6a7067", R.drawable.i1463011091821);
            images.put("7050686f746f732f616e64726f6964362b2f416c626f726e6f7a2e6a7067", R.drawable.i1463011073212);
            images.put("64726f6964362b2f63757368696f6e2d636173652d4170702d322e6a7067", R.drawable.i1463011080189);
            images.put("726f6964362b2f61726d63686169722d636173652d4150702d322e6a7067", R.drawable.i1463011074442);
            images.put("726f6964362b2f66756e64612d6e6f72646963612d6170702d322e6a7067", R.drawable.i1463011082542);
            images.put("2f616e64726f6964362b2f70696c6f77636173652d4170702d322e6a7067", R.drawable.i1463011089782);
            images.put("6f746f732f616e64726f6964362b2f6d616e74612d6170702d322e6a7067", R.drawable.i1463011084544);
            images.put("616e64726f6964362b2f63756272652d63616d612d6170702d322e6a7067", R.drawable.i1463011079443);
            images.put("707050686f746f732f616e64726f6964362b2f356b672d6170702e6a7067", R.drawable.kgj);

        }
    }

    public interface UpdateSumary{
        public void updateSumary(float total);
    }
}
