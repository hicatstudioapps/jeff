package es.mrjeff.app.android.jeff.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import es.mrjeff.app.android.jeff.alarms.service.PositionService;

/**
 * Created by Paulino on 03/02/2016.
 */
public class AlarmUpdate extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent service = new Intent(context, PositionService.class);
        context.startService(service);
    }
}
