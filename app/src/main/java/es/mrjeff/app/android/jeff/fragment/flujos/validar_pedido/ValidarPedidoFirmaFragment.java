package es.mrjeff.app.android.jeff.fragment.flujos.validar_pedido;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.ActivityFlujoScanner;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import es.mrjeff.app.android.jeff.integration.StageManager;
import es.mrjeff.app.android.jeff.model.JeffStageFinish;
import es.mrjeff.app.android.jeff.model.route.Stage;
import es.mrjeff.app.android.jeff.util.Utils;
import es.mrjeff.app.android.jeff.widget.SlideToUnlock;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.Context.MODE_PRIVATE;

public class ValidarPedidoFirmaFragment extends Fragment implements SlideToUnlock.OnUnlockListener {
    private static final String ARG_PARAM1 = "position";
    private static final String ARG_PARAM2 = "object";
    @BindView(R.id.signature_pad)
    SignaturePad signaturePad;

    // TODO: Rename and change types of parameters
    private String position;
    private String mParam2;
    MaterialDialog loading_dialog;
    Stage stage;

    public ValidarPedidoFirmaFragment() {
        // Required empty public constructor
    }

    public static ValidarPedidoFirmaFragment newInstance(String position, String param2) {
        ValidarPedidoFirmaFragment fragment = new ValidarPedidoFirmaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, position);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @BindView(R.id.slidetounlock)
    SlideToUnlock slideToUnlock;
    @BindView(R.id.confirmation_text)
    TextView confimation_text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_validar_pedido_firma, container, false);
        ButterKnife.bind(this, layout);
        if(!JeffAplication.isJeff)
            confimation_text.setText("José García confirma que ha concluido los servicios de limpieza.");
        slideToUnlock.setOnUnlockListener(this);
        loading_dialog = new MaterialDialog.Builder(getActivity())
                .progress(true, 0)
                .content("Guardando firma...")
                .cancelable(false)
                .build();
        if (getActivity() instanceof PedidoDetalleActivity)
            stage = ((PedidoDetalleActivity) getActivity()).getPedido();
        else
            stage = ((ActivityFlujoScanner) getActivity()).getPedido();
        return layout;
    }

    @Override
    public void onUnlock() {

        getSignatureOservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> broadcastFinish())
                .subscribe(new Observer() {
                    @Override
                    public void onCompleted() {
                        loading_dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "A ocurrido un error.", Toast.LENGTH_SHORT).show();
                        loading_dialog.dismiss();
                    }

                    @Override
                    public void onNext(Object o) {
                        if (o == null)
                            loading_dialog.show();
                    }
                });
      //  broadcastFinish();
    }

    private void broadcastFinish() {
        final Dialog d = new Dialog(getContext(), R.style.MaterialDialogSheetCenter);
        d.setContentView(R.layout.dialog_succes);
        d.setCancelable(false);
        d.getWindow().setLayout((int) (Utils.getScreenWidth(getActivity()) * 0.6), LinearLayout.LayoutParams.WRAP_CONTENT);
        d.getWindow().setGravity(Gravity.CENTER_HORIZONTAL);
        d.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() instanceof PedidoDetalleActivity) {
                  //  stage.setCompleted(true);
                    Intent update = new Intent(JeffConstants.PEDIDO_ENTRAGADO_FILTER);
                    Bundle data = new Bundle();
                    data.putInt("position", PedidoDetalleActivity.position);
                    data.putSerializable("key", ((PedidoDetalleActivity) getActivity()).getPedido());
                    update.putExtras(data);
                    getActivity().sendBroadcast(update);
                    d.dismiss();
                    getActivity().setResult(Activity.RESULT_OK, update);
                    getActivity().finish();
                } else {
//                    if (((ActivityFlujoScanner) getActivity()).getPedido().getType().toLowerCase().equals("pickup")) {
//                        //((ActivityFlujoScanner) getActivity()).getPedido().setCompleted(true);
//                    } else
//                       // ((ActivityFlujoScanner) getActivity()).getPedido().setCompleted(true);
                    Intent update = new Intent(JeffConstants.PEDIDO_ENTRAGADO_FILTER);
                    Bundle data = new Bundle();
                    data.putInt("position", ActivityFlujoScanner.position);
                    data.putSerializable("key", ((ActivityFlujoScanner) getActivity()).getPedido());
                    update.putExtras(data);
                    getActivity().sendBroadcast(update);
                    d.dismiss();
                    getActivity().setResult(Activity.RESULT_OK, update);
                    getActivity().finish();
                }
            }
        }, 1500);
    }

    public Observable getSignatureOservable() {
        return Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                subscriber.onNext(null);
                File firma = Utils.saveBitmap(getActivity(), signaturePad.getSignatureBitmap());
                if (firma != null) {
                    RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), firma);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("file", System.currentTimeMillis() + "", reqFile);
                    String autorization=getContext().getSharedPreferences("auth",MODE_PRIVATE).getString("data","");
                    Call<okhttp3.ResponseBody> call = JeffAplication.apiCalls.uploadSignature(stage.getId(),body,autorization);
                    try {
                        Response<okhttp3.ResponseBody> responseBodyResponse = call.execute();
                        if (responseBodyResponse.code() == 200){
                            int result=  StageManager.finishStage(stage.getId()+"",new JeffStageFinish(JeffAplication.loginResult.getId()+""),autorization);
                            if(result==1)
                                subscriber.onCompleted();
                            else
                                subscriber.onError(new Throwable("Error enviando firma."));
                        }
                        else{
                            subscriber.onError(new Throwable("Error enviando firma."));
                        subscriber.onCompleted();}
                    } catch (IOException e) {
                        subscriber.onError(e);
                    }
                }
            }
        });
    }
}
