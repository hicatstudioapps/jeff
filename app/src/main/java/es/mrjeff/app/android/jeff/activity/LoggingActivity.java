package es.mrjeff.app.android.jeff.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.UnknownHostException;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.model.login.LoginData;
import es.mrjeff.app.android.jeff.model.login.LoginResult;
import es.mrjeff.app.android.jeff.util.ExceptionHandler;
import es.mrjeff.app.android.jeff.util.FormValidatorMrJeff;
import es.mrjeff.app.android.jeff.util.Utils;
import es.mrjeff.app.android.jeff.view.listener.FocusChangeFormLogin;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Paulino on 12/06/2016.
 */
public class LoggingActivity extends ConnectionActivity {

    protected EditText login_email;
    protected EditText login_password;
    protected View btnlogin;
    @BindView(R.id.user_email_login)
    EditText email;
    @BindView(R.id.user_pass_login)
    EditText passw;
    @BindView(R.id.button11)
            View button;
    MaterialDialog fail_info_dialog;
    MaterialDialog loading_dialog;
    private String authorizationString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        initComponents();
        initEvent();
    }

    @Override
    protected void setContentViewActivity() {
        try {
            setContentView(R.layout.activity_login);
            ButterKnife.bind(this);
        } catch (Exception e) {
            btnlogin = findViewById(R.id.btnlogin);
        }
    }

    protected void initComponents() {
        fail_info_dialog = new MaterialDialog.Builder(LoggingActivity.this)
                .positiveText("OK")
                .onPositive((dialog, which) -> dialog.dismiss())
                .build();
        loading_dialog = new MaterialDialog.Builder(LoggingActivity.this)
                .progress(true, 0)
                .content("Enviando datos...")
                .cancelable(false)
                .build();
        login_email = (EditText) findViewById(R.id.user_email_login);
        login_email.setTypeface(JeffAplication.rr);
        login_password = (EditText) findViewById(R.id.user_pass_login);
        login_password.setTypeface(JeffAplication.rr);
        btnlogin = findViewById(R.id.btnlogin);
        ((TextView)findViewById(R.id.btnlogin)).setTypeface(JeffAplication.rr);
    }

    protected void initEvent() {
        btnlogin.setOnClickListener(getOnClickListenerButtonLogin());
        new FocusChangeFormLogin(login_email);
        new FocusChangeFormLogin(login_password);
    }

    protected View.OnClickListener getOnClickListenerButtonLogin() {
        return v -> {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(login_email.getWindowToken(), 0);
            inputMethodManager.hideSoftInputFromWindow(login_password.getWindowToken(), 0);
            sendLoginOrder();
        };
    }

    private void sendLoginOrder() {
        boolean haveError = !FormValidatorMrJeff.validRequired(login_email.getText().toString());
        haveError = haveError || !FormValidatorMrJeff.validRequiredMin(2, login_password.getText().toString());
        if (haveError) {
            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorForm).setMessage(R.string.modalErrorFormText).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    setResult(Activity.RESULT_OK);
                }
            }).show();

        } else {
            loginUser();
        }
    }

    Observable getLoginObservable() {
        return Observable.create(subscriber -> {
            subscriber.onNext(null);
            try {
                String user_name= login_email.getText().toString();
                String passw= login_password.getText().toString();
                authorizationString = "Basic " + Base64.encodeToString(
                        (user_name + ":" + passw).getBytes(),
                        Base64.NO_WRAP);
                String json= new Gson().toJson(new LoginData(user_name,
                        passw), LoginData.class);
            Call<ResponseBody> loginResultCall = JeffAplication.apiCalls.login(new LoginData(user_name,
                    passw),authorizationString);
            Response<ResponseBody> result= loginResultCall.execute();
                Log.d("response code", result.code()+"");
                String ret= result.body().string();
                subscriber.onNext(ret);
                subscriber.onCompleted();
            } catch (Exception e) {
               subscriber.onError(e);
            }
        });
    }

    private void loginUser() {
        getLoginObservable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        loading_dialog.dismiss();
                        if (e instanceof UnknownHostException)
                            fail_info_dialog.setContent(getString(R.string.modalErrorConectionSubTitle));

                        else
                            fail_info_dialog.setContent("Error enviando datos, compruebe su conexión.");
                        fail_info_dialog.show();
                    }

                    @Override
                    public void onNext(String result) {
                        if (result == null)
                            loading_dialog.show();
                        else {
                            try {
                                LoginResult loginResult = new Gson().fromJson(result, LoginResult.class);
                                JeffAplication.loginResult=loginResult;
                                getSharedPreferences("login",MODE_PRIVATE).edit().putString("data",result).commit();
                                getSharedPreferences("auth",MODE_PRIVATE).edit().putString("data",authorizationString).commit();
                                Intent myIntent = new Intent(LoggingActivity.this, OrdersActivity.class);
                                startActivity(myIntent);
                                finish();
                            }catch (Exception ex){
                                fail_info_dialog.setContent("Usuario o contraseña incorrectos.");
                                fail_info_dialog.show();
                            }
//                            if (result != null && result.getToken() != null) {
//                                getSharedPreferences("login",MODE_PRIVATE).edit().putBoolean("logedin",true).commit();
//                                loading_dialog.dismiss();
//                                Intent myIntent = new Intent(LoggingActivity.this, OrdersActivity.class);
//                                startActivity(myIntent);
//                                finish();
//                            } else {
//                                loading_dialog.dismiss();
//                                fail_info_dialog.setContent(getString(R.string.errorlogin));
//                                fail_info_dialog.show();
//                            }
                        }
                    }
                });
    }

}
