package es.mrjeff.app.android.jeff.util;

import android.text.TextUtils;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Pattern;

/**
 * Created by Paulino on 10/03/2016.
 */
public class FormValidatorMrJeff {

    private static final Pattern PATTERN_PHONE_SPAIN9 = Pattern.compile("^9[0-9]{8}$");
    private static final Pattern PATTERN_PHONE_SPAIN8 = Pattern.compile("^8[0-9]{8}$");
    private static final Pattern PATTERN_PHONE_MOBILE = Pattern.compile("^6[0-9]{8}$");
    private static final Pattern PATTERN_PHONE_WORLD = Pattern.compile("^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$");

    public static  boolean validRequired(String text){
        boolean result = true;
        if( text == null || text.isEmpty() || text.length() < 4){
            result = false;
        }

        return result;
    }

    public static boolean validAddress(String text){
        boolean result = validRequired(text);
        if(result){
            String[] split = text.split(",");

            if(split != null && split.length >= 2){
                result = true;

                if(result){
                    result = validRequired(split[0]);
                }

            } else {
                result = false;
            }
        }
        return result;
    }


    public static  boolean validRequiredMin(int lengthMin, String text){
        boolean result = true;
        if(!validRequired(text) || text.length() < lengthMin){
            result = false;
        }

        return result;
    }

    public static  boolean validRequiredMax(int lengthMax, String text){
        boolean result = true;
        if(!validRequired(text) || text.length() > lengthMax){
            result = false;
        }

        return result;
    }

    public static  boolean validRequiredMinMax(int lengthMin, int lengthMax, String text){
        boolean result = true;
        if(!validRequiredMin(lengthMin,text) || !validRequiredMax(lengthMax,text)){
            result = false;
        }

        return result;
    }

    public static boolean validPhone(String text){
        boolean result = false;
        if (TextUtils.isEmpty(text)) {
            return false;
        } else if(PATTERN_PHONE_SPAIN9.matcher(text).matches() ||
                PATTERN_PHONE_SPAIN8.matcher(text).matches() ||
                PATTERN_PHONE_MOBILE.matcher(text).matches() ||
                PATTERN_PHONE_WORLD.matcher(text).matches()){
            result = true;
        }
        return result;
    }


    public final static boolean isValidEmail(String text) {
        EmailValidator emailValidator= EmailValidator.getInstance();
        return emailValidator.isValid(text);
    }

    public final static boolean isValidPostalCode(String text) {
        boolean result = validRequiredMinMax(5,5,text.toString());
        if (result) {
            //String city = PostalCodeStorage.getInstance(null).getCity(text);
            //result = city != null && !city.trim().isEmpty();
        }
        return result;
    }

}
