package es.mrjeff.app.android.jeff.integration;

/**
 * Created by admin on 06/02/2017.
 */

public abstract class GenericManager  {

    public abstract void save();
    public abstract void loadCache();
}
