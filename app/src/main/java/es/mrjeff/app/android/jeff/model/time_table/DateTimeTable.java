
package es.mrjeff.app.android.jeff.model.time_table;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class DateTimeTable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("timetable")
    @Expose
    private List<Timetable> timetable = new ArrayList<Timetable>();

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The timetable
     */
    public List<Timetable> getTimetable() {
        return timetable;
    }

    /**
     * 
     * @param timetable
     *     The timetable
     */
    public void setTimetable(List<Timetable> timetable) {
        this.timetable = timetable;
    }

}
