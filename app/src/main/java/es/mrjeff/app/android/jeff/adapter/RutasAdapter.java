package es.mrjeff.app.android.jeff.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.model.Pedido;
import es.mrjeff.app.android.jeff.model.route.Stage;
import es.mrjeff.app.android.jeff.util.RouteManager;
import es.mrjeff.app.android.jeff.util.Utils;

/**
 * Created by CQ on 11/10/2016.
 */

public class RutasAdapter extends RecyclerView.Adapter<RutasAdapter.ViewHolder> {

    Context context;

    List<Integer> procesados = new ArrayList<Integer>();

    public List<Stage> getPedidos() {
        return pedidos;
    }

    List<Stage> pedidos;

    public RutasAdapter(Context context, Stage pedidos) {
        this.context = context;
        this.pedidos = new ArrayList<>();
        this.pedidos.add(pedidos);
    }

    public void setItem(Stage item, int position) {
        pedidos.remove(position);
        pedidos.add(position, item);
    }

    @Override
    public RutasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.rutas_row, null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        //view.setLayoutParams(lp);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RutasAdapter.ViewHolder holder, int position) {
        Stage pedido = pedidos.get(position);
//        if (pedido.isIncidencia()) {
//            holder.result_icon.setImageResource(R.drawable.fail);
//            holder.success_view.setVisibility(View.VISIBLE);
//        } else if (pedido.isCompleted()) {
//            holder.result_icon.setImageResource(R.drawable.ok_icon);
//            holder.success_view.setVisibility(View.VISIBLE);
//        } else
        DateTime date= Utils.convertStringToDate(pedido.getPlannedArrivalDate());
        holder.time.setText(date.toString("HH:mm"));
        holder.distance.setText(pedido.getDistance()+" Km");
        holder.success_view.setVisibility(View.GONE);
        holder.tv_position.setText(String.valueOf(position + 1));
        if (pedido.getType().toLowerCase().equals("pickup")) {
             holder.action.setText("Recoger");
        } else {
            holder.action.setText("Entregar");
        }
        holder.name.setText(pedido.getCustomer().getName()+" "+pedido.getCustomer().getLastName());
        holder.address.setText(pedido.getAddress().getAddressLine());
        holder.id.setText("ID: "+pedido.getId());
    }

    @Override
    public int getItemCount() {
        return pedidos.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btn_recoger)
        View btn_recoger;
        @BindView(R.id.action)
        TextView action;
        @BindView(R.id.position)
        TextView tv_position;
        @BindView(R.id.imageView3)
        View btn_detalle;
        @BindView(R.id.success)
        View success_view;
        @BindView(R.id.result_icon)
        ImageView result_icon;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.adress)
        TextView address;
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.time)
                TextView time;
        @BindView(R.id.distance_time)
                TextView distance;
        View root;

        public ViewHolder(View itemView) {
            super(itemView);
            root = itemView;
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.detail)
        public void showDetalle() {
            Intent detalle = new Intent(context, PedidoDetalleActivity.class);
            detalle.putExtra("position", getAdapterPosition());
            Bundle data = new Bundle();
            data.putSerializable("key", pedidos.get(getAdapterPosition()));
            detalle.putExtras(data);
            if (procesados.contains(getAdapterPosition()))
                detalle.putExtra("entregado", true);
            context.startActivity(detalle);
        }

        @OnClick(R.id.root)
        public void movemapToPosition(){
            showDetalle();
        }
    }
}
