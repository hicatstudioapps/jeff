package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;
import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.NuevoPedidoActivity;
import es.mrjeff.app.android.jeff.api.ApiCalls;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.OrderApi;
import es.mrjeff.app.android.jeff.integration.CustomerManager;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.integration.OrderProductManager;
import es.mrjeff.app.android.jeff.integration.SuscriptionB2BManager;
import es.mrjeff.app.android.jeff.model.nuevo.order.LineItem;
import es.mrjeff.app.android.jeff.model.nuevo.order.Oder;
import es.mrjeff.app.android.jeff.model.nuevo.order.OrderMeta;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product_;
import es.mrjeff.app.android.jeff.model.nuevo.product.SendProducts;
import es.mrjeff.app.android.jeff.model.time_table.DateTimeTable;
import es.mrjeff.app.android.jeff.model.time_table.Timetable;
import es.mrjeff.app.android.jeff.util.DialogManager;
import es.mrjeff.app.android.jeff.view.components.TimePickerView;
import retrofit2.Call;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class NuevoPedidoHorarios extends Fragment implements Step {

    @BindView(R.id.recogida_dia)
    TextView recogida_dia;
    @BindView(R.id.recogida_hora)
    TextView recogida_hora;
    @BindView(R.id.entrega_dia)
    TextView entrega_dia;
    @BindView(R.id.entrega_hora)
    TextView entrega_hora;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.recogida_time)
    LinearLayout recogidaTime;
    @BindView(R.id.textView78)
    TextView textView78;
    @BindView(R.id.imageView88)
    ImageView imageView88;
    @BindView(R.id.imageView89)
    ImageView imageView89;
    @BindView(R.id.entrega_time)
    LinearLayout entregaTime;
    @BindView(R.id.imageView19)
    ImageView imageView19;
    @BindView(R.id.finish)
    RelativeLayout finish;
    @BindView(R.id.content_activity_direccion)
    RelativeLayout contentActivityDireccion;

    private Dialog dialog_pickerRecogida;
    TimePickerView timePickerViewRecogida;
    private Dialog dialog_pickerEntrega;
    TimePickerView timePickerViewEntrega;
    private DateTimeTable dateTimeTableRecogida;
    private DateTimeTable dateTimeTableEntrega;
    private NuevoPedidoActivity listener;
    Dialog dialog;
    private int type;
    private DateTime recogidaSus;
    private DateTime entregaSus;


    // TODO: Rename and change types and number of parameters
    public static NuevoPedidoHorarios newInstance(int param1) {
        NuevoPedidoHorarios fragment = new NuevoPedidoHorarios();
        Bundle bundle= new Bundle();
        bundle.putInt("type",param1);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type= getArguments().getInt("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nuevo_pedido_horarios, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void init() {
        name.setTypeface(JeffAplication.rb);
        email.setTypeface(JeffAplication.rb);
        recogida_dia.setTypeface(JeffAplication.rr);
        recogida_hora.setTypeface(JeffAplication.rr);
        entrega_dia.setTypeface(JeffAplication.rr);
        entrega_hora.setTypeface(JeffAplication.rr);
        next.setTypeface(JeffAplication.rb);
        name.setText(CustomerManager.getInstance(null).getCustomer().getCustomer().getFirstName() + " " +
                CustomerManager.getInstance(null).getCustomer().getCustomer().getLastName());
        email.setText(CustomerManager.getInstance(null).getCustomer().getCustomer().getEmail());
        if (type == 0) {
            dialog = DialogManager.getLoadingDialog(getContext(), "Cargando horarios");
            dialog.show();
            dialog_pickerRecogida = new Dialog(getActivity(), R.style.MaterialDialogSheetCenter);
            dialog_pickerRecogida.setContentView(getActivity().getLayoutInflater().inflate(R.layout.time_picker_dialog, null));
            dialog_pickerRecogida.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog_pickerRecogida.getWindow().setGravity(Gravity.BOTTOM);
            timePickerViewRecogida = ((TimePickerView) dialog_pickerRecogida.findViewById(R.id.time_picker_view));
            ((TextView) dialog_pickerRecogida.findViewById(R.id.type)).setText("Recogida");
            dialog_pickerRecogida.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateRecogida(timePickerViewRecogida.getHourSelected(), timePickerViewRecogida.getDateSelected());
                    generateDeliveryDates();
                    dialog_pickerRecogida.dismiss();
                }
            });
            dialog_pickerEntrega = new Dialog(getActivity(), R.style.MaterialDialogSheetCenter);
            dialog_pickerEntrega.setContentView(getActivity().getLayoutInflater().inflate(R.layout.time_picker_dialog, null));
            dialog_pickerEntrega.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog_pickerEntrega.getWindow().setGravity(Gravity.BOTTOM);
            timePickerViewEntrega = ((TimePickerView) dialog_pickerEntrega.findViewById(R.id.time_picker_view));
            ((TextView) dialog_pickerEntrega.findViewById(R.id.type)).setText("Entrega");
            dialog_pickerEntrega.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateEntrega(timePickerViewEntrega.getHourSelected(), timePickerViewEntrega.getDateSelected());
                    dialog_pickerEntrega.dismiss();
                }
            });
            getDateTimeTable();
        } else {
            SuscriptionB2BManager suscriptionB2BManager= SuscriptionB2BManager.getInstance(null);
            recogidaSus= calcularFechaSuscripcionRecogida(new DateTime(),Integer.parseInt(suscriptionB2BManager.getSuscriptionB2b().getPickupDay()));
            entregaSus=calcularFechaSuscripcionEntrega(recogidaSus,Integer.parseInt(suscriptionB2BManager.getSuscriptionB2b().getDropoffDay()));
            updateEntrega(suscriptionB2BManager.getSuscriptionB2b().getDropoffHour(), entregaSus.toString("yyyy-MM-dd"));
            updateRecogida(suscriptionB2BManager.getSuscriptionB2b().getPickupHour(), recogidaSus.toString("yyyy-MM-dd"));

            }
        //eliminamos el cupon existente en cache
        OrderManager orderManager = OrderManager.getInstance(null);
        orderManager.setCoupon(null);
        orderManager.save();

    }

    public DateTime calcularFechaSuscripcionRecogida(DateTime ref, int week_day){
        SuscriptionB2BManager suscriptionB2BManager= SuscriptionB2BManager.getInstance(null);
        DateTime recogida;
        int dayOfWeek= ref.getDayOfWeek();
        int day_recogida= Integer.parseInt(suscriptionB2BManager.getSuscriptionB2b().getPickupDay());
        if(dayOfWeek == day_recogida){
            recogida= ref.plusDays(7);
        }else if(dayOfWeek > day_recogida)
            recogida= ref.plusDays((7-ref.dayOfWeek().get())+ day_recogida);
        else
            recogida=ref.plusDays((day_recogida- ref.dayOfWeek().get()));
        return recogida;
    }

    public DateTime calcularFechaSuscripcionEntrega(DateTime ref, int week_day){
        SuscriptionB2BManager suscriptionB2BManager= SuscriptionB2BManager.getInstance(null);
        DateTime entrega;
        int dayOfWeek= ref.getDayOfWeek();
        int day_recogida= Integer.parseInt(suscriptionB2BManager.getSuscriptionB2b().getDropoffDay());
        if(dayOfWeek == day_recogida){
            entrega= ref.plusDays(7);
        }else if(dayOfWeek > day_recogida)
            entrega= ref.plusDays((7-ref.dayOfWeek().get())+ day_recogida);
        else
            entrega=ref.plusDays((day_recogida- ref.dayOfWeek().get()));
        return entrega;
    }

    @OnClick(R.id.recogida_time)
    public void recogidaTiem() {
        if(dialog_pickerRecogida!= null)
        dialog_pickerRecogida.show();
    }

    @OnClick(R.id.entrega_time)
    public void entregaTime() {
        if(dialog_pickerEntrega != null)
        dialog_pickerEntrega.show();
    }

    public void getDateTimeTable() {
        final String[] url = {"http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/timetablev2.php?postalCode=" + OrderManager.getInstance(getContext()).getOrder().getBillingAddress().getPostcode()};
        final String[] url2 = {"http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/timetablev2.php?postalCode=" + OrderManager.getInstance(getContext()).getOrder().getShippingAddress().getPostcode()};
        Log.d("Url postal", url[0]+"\n"+url2[0]);
        Observable.create(subscriber -> {
            ApiCalls call = ApiServiceGenerator.createService(ApiCalls.class);
//            url[0] = "http://192.168.101.1:8081/jeff/time.json";
//            url2[0] = "http://192.168.101.1:8081/jeff/time2.txt";
            Call<DateTimeTable> call1 = call.getTimeTable(url[0]);
            Call<DateTimeTable> call2 = call.getTimeTable(url2[0]);
            try {
                dateTimeTableRecogida = call1.execute().body();
                dateTimeTableEntrega = call2.execute().body();
                subscriber.onNext(null);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
                subscriber.onCompleted();
            }

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MaterialDialog error_Dialog = new MaterialDialog.Builder(getContext())
                                .title("No está conectado")
                                .content("Por favor revise sus ajustes y vuelva a intentarlo cuando tenga acceso a internet.")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        getActivity().finish();
                                    }
                                })
                                .negativeText("Volver a intentar")
                                .onNegative((dialog1, which) -> {
                                    dialog.dismiss();
                                    getDateTimeTable();
                                })
                                .cancelable(false)
                                .positiveText("OK")
                                .show();
                    }

                    @Override
                    public void onNext(Object o) {
                        timePickerViewRecogida.initData(dateTimeTableRecogida.getTimetable(), true);
                        initInitialDateRecogida(dateTimeTableRecogida.getTimetable().get(0));
                        //llenamos el picker de entrega con el valor por defecto del de recogida
                        List<Timetable> timetable = getDeliveryDates(dateTimeTableRecogida.getTimetable()
                                .get(0).getDay(), Integer.parseInt(dateTimeTableRecogida.getTimetable().get(0).getMinTiming()));
                        timePickerViewEntrega.initData(timetable, false);
                        generateDeliveryDates();
                        dialog.dismiss();
                    }
                });
    }

    private void initInitialDateEntrega(Timetable timetable) {
        updateEntrega(timetable.getDropoffTimetable().split(";")[0], timetable.getDay());
    }

    private void updateEntrega(String hour, String day) {
        entrega_hora.setText(hour);
        entrega_dia.setText(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(day).toString("dd, MMM"));
    }


    private void initInitialDateRecogida(Timetable timetable) {
        updateRecogida(timetable.getPickupTimetable().split(";")[0], timetable.getDay());
    }

    public List<Timetable> getDeliveryDates(String date, int minDays) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(date);
        DateTime plus = dateTime.plusDays(minDays);
        if (plus.getDayOfWeek() == DateTimeConstants.SUNDAY)
            plus = plus.plusDays(1);
        String date_to_search = plus.toString("yyyy-MM-dd");
        List<Timetable> result = new ArrayList<>();
        for (int i = 0; i < dateTimeTableEntrega.getTimetable().size(); i++) {
            if (dateTimeTableEntrega.getTimetable().get(i).getDay().equals(date_to_search)) {
                result = dateTimeTableEntrega.getTimetable().subList(i, dateTimeTableEntrega.getTimetable().size());
                break;
            }
        }
        updateEntrega(result.get(0).getDropoffTimetable().split(";")[0], result.get(0).getDay());
        return result;
    }

    public void updateRecogida(String hour, String date) {
        recogida_hora.setText(hour);
        recogida_dia.setText(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(date).toString("dd, MMM"));

    }

    private void generateDeliveryDates() {
        timePickerViewEntrega.initData(getDeliveryDates(timePickerViewRecogida.getDateSelected()
                , timePickerViewRecogida.getMin_day()), false);
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        init();
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @OnClick(R.id.finish)
    public void onClick() {
        OrderMeta orderMeta = new OrderMeta();
        if(type==0){
        String recogidaDate = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(timePickerViewRecogida.getDateSelected()).toString("dd-MM-yyyy");
        String recogidaHour = timePickerViewRecogida.getHourSelected().split("-")[0] + " - " + timePickerViewRecogida.getHourSelected().split("-")[1];
        String entregaaDate = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(timePickerViewEntrega.getDateSelected()).toString("dd-MM-yyyy");
        String entregaHour = timePickerViewEntrega.getHourSelected().split("-")[0] + " - " + timePickerViewEntrega.getHourSelected().split("-")[1];
        orderMeta.setMyfield2(recogidaDate);
        orderMeta.setMyfield3(recogidaHour);
        orderMeta.setMyfield4(entregaaDate);
        orderMeta.setMyfield5(entregaHour);
        }else {
            SuscriptionB2BManager suscriptionB2BManager= SuscriptionB2BManager.getInstance(null);
            String recogidaDate = recogidaSus.toString("dd-MM-yyyy");
            String recogidaHour = suscriptionB2BManager.getSuscriptionB2b().getPickupHour();
            String entregaaDate = entregaSus.toString("dd-MM-yyyy");
            String entregaHour = suscriptionB2BManager.getSuscriptionB2b().getDropoffHour();
            orderMeta.setMyfield2(recogidaDate);
            orderMeta.setMyfield3(recogidaHour);
            orderMeta.setMyfield4(entregaaDate);
            orderMeta.setMyfield5(entregaHour);
        }
        orderMeta.setDispositvo("android");
        OrderManager orderManager = OrderManager.getInstance(getContext());
        orderManager.getOrder().setOrderMeta(orderMeta);
        orderManager.save();
        Oder oder = new Oder();
        oder.setOrder(orderManager.getOrder());
        String actualJson = new Gson().toJson(oder, Oder.class);
        Log.d("Actual Json", "save: " + actualJson);
        crearPedido(oder);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (NuevoPedidoActivity) context;
    }

    void crearPedido(Oder order) {
        if (JeffAplication.testingLocal) {
            listener.nextPage();
            return;
        }
        dialog = DialogManager.getLoadingDialog(getContext(), "Creando Pedido...");
        dialog.show();
        Observable.create((Observable.OnSubscribe<Oder>) subscriber -> {
            OrderApi orderApi = ApiServiceGenerator.createService(OrderApi.class);
            SendProducts product = null;
            try {
                if (NuevoPedidoActivity.pedido_minimo) {
                    Product_ product_ = new Product_();
                    product_.setType("simple");
                    product_.setTitle("Pedido mínimo ");
                    product_.setCatalogVisibility("hidden");
                    product_.setTaxStatus("none");
                    product_.setRegularPrice(new BigDecimal(JeffConstants.PEDIDO_MINIMO).subtract(OrderProductManager.getInstance(getContext()).getPriceTotal()).toString());
                    product = new SendProducts();
                    product.setProduct(product_);
                    String json = new Gson().toJson(product, SendProducts.class);
                    Call<SendProducts> productCall = orderApi.createMinProduct(JeffConstants.TOKEN_consumer_key, JeffConstants.TOKEN_consumer_secret, product);
                    SendProducts resulProduct_1 = productCall.execute().body();
                    LineItem minimo = new LineItem();
                    minimo.setProductId(resulProduct_1.getProduct().getId());
                    minimo.setQuantity(1);
                    order.getOrder().getLineItems().add(minimo);
                    // registramos el pedido
                }
                String json = new Gson().toJson(order, Oder.class);
                Call<Oder> orderCall = orderApi.createOrder(JeffConstants.TOKEN_consumer_key, JeffConstants.TOKEN_consumer_secret, order);
                Oder oder = orderCall.execute().body();
                if (oder != null) {
                    Log.d("ID Pedido", order.getOrder().getId()+"");
                    OrderManager.saveFinalOrder(oder.getOrder());
                    subscriber.onNext(oder);
                    subscriber.onCompleted();
                } else throw new Exception("sdsds");


            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Oder>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                      //  Toast.makeText(getContext(), "Error creando pedido!!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Oder oder) {
                        Toast.makeText(getContext(), "Pedido Creado con exito!!", Toast.LENGTH_SHORT).show();
                        listener.nextPage();
                        dialog.dismiss();
                    }
                });
    }


}
