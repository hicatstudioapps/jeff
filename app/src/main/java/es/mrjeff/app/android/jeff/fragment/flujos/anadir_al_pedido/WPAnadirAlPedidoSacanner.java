package es.mrjeff.app.android.jeff.fragment.flujos.anadir_al_pedido;

import android.support.v4.app.Fragment;

import me.panavtec.wizard.WizardPage;

/**
 * Created by CQ on 23/10/2016.
 */

public class WPAnadirAlPedidoSacanner extends WizardPage<Fragment> {
    @Override
    public Fragment createFragment() {
        return new AnadirAlPedidoSacanner();
    }
}
