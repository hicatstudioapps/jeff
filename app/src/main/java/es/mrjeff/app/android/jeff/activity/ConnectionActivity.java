package es.mrjeff.app.android.jeff.activity;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;

/**
 * Created by linovm on 10/10/15.
 */
public abstract class ConnectionActivity extends AppCompatActivity {

    protected Boolean finishActivity = Boolean.TRUE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        finishActivity = Boolean.FALSE;
        super.onCreate(savedInstanceState);
        setContentViewActivity();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
//        NetworkInfo activeConnection = connectivityManager.getActiveNetworkInfo();
//        boolean isOnline = (activeConnection != null) && activeConnection.isConnected();
//        if(!isOnline){
//            new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.Theme_AppCompat_Light)).setTitle(R.string.modalErrorConectionTitle).setMessage(R.string.modalErrorConectionSubTitle).setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int id) {
//                    setResult(RESULT_OK);
//                    finish();
//                }
//            }).show();
//        }


    }

    protected abstract void setContentViewActivity();


}
