
package es.mrjeff.app.android.jeff.model.nuevo.order;

import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.annimon.stream.Stream;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.Streams;

import es.mrjeff.app.android.jeff.integration.legacy.Coupon_lines;

public class Order {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("order_number")
    @Expose
    private Integer orderNumber;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("completed_at")
    @Expose
    private String completedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("total_line_items_quantity")
    @Expose
    private Integer totalLineItemsQuantity;
    @SerializedName("total_tax")
    @Expose
    private String totalTax;
    @SerializedName("total_shipping")
    @Expose
    private String totalShipping;
    @SerializedName("cart_tax")
    @Expose
    private String cartTax;
    @SerializedName("shipping_tax")
    @Expose
    private String shippingTax;
    @SerializedName("total_discount")
    @Expose
    private String totalDiscount;
    @SerializedName("shipping_methods")
    @Expose
    private String shippingMethods;
    @SerializedName("payment_details")
    @Expose
    private PaymentDetails paymentDetails;
    @SerializedName("billing_address")
    @Expose
    private BillingAddress billingAddress;
    @SerializedName("shipping_address")
    @Expose
    private ShippingAddress shippingAddress;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("customer_ip")
    @Expose
    private String customerIp;
    @SerializedName("customer_user_agent")
    @Expose
    private String customerUserAgent;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("view_order_url")
    @Expose
    private String viewOrderUrl;
    @SerializedName("line_items")
    @Expose
    private List<LineItem> lineItems = null;
    @SerializedName("shipping_lines")
    @Expose
    private List<Object> shippingLines = null;
    @SerializedName("tax_lines")
    @Expose
    private List<TaxLine> taxLines = null;
    @SerializedName("fee_lines")
    @Expose
    private List<Object> feeLines = null;
    @SerializedName("coupon_lines")
    @Expose
    private List<Coupon_lines> couponLines = null;
    @SerializedName("wpo_wcpdf_invoice_number")
    @Expose
    private String wpoWcpdfInvoiceNumber;
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("order_meta")
    @Expose
    private OrderMeta orderMeta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(String completedAt) {
        this.completedAt = completedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getTotalLineItemsQuantity() {
        return totalLineItemsQuantity;
    }

    public void setTotalLineItemsQuantity(Integer totalLineItemsQuantity) {
        this.totalLineItemsQuantity = totalLineItemsQuantity;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public String getTotalShipping() {
        return totalShipping;
    }

    public void setTotalShipping(String totalShipping) {
        this.totalShipping = totalShipping;
    }

    public String getCartTax() {
        return cartTax;
    }

    public void setCartTax(String cartTax) {
        this.cartTax = cartTax;
    }

    public String getShippingTax() {
        return shippingTax;
    }

    public void setShippingTax(String shippingTax) {
        this.shippingTax = shippingTax;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getShippingMethods() {
        return shippingMethods;
    }

    public void setShippingMethods(String shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(ShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCustomerIp() {
        return customerIp;
    }

    public void setCustomerIp(String customerIp) {
        this.customerIp = customerIp;
    }

    public String getCustomerUserAgent() {
        return customerUserAgent;
    }

    public void setCustomerUserAgent(String customerUserAgent) {
        this.customerUserAgent = customerUserAgent;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getViewOrderUrl() {
        return viewOrderUrl;
    }

    public void setViewOrderUrl(String viewOrderUrl) {
        this.viewOrderUrl = viewOrderUrl;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public List<Object> getShippingLines() {
        return shippingLines;
    }

    public void setShippingLines(List<Object> shippingLines) {
        this.shippingLines = shippingLines;
    }

    public List<TaxLine> getTaxLines() {
        return taxLines;
    }

    public void setTaxLines(List<TaxLine> taxLines) {
        this.taxLines = taxLines;
    }

    public List<Object> getFeeLines() {
        return feeLines;
    }

    public void setFeeLines(List<Object> feeLines) {
        this.feeLines = feeLines;
    }

    public List<Coupon_lines> getCouponLines() {
        return couponLines;
    }

    public void setCouponLines(List<Coupon_lines> couponLines) {
        this.couponLines = couponLines;
    }

    public String getWpoWcpdfInvoiceNumber() {
        return wpoWcpdfInvoiceNumber;
    }

    public void setWpoWcpdfInvoiceNumber(String wpoWcpdfInvoiceNumber) {
        this.wpoWcpdfInvoiceNumber = wpoWcpdfInvoiceNumber;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public OrderMeta getOrderMeta() {
        return orderMeta;
    }

    public void setOrderMeta(OrderMeta orderMeta) {
        this.orderMeta = orderMeta;
    }

    public Order getOrderNoCopuon(){
        Order result= new Order();
        //obtenemos los line items, obviando el cupon
        List<LineItem> lineItemList= new ArrayList<>();
        Stream.of(lineItems).forEach(lineItem -> {
            if(Float.parseFloat(lineItem.getPrice())>0){
                LineItem temp= new LineItem();
                temp.setProductId(lineItem.getProductId());
                temp.setQuantity(lineItem.getQuantity());
                lineItemList.add(temp);
            }
        });
        //fijamos la misma direccion de entrega y recogida
        result.setCustomerId(getCustomerId());
        result.setLineItems(lineItemList);
        result.setShippingAddress(getShippingAddress());
        result.setBillingAddress(getBillingAddress());
        result.setOrderMeta(getOrderMeta());
        return result;
    }

}
