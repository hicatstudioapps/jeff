package es.mrjeff.app.android.jeff.adapter;

import android.content.Context;
import android.support.annotation.BinderThread;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import es.mrjeff.app.android.jeff.model.issues.IssueSend;

/**
 * Created by CQ on 05/12/2016.
 */

public class IssueAdapter extends RecyclerView.Adapter<IssueAdapter.Holder> {

    Context context;
    View edit,ok,otros;
    View recycler;
    IssueSend issueSend=new IssueSend();

    public IssueAdapter(Context context, View otros,View edit, View ok, View recicler) {
        this.context = context;
        this.otros=otros;
        this.edit=edit;
        this.ok=ok;
        this.recycler=recicler;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.issue_dialog_item,null);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
            holder.text.setText(JeffAplication.issueTypes.get(position).getName());
        holder.text.setTag(JeffAplication.issueTypes.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return JeffAplication.issueTypes.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        @BindView(R.id.horario)
        TextView text;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(text.getTag().toString().toLowerCase().equals("otros")){
                        recycler.setVisibility(View.GONE);
                        otros.setVisibility(View.VISIBLE);
                        issueSend.setJeffId(JeffAplication.loginResult.getId()+"");
                        issueSend.setIncidentTypeId(JeffAplication.issueTypes.get(getAdapterPosition()).getId()+"");
                        ok.setOnClickListener(click -> {
                            issueSend.setCause(((EditText)edit).getText().toString());
                            ((PedidoDetalleActivity)context).setIssueSend(issueSend,issueSend.getCause());
                        });
                    }else
                    ((PedidoDetalleActivity)context).setIssueSend(new IssueSend(JeffAplication.issueTypes.get(getAdapterPosition()).getId()+"",
                            JeffAplication.loginResult.getId()+"",""),JeffAplication.issueTypes.get(getAdapterPosition()).getName());
                }
            });
        }


    }
}
