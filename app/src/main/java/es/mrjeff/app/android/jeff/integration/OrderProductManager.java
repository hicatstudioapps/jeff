package es.mrjeff.app.android.jeff.integration;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.annimon.stream.Stream;
import com.google.gson.Gson;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.dao.JeffCacheDao;
import es.mrjeff.app.android.jeff.model.nuevo.order.LineItem;

/**
 * Created by admin on 06/02/2017.
 */

public class OrderProductManager extends GenericManager {

    //id-cantidad
    Map<Integer,Integer> products;

    Integer numTotal = 0;

    public BigDecimal getPriceTotal() {
        return new BigDecimal(priceTotal).setScale(2, RoundingMode.HALF_EVEN);
    }

    public void setPriceTotal(String priceTotal) {
        this.priceTotal = priceTotal;
        save();
    }

    String priceTotal= "0.00";


    private static OrderProductManager mInstance = null;
    private static Context mCtx;

    private OrderProductManager(Context context){
        mCtx = context;
    }

    public static synchronized OrderProductManager getInstance(Context context){
        if(mInstance == null){
            mInstance = new OrderProductManager(context);
            mInstance.loadCache();
        }
        else
            mCtx = context;
        return mInstance;
    }

    public void addProduct(Integer idProduct, Integer count){
        products.put(idProduct,count);
        numTotal = numTotal + count;

        save();
    }

    public void remove(Integer idProduct){
        try {
            Integer numProduct = products.get(idProduct);
            if(numProduct != null && numProduct.intValue() >= 0) {
                numTotal = numTotal - numProduct;
                products.remove(idProduct);
            }

            save();
        }catch (Exception e){

        }
    }

    public void addProductElement(Integer idProduct){
        Integer num = products.get(idProduct);
        if(num == null){
            num = 0;
        }

        products.put(idProduct,++num);
        numTotal++;

        save();

    }

    public void removeProductElement(Integer idProduct){
        Integer num = products.get(idProduct);
        if(num == null || num.intValue() == 0){
            num = 0;
        } else {
            products.put(idProduct,--num);
            --numTotal;
        }
        save();
    }

    public void removeProductElementAll(Integer idProduct){
        Integer num = products.get(idProduct);
        products.put(idProduct,0);
        numTotal-=num;
        save();
    }

    public Integer countProduct(Integer idProduct){
        Integer num = products.get(idProduct);
        if(num == null){
            num = 0;
        }

        return num;

    }

    public void clear(){
        products.clear();
        products = new HashMap<Integer,Integer>();
        numTotal = 0;
        priceTotal="0.00";
        save();
    }

    public List<LineItem> getLineItem(){
        List<LineItem> lineItems= new ArrayList<>();
        Set<Integer> keys=products.keySet();
        Stream.of(keys).forEach(integer -> {
            LineItem lineItem= new LineItem();
            lineItem.setProductId(integer);
            lineItem.setQuantity(products.get(integer));
            lineItems.add(lineItem);
        });
        return lineItems;
    }

    public Map<Integer, Integer> getProducts() {
        return products;
    }

    private void cleanProducts(){
        int total = 0;
        List<Integer> keyRemove= new ArrayList<Integer>();
        if(products != null && products.size() > 0){
            for(Integer key: products.keySet() ){
                if(products.get(key) != null && products.get(key).intValue() > 0) {
                    total = total + products.get(key);
                } else{
                    keyRemove.add(key);
                }
            }
        }

        if(keyRemove != null && keyRemove.size() > 0){
            for(Integer key: keyRemove ){
                products.remove(key);
            }
        }

        numTotal = total;
    }
    
    
    @Override
    public void save() {
        JeffCacheDao jeffCacheDao= JeffAplication.getDaoSession().getJeffCacheDao();
        String json= new Gson().toJson(this,OrderProductManager.class);
        JeffCache jeffCache= jeffCacheDao.load(1L);
        jeffCache.setPicked_products(json);
        jeffCacheDao.updateInTx(jeffCache);
    }

    @Override
    public void loadCache() {
        JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = dao.load(1L);
        String json = jeffCache.getPicked_products();
        try {
            if (json == null || TextUtils.isEmpty(json)) {
                numTotal = 0;
                products = new HashMap<>();
                priceTotal = "0.00";
            } else {
                OrderProductManager offline = new Gson().fromJson(json, OrderProductManager.class);
                numTotal = offline.numTotal;
                products = offline.products;
                priceTotal = offline.priceTotal;
            }
        } catch (Exception e) {
            e.printStackTrace();
            numTotal = 0;
            products = new HashMap<>();
            priceTotal = "0.00";
        }
    }
}
