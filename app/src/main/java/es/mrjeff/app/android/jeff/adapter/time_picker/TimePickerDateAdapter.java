package es.mrjeff.app.android.jeff.adapter.time_picker;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.model.time_table.Timetable;

/**
 * Created by CQ on 10/11/2016.
 */

public class TimePickerDateAdapter extends RecyclerView.Adapter<TimePickerDateAdapter.Holder> implements DateTimePickerHourAdapter.SelectedHour{

    private final SelectedDataDate listener;
    boolean type;
    List<Timetable> dateTimeTable;
    RecyclerView rv_hour;
    int selected_position=0;
    Holder last_selected=null;

    DateTimePickerHourAdapter dateTimePickerHourAdapter;
    Context context;
    String dateSelected;
    String minTiming="";

    public String getDateSelected() {
        return dateSelected;
    }

    public String getMinTiming() {
        return minTiming;
    }

    public boolean isType() {
        return type;
    }

    public DateTimePickerHourAdapter getDateTimePickerHourAdapter() {
        return dateTimePickerHourAdapter;
    }


    public TimePickerDateAdapter(List<Timetable> dateTimeTable, RecyclerView rv_hour, Context context, boolean type,SelectedDataDate listener) {
        this.dateTimeTable = dateTimeTable;
        this.rv_hour = rv_hour;
        this.context = context;
        this.type= type;
        this.listener= listener;
        selected_position=0;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.time_picker_date_item,null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if(last_selected==null){
            last_selected=holder;
            listener.onDateSelected(dateTimeTable.get(position).getDay());
            listener.onMinDay(Integer.parseInt(dateTimeTable.get(position).getMinTiming()));
        }
        if(position==selected_position){
            holder.root.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.day_week.setTextColor(Color.WHITE);
            holder.day_plus_month.setTextColor(Color.WHITE);
        }else{
            holder.root.setBackgroundColor(Color.WHITE);
            holder.day_week.setTextColor(context.getResources().getColor(R.color.colorAccent));
            holder.day_plus_month.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }

        Timetable timetable= dateTimeTable.get(position);
        DateTimeFormatter dateTimeFormatter= DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime dateTime= dateTimeFormatter.parseDateTime(timetable.getDay());
        Log.d("Dia de la semana", " "+ dateTime.getDayOfWeek());
        holder.day_week.setText(StringUtils.capitalize(dateTime.dayOfWeek().getAsText(new Locale("ES"))));
        holder.day_plus_month.setText(dateTime.dayOfMonth().getAsText()+" de "+ StringUtils.capitalize(dateTime.monthOfYear().getAsText()));
    }

    @Override
    public int getItemCount() {
        return dateTimeTable.size();
    }

    @Override
    public void onHourSelected(String data) {
        listener.onHourSelected(data);
    }

    public class Holder  extends RecyclerView.ViewHolder {

        @BindView(R.id.day_week)
        TextView day_week;
        @BindView(R.id.day_plus_month)
        TextView day_plus_month;
        @BindView(R.id.root)
        View root;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(selected_position!= getAdapterPosition()){
                        root.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                        day_week.setTextColor(Color.WHITE);
                        day_plus_month.setTextColor(Color.WHITE);
                        last_selected.root.setBackgroundColor(Color.WHITE);
                        last_selected.day_week.setTextColor(context.getResources().getColor(R.color.colorAccent));
                        last_selected.day_plus_month.setTextColor(context.getResources().getColor(R.color.colorAccent));
                        last_selected=Holder.this;
                    selected_position=getAdapterPosition();
                    dateSelected= dateTimeTable.get(getAdapterPosition()).getDay();
                    listener.onDateSelected(dateSelected);
                    minTiming= dateTimeTable.get(getAdapterPosition()).getMinTiming();
                    listener.onMinDay(Integer.parseInt(minTiming));
                    if(type)
                    dateTimePickerHourAdapter.setData(dateTimeTable.get(getAdapterPosition()).getPickupTimetable().split(";"));
                    else
                        dateTimePickerHourAdapter.setData(dateTimeTable.get(getAdapterPosition()).getDropoffTimetable().split(";"));
                    dateTimePickerHourAdapter.resetFlags();
                    dateTimePickerHourAdapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }

    public void setType(boolean pickUp){
        if(pickUp){
            if(dateTimePickerHourAdapter==null){
                dateTimePickerHourAdapter= new DateTimePickerHourAdapter(dateTimeTable.get(0).getPickupTimetable().split(";"),context,this);
                rv_hour.setAdapter(dateTimePickerHourAdapter);
            }else{
                dateTimePickerHourAdapter.setData(dateTimeTable.get(0).getPickupTimetable().split(";"));
                dateTimePickerHourAdapter.notifyDataSetChanged();
            }
        }
        else{
            if(dateTimePickerHourAdapter==null){
                dateTimePickerHourAdapter= new DateTimePickerHourAdapter(dateTimeTable.get(0).getDropoffTimetable().split(";"),context,this);
                rv_hour.setAdapter(dateTimePickerHourAdapter);
            }else{
                dateTimePickerHourAdapter.setData(dateTimeTable.get(0).getDropoffTimetable().split(";"));
                dateTimePickerHourAdapter.notifyDataSetChanged();
            }
        }
    }

   public interface SelectedDataDate{
        void onDateSelected(String data);
        void onHourSelected(String data);
        void onMinDay(int day);
    }
}
