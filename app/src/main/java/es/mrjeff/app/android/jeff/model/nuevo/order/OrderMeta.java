
package es.mrjeff.app.android.jeff.model.nuevo.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderMeta {

    @SerializedName("nm_members")
    @Expose
    private Object nmMembers;
    @SerializedName("dispositvo")
    @Expose
    private String dispositvo;
    @SerializedName("myfield4")
    @Expose
    private String myfield4;
    @SerializedName("myfield5")
    @Expose
    private String myfield5;
    @SerializedName("myfield2")
    @Expose
    private String myfield2;

    public String getMyfield3() {
        return myfield3;
    }

    public void setMyfield3(String myfield3) {
        this.myfield3 = myfield3;
    }

    public String getMyfield2() {
        return myfield2;
    }

    public void setMyfield2(String myfield2) {
        this.myfield2 = myfield2;
    }

    @SerializedName("myfield3")
    @Expose
    private String myfield3;

    public Object getNmMembers() {
        return nmMembers;
    }

    public void setNmMembers(Object nmMembers) {
        this.nmMembers = nmMembers;
    }

    public String getDispositvo() {
        return dispositvo;
    }

    public void setDispositvo(String dispositvo) {
        this.dispositvo = dispositvo;
    }

    public String getMyfield4() {
        return myfield4;
    }

    public void setMyfield4(String myfield4) {
        this.myfield4 = myfield4;
    }

    public String getMyfield5() {
        return myfield5;
    }

    public void setMyfield5(String myfield5) {
        this.myfield5 = myfield5;
    }

}
