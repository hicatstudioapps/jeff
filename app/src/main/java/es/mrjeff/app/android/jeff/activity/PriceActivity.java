package es.mrjeff.app.android.jeff.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.adapter.PreciosAdapter;

/**
 * Created by CQ on 12/10/2016.
 */

public class PriceActivity extends MenuActivity {

    @BindView(R.id.list)
    RecyclerView listaDePrecios;

    @Override
    protected int positionMenu() {
        return R.id.precios;
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_price);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initComponents();
    }

    private void initComponents() {
        listaDePrecios.setAdapter(new PreciosAdapter(this));
    }
}
