package es.mrjeff.app.android.jeff.fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.NuevoPedidoActivity;
import es.mrjeff.app.android.jeff.api.ApiCalls;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.model.time_table.DateTimeTable;
import es.mrjeff.app.android.jeff.model.time_table.Timetable;
import es.mrjeff.app.android.jeff.view.components.TimePickerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NuevoPedidoDatosGenerales extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    @BindView(R.id.recogida_dia)
    TextView recogida_dia;
    @BindView(R.id.recogida_hora)
    TextView recogida_hora;
    @BindView(R.id.entrega_dia)
    TextView entrega_dia;
    @BindView(R.id.entrega_hora)
    TextView entrega_hora;
    @BindView(R.id.textView7)
    TextView picker_title;
    @BindView(R.id.entrega_time)
    View entrega_time;

    private Dialog dialog_pickerRecogida;
    TimePickerView timePickerViewRecogida;
    private Dialog dialog_pickerEntrega;
    TimePickerView timePickerViewEntrega;
    private DateTimeTable dateTimeTable;
    
    
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Context context;
    private LayoutInflater inflater;


    public NuevoPedidoDatosGenerales() {
        // Required empty public constructor
    }

    public static NuevoPedidoDatosGenerales newInstance(String param1, String param2) {
        NuevoPedidoDatosGenerales fragment = new NuevoPedidoDatosGenerales();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_wizard_page2, container, false);
        ButterKnife.bind(this,view);
        this.inflater=inflater;
        return view;
    }

    private void init() {
        if(!JeffAplication.isJeff){
            picker_title.setText("LLegada");
        }else
            entrega_time.setVisibility(View.VISIBLE);
        dialog_pickerRecogida = new Dialog(this.context, R.style.MaterialDialogSheetCenter);
        dialog_pickerRecogida.setContentView(inflater.inflate(R.layout.time_picker_dialog, null));
        dialog_pickerRecogida.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog_pickerRecogida.getWindow().setGravity(Gravity.BOTTOM);
        timePickerViewRecogida = ((TimePickerView) dialog_pickerRecogida.findViewById(R.id.time_picker_view));
        ((TextView) dialog_pickerRecogida.findViewById(R.id.type)).setText("Recogida");
        dialog_pickerRecogida.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateRecogida(timePickerViewRecogida.getHourSelected(), timePickerViewRecogida.getDateSelected());
                generateDeliveryDates();
                dialog_pickerRecogida.dismiss();
            }
        });
        dialog_pickerEntrega = new Dialog(this.context, R.style.MaterialDialogSheetCenter);
        dialog_pickerEntrega.setContentView(inflater.inflate(R.layout.time_picker_dialog, null));
        dialog_pickerEntrega.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog_pickerEntrega.getWindow().setGravity(Gravity.BOTTOM);
        timePickerViewEntrega = ((TimePickerView) dialog_pickerEntrega.findViewById(R.id.time_picker_view));
        ((TextView) dialog_pickerEntrega.findViewById(R.id.type)).setText("Entrega");
        dialog_pickerEntrega.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEntrega(timePickerViewEntrega.getHourSelected(), timePickerViewEntrega.getDateSelected());
                dialog_pickerEntrega.dismiss();
            }
        });
        getDateTimeTable();
    }

    @OnClick(R.id.recogida_time)
    public void recogidaTiem() {
        dialog_pickerRecogida.show();
    }

    @OnClick(R.id.entrega_time)
    public void entregaTime() {
        dialog_pickerEntrega.show();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        ((NuevoPedidoActivity)getActivity()).setTitle("Datos Generales");
        ((NuevoPedidoActivity)getActivity()).setStep("1/2");
        ((NuevoPedidoActivity)getActivity()).getToggle().setDrawerIndicatorEnabled(false);
        ((NuevoPedidoActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((NuevoPedidoActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((NuevoPedidoActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.back));
    }
    @OnClick(R.id.next)
    public void next(){
        //((NuevoPedidoActivity)getActivity()).getWizard().navigateNext();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    public void getDateTimeTable() {
        ApiCalls call = ApiServiceGenerator.createService(ApiCalls.class);
        String url = "http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/timetablev2.php?postalCode=28014";
//        url = "http://192.168.101.1:8081/jeff/time.json";
//        call.getTimeTable(url).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.isSuccessful()) {
//                    try {
//                        dateTimeTable = new Gson().fromJson(response.body().string(), DateTimeTable.class);
//                        timePickerViewRecogida.initData(dateTimeTable.getTimetable(), true);
//                        initInitialDateRecogida(dateTimeTable.getTimetable().get(0));
//                        //llenamos el picker de entrega con el valor por defecto del de recogida
//                        List<Timetable> timetable = getDeliveryDates(dateTimeTable.getTimetable()
//                                .get(0).getDay(), Integer.parseInt(dateTimeTable.getTimetable().get(0).getMinTiming()));
//                        timePickerViewEntrega.initData(timetable, false);
//                        initInitialDateEntrega(timetable.get(0));
//                    } catch (IOException e) {
//                        onFailure(call, null);
//                    }
//                } else {
//                    onFailure(call, null);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                MaterialDialog error_Dialog = new MaterialDialog.Builder(context)
//                        .title("No está conectado")
//                        .content("Por favor revise sus ajustes y vuelva a intentarlo cuando tenga acceso a internet.")
//                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                            @Override
//                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
////                                ActivityDireccion.this.finish();
//                            }
//                        })
//                        .cancelable(false)
//                        .positiveText("OK")
//                        .show();
//            }
//        });
    }

    private void initInitialDateEntrega(Timetable timetable) {
        updateEntrega(timetable.getDropoffTimetable().split(";")[0], timetable.getDay());
    }

    private void updateEntrega(String hour, String day) {
        entrega_hora.setText(hour);
        entrega_dia.setText(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(day).toString("dd, MMM"));
    }


    private void initInitialDateRecogida(Timetable timetable) {
        updateRecogida(timetable.getPickupTimetable().split(";")[0], timetable.getDay());
    }

    public List<Timetable> getDeliveryDates(String date, int minDays) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(date);
        DateTime plus = dateTime.plusDays(minDays);
        if (plus.getDayOfWeek() == DateTimeConstants.SUNDAY)
            plus = plus.plusDays(1);
        String date_to_search = plus.toString("yyyy-MM-dd");
        List<Timetable> result = new ArrayList<>();
        for (int i = 0; i < dateTimeTable.getTimetable().size(); i++) {
            if (dateTimeTable.getTimetable().get(i).getDay().equals(date_to_search)) {
                result = dateTimeTable.getTimetable().subList(i, dateTimeTable.getTimetable().size());
                break;
            }
        }
        updateEntrega(result.get(0).getDropoffTimetable().split(";")[0],result.get(0).getDay());
        return result;
    }

    public void updateRecogida(String hour, String date) {
        recogida_hora.setText(hour);
        recogida_dia.setText(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(date).toString("dd, MMM"));

    }

    private void generateDeliveryDates() {
        timePickerViewEntrega.initData(getDeliveryDates(timePickerViewRecogida.getDateSelected()
                , timePickerViewRecogida.getMin_day()), false);
    }
}
