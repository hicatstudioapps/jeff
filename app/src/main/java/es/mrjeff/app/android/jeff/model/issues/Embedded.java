
package es.mrjeff.app.android.jeff.model.issues;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Embedded {

    @SerializedName("incidentTypes")
    @Expose
    private List<IssueType> issueTypes = new ArrayList<IssueType>();

    /**
     * 
     * @return
     *     The issueTypes
     */
    public List<IssueType> getIssueTypes() {
        return issueTypes;
    }

    /**
     * 
     * @param issueTypes
     *     The issueTypes
     */
    public void setIssueTypes(List<IssueType> issueTypes) {
        this.issueTypes = issueTypes;
    }

}
