package es.mrjeff.app.android.jeff.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.api.GoogleApiClient;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.adapter.IssueAdapter;
import es.mrjeff.app.android.jeff.fragment.AnadirFragment;
import es.mrjeff.app.android.jeff.fragment.ProductoDetalleFragment;
import es.mrjeff.app.android.jeff.fragment.ScannerFragment;
import es.mrjeff.app.android.jeff.fragment.flujos.validar_pedido.ValidarPedidoW2;
import es.mrjeff.app.android.jeff.integration.IssueManager;
import es.mrjeff.app.android.jeff.integration.StageManager;
import es.mrjeff.app.android.jeff.model.JeffStageFinish;
import es.mrjeff.app.android.jeff.model.issues.IssueSend;
import es.mrjeff.app.android.jeff.model.route.Stage;
import es.mrjeff.app.android.jeff.util.Utils;
import es.mrjeff.app.android.jeff.widget.SlideToUnlock;
import me.panavtec.wizard.Wizard;
import me.panavtec.wizard.WizardListener;
import me.panavtec.wizard.WizardPage;
import me.panavtec.wizard.WizardPageListener;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PedidoDetalleActivity extends AppCompatActivity implements SlideToUnlock.OnUnlockListener,
        WizardPageListener, WizardListener, ProductoDetalleFragment.ScanerProdutAction {

    @BindView(R.id.detalles_container)
    View detalle_container;
    @BindView(R.id.bottom_container)
    View bottom_container;
    @BindView(R.id.anadir)
    View anadir;
    @BindView(R.id.ly_incidencia)
    View ly_incidencia;
    @BindView(R.id.tv_incidencia)
    TextView tv_incidencia;
    @BindView(R.id.ly_success)
    View ly_success;
    @BindView(R.id.ly_validar)
    View ly_validar;
    @BindView(R.id.productos_anadidos)
    View productos_anadidos;
    @BindView(R.id.pedido_creado)
    View pedido_creado;
    @BindView(R.id.scanner_container)
    View scaner_container;
    @BindView(R.id.productos)
    View servicios_container;

    //toolbar
    private Toolbar toolbar;

    //botones superiores
    @BindView(R.id.button3)
    View btn_productos;
    @BindView(R.id.añadir_container)
    View añadir_container;
    @BindView(R.id.productos_container)
    View productos_container;
    @BindView(R.id.anadir_indicator)
    View anadir_indicator;
    @BindView(R.id.button4)
    View btn_detalle;
    @BindView(R.id.detalle_indicator)
    View detalle_indicator;
    private Dialog incidencia_dialog;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private ScannerFragment scannerFragment;
    private AnadirFragment anadirFragment;

    public Stage getPedido() {
        return pedido;
    }

    private Stage pedido;

    public void setIssueSend(IssueSend issueSend, String text) {
        this.issueSend = issueSend;
        tv_incidencia.setText(text);
        incidencia_dialog.dismiss();
        ly_incidencia.setVisibility(View.VISIBLE);
    }

    IssueSend issueSend;

    public Wizard getValidar_pedido_wizard() {
        return validar_pedido_wizard;
    }

    //validar_pedido_wizard
    Wizard validar_pedido_wizard;

    public void setEs_para_nuevo_pedido(boolean es_para_nuevo_pedido) {
        this.es_para_nuevo_pedido = es_para_nuevo_pedido;
    }

    // si corresponde al un nuevo pedido o no
    boolean es_para_nuevo_pedido = false;

    //slide
    @BindView(R.id.slidetounlock)
    SlideToUnlock slideToUnlock;

    //objectos de referencia para los flujos
    public static int position;
    MaterialDialog progress_dialog;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.textView155)
    TextView horario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_pedido_detalle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        pedido = (Stage) getIntent().getSerializableExtra("key");
        position = getIntent().getIntExtra("position", 0);
        ButterKnife.bind(this);
        showBottomViews();
        initComp();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            super.onBackPressed();
        return false;
    }


    @BindView(R.id.result_icon)
    ImageView result_icon;

    private void showBottomViews() {
//        if(pedido.isIncidencia()){
//            result_icon.setImageResource(R.drawable.fail);
//            ly_success.setVisibility(View.VISIBLE);
//        }else if(pedido.isCompleted()){
//            result_action.setText(pedido.getType().toLowerCase().equals("pickup") ? "RECOGIDO": "ENTREGADO");
//            ly_success.setVisibility(View.VISIBLE);}
//        else
        ly_validar.setVisibility(View.VISIBLE);
    }

    void initComp() {
        //scannerFragment= ScannerFragment.newInstance();
       // getSupportFragmentManager().beginTransaction().replace(R.id.scanner_container,scannerFragment).commit();
        if(!JeffAplication.isJeff){
            añadir_container.setVisibility(View.INVISIBLE);
            btn_productos.setVisibility(View.INVISIBLE);
            servicios_container.setVisibility(View.VISIBLE);
        }
        name.setText(pedido.getCustomer().getName() + " " + pedido.getCustomer().getLastName());
        phone.setText(pedido.getCustomer().getPhoneNumber());
        address.setText(pedido.getAddress().getAddressLine());
        DateTime date = Utils.convertStringToDate(pedido.getPlannedArrivalDate());
        horario.setText(String.format(getString(R.string.date_detail), date.toString("d"), date.toString("MMMM yyyy HH:mm aaa")));
        //aqgregamos el apartado detalle
        progress_dialog = new MaterialDialog.Builder(this)
                .progress(true, 0)
                .content("Guardando estado...")
                .cancelable(false)
                .build();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        slideToUnlock.setOnUnlockListener(this);
    }

    @OnClick(R.id.incidencia)
    void showIncidenciasDialog() {
//
        View view = getLayoutInflater().inflate(R.layout.issue_dialog, null);
        incidencia_dialog = new Dialog(this, R.style.MaterialDialogSheet);
        incidencia_dialog.setContentView(view);
        incidencia_dialog.setCancelable(true);
        incidencia_dialog.getWindow().setLayout((int) (Utils.getScreenWidth(this) * 0.8), LinearLayout.LayoutParams.WRAP_CONTENT);
        incidencia_dialog.getWindow().setGravity(Gravity.BOTTOM);
        View otros = view.findViewById(R.id.otros);
        View edit= view.findViewById(R.id.editText6);
        View ok = view.findViewById(R.id.textView15);
        ((RecyclerView) view.findViewById(R.id.rv)).setAdapter(new IssueAdapter(this, otros,edit, ok,view.findViewById(R.id.rv)));
        view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                issueSend = null;
                ly_incidencia.setVisibility(View.GONE);
                incidencia_dialog.dismiss();
            }
        });
        incidencia_dialog.show();

    }

    void showScannerDialog() {
        MaterialDialog recogida_dialog = new MaterialDialog.Builder(this)
                .content(pedido.getType().toLowerCase().equals("delivery") ? "Acabas de confirmar que has entregado este pedido?" : "Has completado el escaneo de todos los productos?")
                .negativeText("No")
                .cancelable(false)
                .positiveText("Si")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        slideToUnlock.reset();
//                        if (pedido.getType().toLowerCase().equals("pickup")) {
//                            Intent flujo_escaner = new Intent(PedidoDetalleActivity.this, ActivityFlujoScanner.class);
//                            Bundle data = new Bundle();
//                            data.putSerializable("key", pedido);
//                            data.putInt("position", position);
//                            flujo_escaner.putExtras(data);
//                            startActivityForResult(flujo_escaner, JeffConstants.FLUJO_RECOGER);
//                        } else {
                            validar_pedido_wizard = new Wizard.Builder(PedidoDetalleActivity.this, new ValidarPedidoW2())
                                    .containerId(R.id.wizard_container)
                                    .enterAnimation(R.anim.card_slide_right_in)
                                    .exitAnimation(R.anim.card_slide_left_out)
                                    .popEnterAnimation(R.anim.card_slide_left_in)
                                    .popExitAnimation(R.anim.card_slide_right_out)
                                    .build();
                            validar_pedido_wizard.returnToFirst();
                            validar_pedido_wizard.init();
                       // }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        slideToUnlock.reset();
                        dialog.dismiss();
                    }
                }).build();
        if(!JeffAplication.isJeff){
            recogida_dialog.setContent("Ha concluido con todas sus tareas de limpieza?");
        }
        recogida_dialog.show();
    }

    @OnClick(R.id.button4)
    public void mostrarDetalle() {
        if (detalle_container.getVisibility() != View.VISIBLE) {
            detalle_indicator.setVisibility(View.VISIBLE);
            anadir_indicator.setVisibility(View.INVISIBLE);
            productos_container.setVisibility(View.GONE);
            detalle_container.setVisibility(View.VISIBLE);
            bottom_container.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.call)
    public void call() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "653307225"));
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivity(intent);
    }

    @OnClick(R.id.button3)
    public void mostrarProductos() {
        if (productos_container.getVisibility() != View.VISIBLE) {
            ProductoDetalleFragment anadirFragment = ProductoDetalleFragment.newInstance("","");
            getSupportFragmentManager().beginTransaction().replace(R.id.productos_container, anadirFragment).commit();
            detalle_indicator.setVisibility(View.INVISIBLE);
            anadir_indicator.setVisibility(View.VISIBLE);
            productos_container.setVisibility(View.VISIBLE);
            detalle_container.setVisibility(View.INVISIBLE);
            //bottom_container.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.toolbar_anadir,R.id.add})
    public void mostrarAnadir(){
        if (anadir.getVisibility() != View.VISIBLE) {
            anadirFragment = new AnadirFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.anadir, anadirFragment).commit();
            anadir.setVisibility(View.VISIBLE);
        }else{
            anadir.setVisibility(View.GONE);
        }
    }

    @BindView(R.id.result_action)
    TextView result_action;

    @Override
    public void onUnlock() {
        //Toast.makeText(this, "Unlocked", Toast.LENGTH_SHORT).show();
        //si hemos seleccionado alguna incidencia, marcamos el pedido como no entregado o recogido
//        if(pedido.isIncidencia()){
//            ly_validar.setVisibility(View.GONE);
//            result_icon.setImageResource(R.drawable.fail);
//            ly_success.setVisibility(View.VISIBLE);
//            if(pedido.getType().toLowerCase().equals("pickup"))
//                result_action.setText("NO RECOGIDO");
//            else
//                result_action.setText("NO ENTREGADO");
//            //mandamos a actualizar la ruta
//            Intent ruta= new Intent(JeffConstants.PEDIDO_ENTRAGADO_FILTER);
//            Bundle data= new Bundle();
//            data.putSerializable("key",pedido);
//            ruta.putExtra("position",position);
//            ruta.putExtras(data);
//            sendBroadcast(ruta);
//        }else
//        showScannerDialog();
        if (issueSend != null) {
            publishIncidencia();
        } else
            showScannerDialog();
    }

    private void publishIncidencia() {
        MaterialDialog error_dialg = new MaterialDialog.Builder(this)
                .title("Error")
                .content("Ha ocurrido un error actualizando el estado de la ruta.")
                .positiveText("Reintentar")
                .negativeText("Canncelar")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        publishIncidencia();
                    }
                }).build();
        progress_dialog.show();
        Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                String autorization=getSharedPreferences("auth",MODE_PRIVATE).getString("data","");
                if (IssueManager.registerIncident(pedido.getId() + "", issueSend,autorization) == 1) {
                    if (StageManager.finishStage(pedido.getId() + "", new JeffStageFinish(JeffAplication.loginResult.getId() + ""),autorization) == 1)
                        subscriber.onNext(1);
                    else
                        subscriber.onNext(-1);
                } else {
                    subscriber.onError(null);
                }

            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Integer>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        slideToUnlock.reset();
                        progress_dialog.dismiss();
                        error_dialg.show();
                    }

                    @Override
                    public void onNext(Integer integer) {
                        if (integer.intValue() == 1) {
                            progress_dialog.dismiss();
                            Intent ruta = new Intent(JeffConstants.PEDIDO_ENTRAGADO_FILTER);
                            sendBroadcast(ruta);
                            finish();
                        } else onError(null);
                    }
                });
    }

    @Override
    public void onPageChanged(int currentPageIndex, WizardPage page) {


    }

    @Override
    public void onWizardFinished() {

    }

    public void anadirAlPedidoWizard() {
        Intent flujo_sacanner = new Intent(this, ActivityFlujoScanner.class);
        Bundle data = new Bundle();
        data.putSerializable("key", pedido);
        data.putInt("position", position);
        data.putBoolean("nuevo", true);
        flujo_sacanner.putExtras(data);
        if (es_para_nuevo_pedido) {
            startActivityForResult(flujo_sacanner, JeffConstants.FLUJO_NUEVO_PEDIDO);
        } else {
            startActivityForResult(flujo_sacanner, JeffConstants.FLUJO_ANADIR_PEDIDO);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
//            pedido= (Stage) data.getExtras().getSerializable("key");
//            if(requestCode == JeffConstants.FLUJO_RECOGER ){
//                ly_validar.setVisibility(View.GONE);
//                showBottomViews();
//                return;
//            }
//            if(requestCode == JeffConstants.FLUJO_ANADIR_PEDIDO){
//                ly_validar.setVisibility(View.GONE);
//                productos_anadidos.setVisibility(View.VISIBLE);
//                mostrarDetalle();
//                return;
//            }
//            if(requestCode == JeffConstants.FLUJO_NUEVO_PEDIDO){
//                ly_validar.setVisibility(View.GONE);
//                pedido_creado.setVisibility(View.VISIBLE);
//                mostrarDetalle();
//                return;
//            }
            finish();
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
//    public Action getIndexApiAction() {
//        Thing object = new Thing.Builder()
//                .setName("PedidoDetalle Page") // TODO: Define a title for the content shown.
//                // TODO: Make sure this auto-generated URL is correct.
//                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
//                .build();
//        return new Action.Builder(Action.TYPE_VIEW)
//                .setObject(object)
//                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
//                .build();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        AppIndex.AppIndexApi.start(client, getIndexApiAction());
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        AppIndex.AppIndexApi.end(client, getIndexApiAction());
//        client.disconnect();
//    }

    @Override
    public void showScanner() {
        scaner_container.setVisibility(View.VISIBLE);
    }

    public void hideScanner() {
        scaner_container.setVisibility(View.INVISIBLE);
    }
}
