
package es.mrjeff.app.android.jeff.model.time_table;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Timetable {

    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("ordersClosing")
    @Expose
    private String ordersClosing;
    @SerializedName("pickupTimetable")
    @Expose
    private String pickupTimetable;
    @SerializedName("dropoffTimetable")
    @Expose
    private String dropoffTimetable;
    @SerializedName("minTiming")
    @Expose
    private String minTiming;

    /**
     * 
     * @return
     *     The city
     */
    public String getCity() {
        return city;
    }

    /**
     * 
     * @param city
     *     The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 
     * @return
     *     The postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * 
     * @param postalCode
     *     The postalCode
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * 
     * @return
     *     The day
     */
    public String getDay() {
        return day;
    }

    /**
     * 
     * @param day
     *     The day
     */
    public void setDay(String day) {
        this.day = day;
    }

    /**
     * 
     * @return
     *     The ordersClosing
     */
    public String getOrdersClosing() {
        return ordersClosing;
    }

    /**
     * 
     * @param ordersClosing
     *     The ordersClosing
     */
    public void setOrdersClosing(String ordersClosing) {
        this.ordersClosing = ordersClosing;
    }

    /**
     * 
     * @return
     *     The pickupTimetable
     */
    public String getPickupTimetable() {
        return pickupTimetable;
    }

    /**
     * 
     * @param pickupTimetable
     *     The pickupTimetable
     */
    public void setPickupTimetable(String pickupTimetable) {
        this.pickupTimetable = pickupTimetable;
    }

    /**
     * 
     * @return
     *     The dropoffTimetable
     */
    public String getDropoffTimetable() {
        return dropoffTimetable;
    }

    /**
     * 
     * @param dropoffTimetable
     *     The dropoffTimetable
     */
    public void setDropoffTimetable(String dropoffTimetable) {
        this.dropoffTimetable = dropoffTimetable;
    }

    /**
     * 
     * @return
     *     The minTiming
     */
    public String getMinTiming() {
        return minTiming;
    }

    /**
     * 
     * @param minTiming
     *     The minTiming
     */
    public void setMinTiming(String minTiming) {
        this.minTiming = minTiming;
    }

}
