package es.mrjeff.app.android.jeff;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.List;

import es.mrjeff.app.android.jeff.api.ApiCalls;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.model.dao.DaoMaster;
import es.mrjeff.app.android.jeff.model.dao.DaoSession;
import es.mrjeff.app.android.jeff.model.issues.IssueType;
import es.mrjeff.app.android.jeff.model.login.LoginResult;
import es.mrjeff.app.android.jeff.model.nuevo.order.Order;
import es.mrjeff.app.android.jeff.util.RouteManager;

/**
 * Created by CQ on 16/11/2016.
 */

public class JeffAplication extends MultiDexApplication {

    public static ApiCalls apiCalls;
    public static RouteManager routeManager;
    public static List<IssueType> issueTypes;
    public static LoginResult loginResult;
    public static boolean isJeff=true;
    public static boolean testingLocal=false;
    public static Typeface arial;
    public static Typeface rb;
    public static Typeface rr;

    static DaoSession daoSession;

    public static DaoSession getDaoSession() {
        return daoSession;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DaoMaster.DevOpenHelper devOpenHelper= new DaoMaster.DevOpenHelper(this,"jeff-db");
        SQLiteDatabase sqLiteDatabase= devOpenHelper.getWritableDatabase();
        DaoMaster daoMaster= new DaoMaster(sqLiteDatabase);
        daoSession= daoMaster.newSession();
        apiCalls= ApiServiceGenerator.createService(ApiCalls.class);
        routeManager= RouteManager.getInstance();
        arial=Typeface.createFromAsset(getAssets(), "font/arial.ttf");
        rb= Typeface.createFromAsset(getAssets(), "font/rb.ttf");
        rr=Typeface.createFromAsset(getAssets(), "font/rr.ttf");
        String loginData= getSharedPreferences("login",MODE_PRIVATE).getString("data","");
        if(TextUtils.isEmpty(loginData))
            loginResult=null;
        else
            loginResult= new Gson().fromJson(loginData,LoginResult.class);


    }

}
