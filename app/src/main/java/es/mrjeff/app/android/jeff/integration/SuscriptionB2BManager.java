package es.mrjeff.app.android.jeff.integration;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.model.b2b.SuscriptionB2b;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.dao.JeffCacheDao;
import es.mrjeff.app.android.jeff.model.nuevo.customer.Customer;

/**
 * Created by admin on 21/03/2017.
 */

public class SuscriptionB2BManager extends GenericManager{

    private static SuscriptionB2BManager mInstance = null;
    SuscriptionB2b suscriptionB2b;

    public SuscriptionB2b getSuscriptionB2b() {
        return suscriptionB2b;
    }

    public void setSuscriptionB2b(SuscriptionB2b suscriptionB2b) {
        this.suscriptionB2b = suscriptionB2b;
    }

    private static Context mCtx;

    private SuscriptionB2BManager(Context context){
        mCtx = context;
    }

    public static synchronized SuscriptionB2BManager getInstance(Context context){
        if(mInstance == null)
            mInstance = new SuscriptionB2BManager(context);
        else
            mCtx = context;
        return mInstance;
    }

    @Override
    public void save() {
        JeffCacheDao jeffCacheDao= JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache= jeffCacheDao.load(1L);
        jeffCache.setB2b_suscription(new Gson().toJson(suscriptionB2b, SuscriptionB2b.class));
        jeffCacheDao.updateInTx(jeffCache);
    }

    @Override
    public void loadCache() {
        JeffCacheDao jeffCacheDao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = jeffCacheDao.load(1L);
        String offline = jeffCache.getB2b_suscription();
        if (offline == null && TextUtils.isEmpty(offline))
            suscriptionB2b = null;
        else {
            SuscriptionB2b offsuscriptionB2b = new Gson().fromJson(offline, SuscriptionB2b.class);
            suscriptionB2b = offsuscriptionB2b;
        }
    }
}
