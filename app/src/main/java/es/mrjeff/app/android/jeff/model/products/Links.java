
package es.mrjeff.app.android.jeff.model.products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links {

    @SerializedName("self")
    @Expose
    private Self self;
    @SerializedName("productType")
    @Expose
    private ProductType_ productType;
    @SerializedName("productItems")
    @Expose
    private ProductItems productItems;

    public Self getSelf() {
        return self;
    }

    public void setSelf(Self self) {
        this.self = self;
    }

    public ProductType_ getProductType() {
        return productType;
    }

    public void setProductType(ProductType_ productType) {
        this.productType = productType;
    }

    public ProductItems getProductItems() {
        return productItems;
    }

    public void setProductItems(ProductItems productItems) {
        this.productItems = productItems;
    }

}
