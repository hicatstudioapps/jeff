package es.mrjeff.app.android.jeff.fragment.flujos.validar_pedido;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.activity.ActivityFlujoScanner;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import es.mrjeff.app.android.jeff.R;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ValidarPedidoScannerFragment extends Fragment implements ZXingScannerView.ResultHandler{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    @BindView(R.id.camera_option)
    View camera_option;
    @BindView(R.id.key_board_option)
    View key_borad_option;
    @BindView(R.id.ohter_content)
    View other_content;
    @BindView(R.id.key_container)
    View key_container;
    @BindView(R.id.options)
    View options_container;

    @BindView(R.id.code)
    EditText code;

    public ValidarPedidoScannerFragment() {
        // Required empty public constructor
    }

    private ZXingScannerView mScannerView;


    public static ValidarPedidoScannerFragment newInstance(String param1, String param2) {
        ValidarPedidoScannerFragment fragment = new ValidarPedidoScannerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        RelativeLayout layout= (RelativeLayout) inflater.inflate(R.layout.fragment_validar_pedido_scanner, container, false);
        mScannerView = new ZXingScannerView(getContext());
        ((RelativeLayout)layout.findViewById(R.id.scanner)).addView(mScannerView);
        ButterKnife.bind(this,layout);
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i== EditorInfo.IME_ACTION_DONE){
                    Toast.makeText(getActivity(),"safsdf",Toast.LENGTH_SHORT).show();
                    ((ActivityFlujoScanner)getActivity()).getWizard().navigateNext();
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(code.getWindowToken(), 0);
                }
//                EditorInfo.IME_ACTION_DONE
                return false;
            }
        });

        //mostramos las opciones 2 segundos despues, para dar tiempo a que se inicialice el scanner
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Animation anim= AnimationUtils.loadAnimation(getActivity(),android.R.anim.fade_in);
                options_container.setAnimation(anim);
                options_container.setVisibility(View.VISIBLE);
            }
        },1500);
    }

    @OnClick(R.id.button8)
    public void siguiente(){
        ((PedidoDetalleActivity)getActivity()).getValidar_pedido_wizard().navigateNext();
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Toast.makeText(getContext(), "Contents = " + rawResult.getText() +
                ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();

        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ValidarPedidoScannerFragment.this);
            }
        }, 2000);
    }

    @OnClick(R.id.key_board_option)
    public void showKeyBoard(){
        if(other_content.getVisibility()!= View.VISIBLE){
            key_container.setBackgroundColor(Color.WHITE);
            other_content.setVisibility(View.VISIBLE);
            //mostramos el teclado
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(code, InputMethodManager.SHOW_IMPLICIT);
        }
    }
    @OnClick(R.id.camera_option)
    public void show_camera(){
        if(other_content.getVisibility() == View.VISIBLE){
            key_container.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            other_content.setVisibility(View.INVISIBLE);
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(code.getWindowToken(), 0);
        }
    }
}
