package es.mrjeff.app.android.jeff.integration.legacy;

import android.util.Log;

import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.OrderApi;
import retrofit2.Call;

/**
 * Created by linovm on 7/10/15.
 */
public class SearchCuponCountUsage {


    public ResultServiceCountCoupon getReferral(String idUser, String codeCoupon){
        ResultServiceCountCoupon result = null;
        try {

            Call<ResultServiceCountCoupon> response= ApiServiceGenerator.createService(OrderApi.class).getCouponUse(idUser,codeCoupon);
            result= response.execute().body();

        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }
        return result;
    }
}
