
package es.mrjeff.app.android.jeff.model.nuevo.postal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostalCode {

    @SerializedName("codigo")
    @Expose
    private String codigo;
    @SerializedName("ciudad")
    @Expose
    private String ciudad;
    @SerializedName("pais")
    @Expose
    private String pais;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

}
