
package es.mrjeff.app.android.jeff.model.nuevo.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Oder {

    @SerializedName("order")
    @Expose
    private Order order;

    public Oder(Order order) {
        this.order = order;
    }

    public Oder() {

    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

}
