package es.mrjeff.app.android.jeff.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by CQ on 30/11/2016.
 */

public class LoginStore  {


    Long id;
    String json;

    public LoginStore(Long id, String json) {
        this.id = id;
        this.json = json;
    }

    public LoginStore() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getJson() {
        return this.json;
    }
    public void setJson(String json) {
        this.json = json;
    }
}
