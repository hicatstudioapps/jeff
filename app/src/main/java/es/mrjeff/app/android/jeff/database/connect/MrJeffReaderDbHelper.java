package es.mrjeff.app.android.jeff.database.connect;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.database.util.ConstantsDB;

/**
 * Created by linovm on 18/10/15.
 */
public class MrJeffReaderDbHelper extends SQLiteOpenHelper {

    protected Context context;

    public MrJeffReaderDbHelper(Context context) {
        super(context,
                ConstantsDB.DB_NAME,//String name
                null,//factory
                ConstantsDB.DB_VERSION//int version
        );

        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.v("BD_INIT", "Se inicia la creacion de la BD");
            insertFromFile(db, R.raw.mrjeff);
        } catch (Exception e) {
            Log.e("BD_INIT", "ERROR AL CREAR LA BD",e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public int insertFromFile(SQLiteDatabase db, int resourceId) throws IOException {
        int result = 0;
        InputStream insertsStream = context.getResources().openRawResource(resourceId);
        BufferedReader insertReader = new BufferedReader(new InputStreamReader(insertsStream));
        int i = 0;
        while (insertReader.ready()) {
            String insertStmt = insertReader.readLine();
            db.execSQL(insertStmt);
            result++;
            i++;
        }
        insertReader.close();
        return result;
    }
}
