package es.mrjeff.app.android.jeff.api;

import java.util.ArrayList;

import es.mrjeff.app.android.jeff.model.JeffStageFinish;
import es.mrjeff.app.android.jeff.model.SendJeffPosition;
import es.mrjeff.app.android.jeff.model.b2b.SuscriptionB2b;
import es.mrjeff.app.android.jeff.model.issues.IssueSend;
import es.mrjeff.app.android.jeff.model.issues.Issues;
import es.mrjeff.app.android.jeff.model.login.LoginData;
import es.mrjeff.app.android.jeff.model.products.ProductInfo;
import es.mrjeff.app.android.jeff.model.route.Stage;
import es.mrjeff.app.android.jeff.model.time_table.DateTimeTable;
import es.mrjeff.app.android.jeff.model.time_table.Timetable;
import okhttp3.MultipartBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by CQ on 10/11/2016.
 */

public interface ApiCalls {

//    offline
//    @GET
//    public Call<ResponseBody> getTimeTable(@Url String url);
//
//    @POST("http://www.idontwash.com:8181/authentication")
//    public Call<LoginResult> login(@Body LoginData data);
//
//      @GET("http://192.168.101.1:8081/jeff/route.json")
//      public Observable<Stage> getRoute();
//
//    @Multipart
//    @POST("http://dev.backend.mrjeffapp.net:8080/route-management-service/api/v1/stages/2/signature")
//    public Call<ResponseBody> uploadSignature(@Part MultipartBody.Part file);
//

//    @GET("http://192.168.101.1:8081/jeff/products.json")
//    public Call<ProductInfo> getProduct();
//
//    @GET("http://192.168.101.1:8081/jeff/issues.json")
//    public Call<Issues> getIssues();


//    @POST("http://192.168.101.1:8081/jeff/suscrption.txt")
//    @FormUrlEncoded
//    public Observable<ArrayList<SuscriptionB2b>> getSuscription(@Field("userid") String usrId, @Field("residencecoupon") String residencecoupon);

//    online
    @GET
    public Call<DateTimeTable> getTimeTable(@Url String url);

    @POST("http://dev.backend.mrjeffapp.net/user-service/v1/users/login")
    public Call<ResponseBody> login(@Body LoginData data, @Header("Authorization") String header);

    @GET("route-manager/jeffs/1/stages/current?date=2016-02-26")
    public Observable<Stage> getRoute(@Header("Authorization") String header);

    @Multipart
    @POST("stages/{id}/signature")
    public Call<ResponseBody> uploadSignature(@Path("id") int stageID,@Part MultipartBody.Part file,@Header("Authorization") String header);

    @GET("http://dev.backend.mrjeffapp.net/product-service/v1/api/productTypes/?projection=detailProductType")
    public Call<ProductInfo> getProduct(@Header("Authorization") String header);

    @GET("api/incidentTypes")
    public Call<Issues> getIssues(@Header("Authorization") String header);

    @POST("stages/{stageId}/incident")
    public Call<ResponseBody> registerIssue(@Path("stageId") String stageId, @Body IssueSend issueSend,@Header("Authorization") String header);

    @POST("stages/{stageId}/finish")
    public Call<ResponseBody> finishStage(@Path("stageId") String stageId, @Body JeffStageFinish jeffStageFinish,@Header("Authorization") String header);

    @POST(" http://dev.backend.mrjeffapp.net/tracking-service/v1/users/{jeff_id}/locations")
    public Call<ResponseBody> sendJeffPosition(@Path("jeff_id") String jeff_id, @Body SendJeffPosition sendJeffPosition,@Header("Authorization") String header);

    @POST("http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/cuponb2b2cv2.php")
    @FormUrlEncoded
    public Observable<ArrayList<SuscriptionB2b>> getSuscription(@Field("userid") String usrId, @Field("residencecoupon") String residencecoupon);
}
