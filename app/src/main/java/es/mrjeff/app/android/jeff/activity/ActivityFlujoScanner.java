package es.mrjeff.app.android.jeff.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.fragment.flujos.anadir_al_pedido.WPAnadirAlPedidoFirma;
import es.mrjeff.app.android.jeff.fragment.flujos.anadir_al_pedido.WPAnadirAlPedidoSacanner;
import es.mrjeff.app.android.jeff.fragment.flujos.validar_pedido.ValidarPedidoW1;
import es.mrjeff.app.android.jeff.fragment.flujos.validar_pedido.ValidarPedidoW2;
import es.mrjeff.app.android.jeff.model.Pedido;
import es.mrjeff.app.android.jeff.model.route.Stage;
import me.panavtec.wizard.Wizard;
import me.panavtec.wizard.WizardListener;

public class ActivityFlujoScanner extends AppCompatActivity {

    Stage pedido;
    public static int position;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private Wizard wizard;

    public Stage getPedido(){
        return pedido;
    }

    public Wizard getWizard(){
        return wizard;
    }

    public int getPosition(){
        return position;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flujo_scanner);
        pedido= (Stage) getIntent().getExtras().getSerializable("key");
        position= getIntent().getExtras().getInt("position",0);
        ButterKnife.bind(this);
        toolbar.setContentInsetStartWithNavigation(0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if(! getIntent().getBooleanExtra("nuevo",false)){
        wizard = new Wizard.Builder(this, new ValidarPedidoW1(), new ValidarPedidoW2())
                .containerId(R.id.container)
                .enterAnimation(R.anim.card_slide_right_in)
                .exitAnimation(R.anim.card_slide_left_out)
                .popEnterAnimation(R.anim.card_slide_left_in)
                .popExitAnimation(R.anim.card_slide_right_out)
                .build();}
        else
            wizard= new Wizard.Builder(this, new WPAnadirAlPedidoSacanner(), new WPAnadirAlPedidoFirma())
                    .containerId(R.id.container)
                    .enterAnimation(R.anim.card_slide_right_in)
                    .exitAnimation(R.anim.card_slide_left_out)
                    .popEnterAnimation(R.anim.card_slide_left_in)
                    .popExitAnimation(R.anim.card_slide_right_out)
                    .build();
        wizard.init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()== android.R.id.home)
            onBackPressed();
        return true;
    }
}
