package es.mrjeff.app.android.jeff.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.adapter.RutasAdapter;
import es.mrjeff.app.android.jeff.alarms.service.PositionService;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.issues.IssueType;
import es.mrjeff.app.android.jeff.model.issues.Issues;
import es.mrjeff.app.android.jeff.model.route.Stage;
import es.mrjeff.app.android.jeff.util.RouteManager;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class OrdersActivity extends MenuActivity implements SwipeRefreshLayout.OnRefreshListener,OnMapReadyCallback {

    @BindView(R.id.swipe)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.list)
    RecyclerView orderList;
    @BindView(R.id.emptyrOrder)
    View emptyview;
    @BindView(R.id.content)
    View content;
    MaterialDialog dialog;
    Subscription issuesSubscription;
    GoogleMap googleMap;

    //Reciver que actualizan la lista en dependencia de los cambios en las demas pantalla
    ProductoEntregadoReciver productoEntregadoReciver;
    private RutasAdapter rutasAdapter;
    private SupportMapFragment mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponents();

    }

    private Observable<List<IssueType>> getIssueObservable() {
        return Observable.create(subscriber -> {
            try {
                String autorization=getSharedPreferences("auth",MODE_PRIVATE).getString("data","");
                Issues issues= JeffAplication.apiCalls.getIssues(autorization).execute().body();
                subscriber.onNext(issues.getEmbedded().getIssueTypes());
                subscriber.onCompleted();
            } catch (IOException e) {
                subscriber.onError(e);
                e.printStackTrace();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        issuesSubscription.unsubscribe();
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(productoEntregadoReciver, new IntentFilter(JeffConstants.PEDIDO_ENTRAGADO_FILTER));
        startService(new Intent(this, PositionService.class));
    }

    @Override
    protected int positionMenu() {
        return R.id.orders;
    }

    private void initComponents() {
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        dialog = new MaterialDialog.Builder(this)
                .cancelable(false)
                .progress(true, 0)
                .build();
        productoEntregadoReciver = new ProductoEntregadoReciver();
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(rutasAdapter==null){
                    JeffAplication.routeManager= new RouteManager();
                    JeffAplication.routeManager.init(map,OrdersActivity.this,null);}
                swipeLayout.setRefreshing(false);
            }
        });
        issuesSubscription = getIssueObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<IssueType>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("", "onError: ");
                    }

                    @Override
                    public void onNext(List<IssueType> strings) {
                        JeffAplication.issueTypes = strings;
                    }
                });
    }

    private void findOrders(){
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        FilterOrder filter = new FilterOrder();
//        filter.setDate(format.format(new Date()));
//        filter.setEmailJeff(AuthenticationOpenBravoStorage.getInstance(this).getAuthenticationopenbravo().getJeff().getEmail());
//        FindOrderTask task = new FindOrderTask(this,this);
//        task.execute(filter);
    }

    public void hideMap(){
       content.setVisibility(View.INVISIBLE);
       swipeLayout.setVisibility(View.VISIBLE);
    }

    public void showMap(){
        content.setVisibility(View.VISIBLE);
        swipeLayout.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        swipeLayout.setRefreshing(false);
    }

    GoogleMap map;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;
        if(JeffAplication.testingLocal)
            return;
        if(rutasAdapter==null){
        JeffAplication.routeManager= new RouteManager();
        JeffAplication.routeManager.init(googleMap,this,null);}
    }

    public void initList(Stage stages){
        if(stages==null){
            orderList.removeAllViews();
        }else {
        rutasAdapter= new RutasAdapter(OrdersActivity.this,stages);
        orderList.setAdapter(rutasAdapter);}
    }

    public class ProductoEntregadoReciver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
//            int position= intent.getIntExtra("position",-1);
//            if(position!=-1){
//                Stage pedido=(Stage) intent.getExtras().getSerializable("key");
//                if(pedido.isCompleted())
//                   JeffAplication.routeManager.updateMarkerColor(position, R.drawable.marker_green);
//                else
//                    JeffAplication.routeManager.updateMarkerColor(position, R.drawable.marker_red);
//                rutasAdapter.setItem(pedido,position);
//                rutasAdapter.notifyDataSetChanged();
//            }
            JeffAplication.routeManager= new RouteManager();
            JeffAplication.routeManager.init(map,OrdersActivity.this,null);
        }
    }

}
