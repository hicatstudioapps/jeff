package es.mrjeff.app.android.jeff.model.issues;

import com.annimon.stream.Stream;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 05/12/2016.
 */

public class IssueSend {

    @SerializedName("incidentTypeId")
    @Expose
    private String incidentTypeId;
    @SerializedName("jeffId")
    @Expose
    private String jeffId;
    @SerializedName("cause")
    @Expose
    private String cause;

    public IssueSend(String incidentTypeId, String jeffId, String cause) {
        this.incidentTypeId = incidentTypeId;
        this.jeffId = jeffId;
        this.cause = cause;
    }

    public IssueSend() {

    }

    /**
     *
     * @return
     * The incidentTypeId
     */
    public String getIncidentTypeId() {
        return incidentTypeId;
    }

    /**
     *
     * @param incidentTypeId
     * The incident_type_id
     */
    public void setIncidentTypeId(String incidentTypeId) {
        this.incidentTypeId = incidentTypeId;
    }

    /**
     *
     * @return
     * The jeffId
     */
    public String getJeffId() {
        return jeffId;
    }

    /**
     *
     * @param jeffId
     * The jeff_id
     */
    public void setJeffId(String jeffId) {
        this.jeffId = jeffId;
    }

    /**
     *
     * @return
     * The cause
     */
    public String getCause() {
        return cause;
    }

    /**
     *
     * @param cause
     * The cause
     */
    public void setCause(String cause) {
        this.cause = cause;
    }
}
