package es.mrjeff.app.android.jeff.fragment;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.ActivityFlujoScanner;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ScannerFragment extends Fragment implements ZXingScannerView.ResultHandler {


    private ZXingScannerView mScannerView;
    @BindView(R.id.camera_option)
    View camera_option;
    @BindView(R.id.key_board_option)
    View key_borad_option;
    @BindView(R.id.ohter_content)
    View other_content;
    @BindView(R.id.key_container)
    View key_container;
    @BindView(R.id.options)
    View options_container;
    private Animation anim;
    private PedidoDetalleActivity listener;

    public ScannerFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.code)
    EditText code;

    public static ScannerFragment newInstance(){
        return new ScannerFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        RelativeLayout layout= (RelativeLayout) inflater.inflate(R.layout.fragment_anadir_al_pedido_sacanner, container, false);
        mScannerView = new ZXingScannerView(getContext());
        anim= AnimationUtils.loadAnimation(getContext(),android.R.anim.fade_in);
        ((RelativeLayout)layout.findViewById(R.id.scanner)).addView(mScannerView);
        ButterKnife.bind(this,layout);
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        code.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if(i== EditorInfo.IME_ACTION_DONE){
                    listener.hideScanner();
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(code.getWindowToken(), 0);
                    key_container.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                    other_content.setVisibility(View.INVISIBLE);
                }
                return false;
            }
        });

        //mostramos las opciones 2 segundos despues, para dar tiempo a que se inicialice el scanner
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                options_container.setAnimation(anim);
                options_container.setVisibility(View.VISIBLE);
            }
        },1500);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {

    }

    @OnClick(R.id.key_board_option)
    public void showKeyBoard(){
        if(other_content.getVisibility()!= View.VISIBLE){
            key_container.setBackgroundColor(Color.WHITE);
            other_content.setVisibility(View.VISIBLE);
            //mostramos el teclado
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(code, InputMethodManager.SHOW_IMPLICIT);
        }
    }
    @OnClick(R.id.camera_option)
    public void show_camera(){
        if(other_content.getVisibility() == View.VISIBLE){
            key_container.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            other_content.setVisibility(View.INVISIBLE);
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(code.getWindowToken(), 0);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener=(PedidoDetalleActivity)context;
    }


}
