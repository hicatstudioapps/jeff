package es.mrjeff.app.android.jeff.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.NuevoPedidoCheckOut;
import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.NuevoPedidoDireccion;
import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.NuevoPedidoHorarios;
import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.NuevoPedidoPaid;
import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.NuevoPedidoProductos;
import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.RegistroCliente;
import es.mrjeff.app.android.jeff.integration.OrderProductManager;
import me.panavtec.wizard.Wizard;
import me.panavtec.wizard.WizardListener;
import me.panavtec.wizard.WizardPage;
import me.panavtec.wizard.WizardPageListener;

public class NuevoPedidoActivity extends MenuActivity implements StepperLayout.StepperListener {

    private StepperLayout mStepperLayout;

    public static boolean pedido_minimo=false;

    int type=0; //0- pedido normal,1- pedido suscripcion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        type= getIntent().getIntExtra("type",0);
        initComp();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void setContentViewActivity() {
        setContentView(R.layout.activity_nuevo_pedido);
    }

    @Override
    protected int positionMenu() {
        return R.id.nuevo_pedido;
    }

    void initComp() {
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new SampleFragmentStepAdapter(getSupportFragmentManager(), this), 0);
        mStepperLayout.setListener(this);
    }

    @Override
    public void onCompleted(View completeButton) {

    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    public void nextPage(){
        mStepperLayout.setCurrentStepPosition(mStepperLayout.getCurrentStepPosition()+1);
    }

    public void restart(){
        mStepperLayout.setCurrentStepPosition(0);
    }

    public void gotoRoute(){
        startActivity(new Intent(this,OrdersActivity.class));
        finish();
    }
    class SampleFragmentStepAdapter extends AbstractFragmentStepAdapter {

        public SampleFragmentStepAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
            super(fm, context);
        }

        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            switch (position) {
                case 0:
                    return new StepViewModel.Builder(context)
                            .setTitle("Cliente")
                            .create();
                case 1:
                    return new StepViewModel.Builder(context)
                            .setTitle("Productos")
                            .create();
                case 2:
                    return new StepViewModel.Builder(context)
                            .setTitle("Dirección")
                            .create();
                case 3:
                    return new StepViewModel.Builder(context)
                            .setTitle("Horarios")
                            .create();
                case 4:
                    return new StepViewModel.Builder(context)
                            .setTitle("Confirmar")
                            .create();
                case 5:
                    return new StepViewModel.Builder(context)
                            .setTitle("Pago")
                            .create();
                default:
                    return null;
            }


        }

        @Override
        public Step createStep(int position) {
            switch (position) {
                case 0:
                    return RegistroCliente.newInstance(type);
                case 1:
                    return new NuevoPedidoProductos();
                case 2:
                    return NuevoPedidoDireccion.newInstance(type);
                case 3:
                    return NuevoPedidoHorarios.newInstance(type);
                case 4:
                    return NuevoPedidoCheckOut.newInstance(type);
                case 5:
                    return NuevoPedidoPaid.newInstance(type);
                default:
                  return   new NuevoPedidoCheckOut();
            }
        }

        @Override
        public int getCount() {
            return 6;
        }
    }
}
