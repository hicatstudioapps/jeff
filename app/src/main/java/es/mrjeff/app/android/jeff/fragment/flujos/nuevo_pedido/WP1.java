package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;

import android.support.v4.app.Fragment;

import me.panavtec.wizard.WizardPage;

/**
* Created by CQ on 13/10/2016.
*/
public class    WP1 extends WizardPage<Fragment> {
   @Override
   public Fragment createFragment() {
       return new NuevoPedidoProductos();
   }
}
