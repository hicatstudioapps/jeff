package es.mrjeff.app.android.jeff.view.components;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

import es.mrjeff.app.android.jeff.R;


/**
 * Created by Paulino on 05/04/2016.
 */

public class ProgressDialogMrJeff extends ProgressDialog {

    private AnimationDrawable animation;
    private MrJeffTextView textView;
    private CharSequence message;
    private Handler handler = new Handler();
    private ThreadProgress thread;

    public static ProgressDialogMrJeff createAndStart(Context context) {
        return createAndStart(context, R.string.waitdefault);
    }

    public static ProgressDialogMrJeff createAndStart(Context context, int idMessage) {
        ProgressDialogMrJeff instance = new ProgressDialogMrJeff(context, R.style.ProgressBar);
        instance.setCanceledOnTouchOutside(false);
        instance.setProgressStyle(R.style.ProgressBar);
        instance.setCancelable(false);
        instance.setMessage(context.getString(idMessage));
      //  instance.show();
        return instance;
    }

    public ProgressDialogMrJeff(Context context) {
        super(context);
    }

    public ProgressDialogMrJeff(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_mrjeff);

        ImageView la = (ImageView) findViewById(R.id.animation);
        la.setBackgroundResource(R.drawable.waitanim);
        animation = (AnimationDrawable) la.getBackground();

        textView = (MrJeffTextView) findViewById(R.id.progresstext);
        textView.setText(message);
    }

   @Override
    public void show() {
        super.show();
        thread = new ThreadProgress(this);
        Thread threadExecute = new Thread(thread);
        threadExecute.start();
        animation.start();
    }

    @Override
    public void cancel() {
        dismiss();
    }

    @Override
    public void hide() {
         dismiss();
    }

    @Override
    public void dismiss() {
        animation.stop();
        super.dismiss();
        thread.setContinuos(false);
    }

    @Override
    public void setMessage(CharSequence message) {
        this.message = message;
    }

    public void setMessageDefault() {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(message);
                    }
                });
            }
        });
        threadUpdate.start();
    }

    public void setNewMessageResource(final int idMessage) {
        Thread threadUpdate = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(textView.getContext().getString(idMessage));
                    }
                });
            }
        });
        threadUpdate.start();
    }


    private class ThreadProgress implements Runnable {


        private boolean continuos = true;
        private int[] idsMessage = {R.string.progress1,R.string.progress2,R.string.progress3};
        private int pos = 0;
        private boolean messageLife = false;
        private ProgressDialogMrJeff progress;

        public ThreadProgress(ProgressDialogMrJeff progress) {
            this.progress = progress;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(messageLife ? 1000 : 3000);
                progress.setNewMessageResource(idsMessage[pos%idsMessage.length]);
                pos++;
                messageLife=true;
                while (isContinuos()) {
                    Thread.sleep(messageLife ? 1000 : 3000);
                    if(progress != null && progress.isShowing()){
                        if(!messageLife) {
                            progress.setNewMessageResource(idsMessage[pos % idsMessage.length]);
                            pos++;
                            messageLife = true;
                        } else {
                            progress.setMessageDefault();
                            messageLife = false;
                        }
                    }
                }
            } catch (Exception e) {

            }
        }

        public synchronized boolean isContinuos() {
            return continuos;
        }

        public synchronized void setContinuos(boolean continuos) {
            this.continuos = continuos;
        }
    }


}
