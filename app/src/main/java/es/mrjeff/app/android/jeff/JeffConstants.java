package es.mrjeff.app.android.jeff;

import com.annimon.stream.Stream;
import com.google.maps.internal.StringJoin;

import retrofit2.http.PUT;

/**
 * Created by CQ on 20/10/2016.
 */

public class JeffConstants {

    public static String PEDIDO_ENTRAGADO_FILTER="pedido_entregado";
//    flujos del pedido CODIGOS DE RETORNO ON RESULT
    public static int FLUJO_NUEVO_PEDIDO=25;
    public static int FLUJO_ANADIR_PEDIDO =26;
    public static int FLUJO_RECOGER=27;
    public static int PEDIDO_MINIMO=20;

    ///URL Woocommerce
    public static String WOOCOMERCE_BASE_URL="https://mrjeffapp.com/wc-api";
    public static String TOKEN_consumer_key="ck_04ef92d3a3ff36ccd8427d24f289c47c";
    public static String TOKEN_consumer_secret="cs_0b609d5cc4cd3d199cd3ba927b4b5ce7";
}
