package es.mrjeff.app.android.jeff.integration;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.dao.JeffCacheDao;
import es.mrjeff.app.android.jeff.model.nuevo.customer.Customer;
import es.mrjeff.app.android.jeff.model.nuevo.customer.Customer_;

/**
 * Created by admin on 06/02/2017.
 */

public class CustomerManager extends GenericManager {

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer_ customer) {
        this.customer.setCustomer(customer);
        save();
    }

    Customer customer= new Customer();
    private static CustomerManager mInstance = null;
    private static Context mCtx;

    private CustomerManager(Context context){
        mCtx = context;
    }

    public static synchronized CustomerManager getInstance(Context context){
        if(mInstance == null){
            mInstance = new CustomerManager(context);
            mInstance.loadCache();
        }
        else
            mCtx = context;
        return mInstance;
    }

    @Override
    public void save() {
        JeffCacheDao jeffCacheDao= JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache= jeffCacheDao.load(1L);
        jeffCache.setJeff_data(new Gson().toJson(this, CustomerManager.class));
        jeffCacheDao.updateInTx(jeffCache);
    }

    @Override
    public void loadCache() {
        JeffCacheDao jeffCacheDao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = jeffCacheDao.load(1L);
        String offline = jeffCache.getJeff_data();
        if (offline == null && TextUtils.isEmpty(offline))
            customer = new Customer();
        else {
            CustomerManager offCustomerManager = new Gson().fromJson(offline, CustomerManager.class);
            customer = offCustomerManager.customer;
        }
    }
}
