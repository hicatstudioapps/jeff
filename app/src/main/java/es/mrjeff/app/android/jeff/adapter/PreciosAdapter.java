package es.mrjeff.app.android.jeff.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.mrjeff.app.android.jeff.R;

/**
 * Created by CQ on 12/10/2016.
 */

public class PreciosAdapter extends RecyclerView.Adapter<PreciosAdapter.Viewolder> {

    Context context;

    public PreciosAdapter(Context context) {
        this.context = context;
    }

    @Override
    public Viewolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.price_row,null);
//        RecyclerView.LayoutParams lp= new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        view.setLayoutParams(lp);
        return new Viewolder(view);
    }

    @Override
    public void onBindViewHolder(Viewolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 15;
    }

    public class Viewolder extends RecyclerView.ViewHolder{
        public Viewolder(View itemView) {
            super(itemView);
        }
    }
}
