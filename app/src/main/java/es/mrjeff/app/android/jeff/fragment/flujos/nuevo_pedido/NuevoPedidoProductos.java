package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.gson.Gson;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.math.BigDecimal;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.NuevoPedidoActivity;
import es.mrjeff.app.android.jeff.adapter.ProductAdapter;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.integration.OrderProductManager;
import es.mrjeff.app.android.jeff.integration.ProductManager;
import es.mrjeff.app.android.jeff.model.nuevo.order.LineItem;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product_;
import es.mrjeff.app.android.jeff.util.DialogManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class NuevoPedidoProductos extends Fragment implements ProductAdapter.UpdateSumary, Step {

    private static final String KEY_OFFER = "zpacks";
    private static final String KEY_LAV = "zlavanderia";
    private static final String KEY_NO_LAVO = "newsuscriptions";
    private static final String KEY_TIN = "ztintoreria";
    private static final String KEY_HOME = "zcasa";

    @BindView(R.id.product_container)
    LinearLayout product_container;
    @BindView(R.id.sumary_container)
    View sumary_container;

    //Aqui guardamos el precio total a cobrar
    @BindView(R.id.total)
    TextView total_sumary;
    BigDecimal totalPrice;

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.ofertas_i)
    ImageView ofertasI;
    @BindView(R.id.ofertas_t)
    TextView ofertasT;
    @BindView(R.id.ofertas_c)
    LinearLayout ofertasC;
    @BindView(R.id.lavanderia_i)
    ImageView lavanderiaI;
    @BindView(R.id.lavanderia_t)
    TextView lavanderiaT;
    @BindView(R.id.lavanderia_c)
    LinearLayout lavanderiaC;
    @BindView(R.id.tintureria_i)
    ImageView tintureriaI;
    @BindView(R.id.tintoreria_t)
    TextView tintoreriaT;
    @BindView(R.id.tintoreria_c)
    LinearLayout tintoreriaC;
    @BindView(R.id.casa_i)
    ImageView casaI;
    @BindView(R.id.casa_t)
    TextView casaT;
    @BindView(R.id.casa_c)
    LinearLayout casaC;
    @BindView(R.id.next)
    ImageView next;
    @BindView(R.id.flujo)
    FrameLayout flujo;
    private Dialog dialog;
    ProductManager productManager;
    private NuevoPedidoActivity listener;
    String un_selected_color = "#27688f";
    String selected_color = "#ffffff";
    int selected_resource;
    LinearLayout last_selected;


    public NuevoPedidoProductos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        selected_resource=R.drawable.ico_ofertas_activado;
        last_selected= ofertasC;
        return view;
    }

    private void startLoad() {
        dialog = DialogManager.getLoadingDialog(getActivity(), getString(R.string.productos_cargando));
        dialog.show();
        productManager = ProductManager.getInstance(getActivity());
        if (productManager.getCache() == null) {
            loadFromWeb();
        } else {
            initAdapter();
        }
    }

    private void initAdapter() {
        pager.setOffscreenPageLimit(5);
        pager.setAdapter(new PagerAdapter(getChildFragmentManager()));
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 0:
                        onClick(ofertasC);
                        break;
                    case 1:
                        onClick(lavanderiaC);
                        break;
                    case 2:
                        onClick(tintoreriaC);
                        break;
                    case 3:
                        onClick(casaC);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        totalPrice = OrderProductManager.getInstance(getContext()).getPriceTotal();
        total_sumary.setText(totalPrice.toString());
        dialog.hide();
    }

    private void loadFromWeb() {
        Observable.create(subscriber -> {
            Call<ResponseBody> call = ProductManager.getProduct(null);
            try {
                Response<ResponseBody> responseBodyResponse = call.execute();
                String json = responseBodyResponse.body().string();
                List<Product_> result = Stream.of(new Gson().fromJson(json, Product.class).getProduct()).filter(value -> value.getTags() != null && value.getTags().size() > 0).collect(Collectors.toList());
                productManager = ProductManager.getInstance(getActivity());
                productManager.createCache(result);
                subscriber.onNext(null);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        //   Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Object product) {
                        Toast.makeText(getActivity(), "next", Toast.LENGTH_SHORT).show();
                        initAdapter();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(pager.getAdapter()!= null){
            pager.setCurrentItem(0);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ofertasT.setTypeface(JeffAplication.rr);
        casaT.setTypeface(JeffAplication.rr);
        tintoreriaT.setTypeface(JeffAplication.rr);
        lavanderiaT.setTypeface(JeffAplication.rr);
        total_sumary.setTypeface(JeffAplication.rb);
    }

    @OnClick(R.id.next)
    public void next() {
        if (totalPrice.compareTo(new BigDecimal(0.0)) == 0) {
            showEmtyCart();
        } else {
            if (totalPrice.compareTo(new BigDecimal(JeffConstants.PEDIDO_MINIMO)) == -1)
                new MaterialDialog.Builder(getActivity()).content("El pedido mínimo es de 20 euros. Se cobrará un cargo adicional" +
                        " a su pedido hasta alcanzar dicho monto.")
                        .onPositive((dialog1, which) -> {
                            dialog1.dismiss();
                            NuevoPedidoActivity.pedido_minimo = true;
                            addLineItemToOrder();
                            listener.nextPage();
                        })
                        .positiveText("Ok").build().show();
            else {
                NuevoPedidoActivity.pedido_minimo = false;
                addLineItemToOrder();
                listener.nextPage();
            }
        }
    }

    private void addLineItemToOrder() {
        Observable.create(subscriber -> {
            OrderManager orderManager = OrderManager.getInstance(getContext());
            List<LineItem> lineItems = OrderProductManager.getInstance(getContext()).getLineItem();
            orderManager.getOrder().setLineItems(Stream.of(lineItems).filter(value -> value.getQuantity() > 0).collect(Collectors.toList()));
            orderManager.save();
        }).subscribeOn(Schedulers.computation())
                .subscribe(o -> {
                });
    }

    private void showEmtyCart() {
        DialogManager.showInfo(getContext(), getString(R.string.empty_cart)).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (NuevoPedidoActivity) context;
    }


    public String getlabel(int position) {
        switch (position) {
            case 0:
                return KEY_OFFER;
            case 1:
                return KEY_LAV;
            case 2:
                return KEY_TIN;
            case 3:
                return KEY_HOME;
            case 4:
                return KEY_HOME;
            default:
                return KEY_NO_LAVO;
        }
    }

    @Override
    public void updateSumary(float total) {
        totalPrice = totalPrice.add(new BigDecimal(total).setScale(2, BigDecimal.ROUND_HALF_EVEN));
        total_sumary.setText(totalPrice.floatValue() + " €");
        OrderProductManager.getInstance(getContext()).setPriceTotal(totalPrice.floatValue() + "");
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        startLoad();
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @OnClick({R.id.lavanderia_c, R.id.tintoreria_c,R.id.ofertas_c,R.id.casa_c})
    public void onClick(View view) {
        ((TextView)last_selected.getChildAt(1)).setTextColor(Color.parseColor(un_selected_color));
        switch (selected_resource){
            case R.drawable.ico_ofertas_activado:
                ((ImageView)last_selected.getChildAt(0)).setImageResource(R.drawable.ico_ofertas_desactivado);
                break;
            case R.drawable.ico_lavanderia_activo:
                ((ImageView)last_selected.getChildAt(0)).setImageResource(R.drawable.ico_lavanderia_desactivado);
                break;
            case R.drawable.ico_tintoreria_activado:
                ((ImageView)last_selected.getChildAt(0)).setImageResource(R.drawable.ico_tintoreria_desactivado);
                break;
            case R.drawable.ico_casa_activado:
                ((ImageView)last_selected.getChildAt(0)).setImageResource(R.drawable.ico_casa_desactivado);
                break;
        }
        switch (view.getId()) {
            case R.id.ofertas_c:
                ofertasI.setImageResource(R.drawable.ico_ofertas_activado);
                ofertasT.setTextColor(Color.parseColor(selected_color));
                selected_resource= R.drawable.ico_ofertas_activado;
                last_selected= (LinearLayout) view;
                pager.setCurrentItem(0);
                break;
            case R.id.lavanderia_c:
                lavanderiaI.setImageResource(R.drawable.ico_lavanderia_activo);
                lavanderiaT.setTextColor(Color.parseColor(selected_color));
                selected_resource= R.drawable.ico_lavanderia_activo;
                last_selected= (LinearLayout) view;
                pager.setCurrentItem(1);
                break;
            case R.id.tintoreria_c:
                tintureriaI.setImageResource(R.drawable.ico_tintoreria_activado);
                tintoreriaT.setTextColor(Color.parseColor(selected_color));
                selected_resource= R.drawable.ico_tintoreria_activado;
                last_selected= (LinearLayout) view;
                pager.setCurrentItem(2);
                break;
            case R.id.casa_c:
                casaI.setImageResource(R.drawable.ico_casa_activado);
                casaT.setTextColor(Color.parseColor(selected_color));
                selected_resource= R.drawable.ico_casa_activado;
                last_selected= (LinearLayout) view;
                pager.setCurrentItem(3);
                break;
        }
    }

    class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            ProductFragment fragment = ProductFragment.newInstance(getlabel(position), "");
            fragment.setUpdateSumary(NuevoPedidoProductos.this);
            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}





