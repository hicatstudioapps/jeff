package es.mrjeff.app.android.jeff.util;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import es.mrjeff.app.android.jeff.R;

/**
 * Created by admin on 27/01/2017.
 */

public class DialogManager {

    public static MaterialDialog getLoadingDialog(Context context,String text){
//        View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loading_view_gid, null);
//        ((TextView)view.findViewById(R.id.text)).setText(text);
//        Dialog loading = new Dialog(context, R.style.MaterialDialogSheet);
//        loading.setContentView(view);
//        loading.setCancelable(false);
//        loading.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        loading.getWindow().setGravity(Gravity.CENTER_VERTICAL);

        return new MaterialDialog.Builder(context)
                .content(text)
                .progress(true,0)
                .cancelable(false)
                .build();
    }

    public static MaterialDialog showInfo(Context context,String text){
        return new MaterialDialog.Builder(context).content(text)
                .positiveText("Ok").build();

    }
}
