
package es.mrjeff.app.android.jeff.model.issues;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Issues {

    @SerializedName("_embedded")
    @Expose
    private Embedded embedded;


    /**
     * 
     * @return
     *     The embedded
     */
    public Embedded getEmbedded() {
        return embedded;
    }

    /**
     * 
     * @param embedded
     *     The _embedded
     */
    public void setEmbedded(Embedded embedded) {
        this.embedded = embedded;
    }


}
