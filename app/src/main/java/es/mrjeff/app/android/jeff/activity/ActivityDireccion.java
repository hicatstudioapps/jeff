package es.mrjeff.app.android.jeff.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.api.ApiCalls;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.model.time_table.DateTimeTable;
import es.mrjeff.app.android.jeff.model.time_table.Timetable;
import es.mrjeff.app.android.jeff.view.components.TimePickerView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDireccion extends AppCompatActivity {

    private static final int REQUEST_CODE_AUTOCOMPLETE = 2323;
    @BindView(R.id.direccion)
    EditText direccion;
    @BindView(R.id.recogida_dia)
    TextView recogida_dia;
    @BindView(R.id.recogida_hora)
    TextView recogida_hora;
    @BindView(R.id.entrega_dia)
    TextView entrega_dia;
    @BindView(R.id.entrega_hora)
    TextView entrega_hora;

    private Dialog dialog_pickerRecogida;
    TimePickerView timePickerViewRecogida;
    private Dialog dialog_pickerEntrega;
    TimePickerView timePickerViewEntrega;
    private DateTimeTable dateTimeTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direccion);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(findViewById(R.id.editText5).getWindowToken(), 0);
        dialog_pickerRecogida = new Dialog(ActivityDireccion.this, R.style.MaterialDialogSheetCenter);
        dialog_pickerRecogida.setContentView(getLayoutInflater().inflate(R.layout.time_picker_dialog, null));
        dialog_pickerRecogida.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog_pickerRecogida.getWindow().setGravity(Gravity.BOTTOM);
        timePickerViewRecogida = ((TimePickerView) dialog_pickerRecogida.findViewById(R.id.time_picker_view));
        ((TextView) dialog_pickerRecogida.findViewById(R.id.type)).setText("Recogida");
        dialog_pickerRecogida.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateRecogida(timePickerViewRecogida.getHourSelected(), timePickerViewRecogida.getDateSelected());
                generateDeliveryDates();
                dialog_pickerRecogida.dismiss();
            }
        });
        dialog_pickerEntrega = new Dialog(ActivityDireccion.this, R.style.MaterialDialogSheetCenter);
        dialog_pickerEntrega.setContentView(getLayoutInflater().inflate(R.layout.time_picker_dialog, null));
        dialog_pickerEntrega.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog_pickerEntrega.getWindow().setGravity(Gravity.BOTTOM);
        timePickerViewEntrega = ((TimePickerView) dialog_pickerEntrega.findViewById(R.id.time_picker_view));
        ((TextView) dialog_pickerEntrega.findViewById(R.id.type)).setText("Entrega");
        dialog_pickerEntrega.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEntrega(timePickerViewEntrega.getHourSelected(), timePickerViewEntrega.getDateSelected());
                dialog_pickerEntrega.dismiss();
            }
        });
        getDateTimeTable();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return false;
    }

    @OnClick(R.id.direccion)
    public void launchSearch() {
        openAutocompleteActivity();
    }

    private void openAutocompleteActivity() {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setBoundsBias(new LatLngBounds(new LatLng(40.417160, -3.703561), new LatLng(40.417160, -3.703561)))
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.finish)
    public void finishh() {
        Intent result = new Intent();
        Bundle data = new Bundle();
        result.putExtras(data);
        setResult(Activity.RESULT_OK, result);
        finish();
    }

    @OnClick(R.id.recogida_time)
    public void recogidaTiem() {
        dialog_pickerRecogida.show();
    }

    @OnClick(R.id.entrega_time)
    public void entregaTime() {
        dialog_pickerEntrega.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = PlaceAutocomplete.getPlace(this, data);

                Log.i("Place Selected", "Place Selected: " + place.getName());
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());

                try {
                    List<Address> list = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);
                    if (!list.isEmpty()) {
                        String addressName = list.get(0).getAddressLine(0);
                        direccion.setText(addressName);
                        String postalCode = list.get(0).getPostalCode();
                        String city = list.get(0).getLocality();
                        formatPlaceDetails(getResources(), place.getName(), place.getId(), place.getAddress(), place.getPhoneNumber(),
                                place.getWebsiteUri(), city, postalCode, addressName);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Format the place's details and display them in the TextView.
//                mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
//                        place.getId(), place.getAddress(), place.getPhoneNumber(),
//                        place.getWebsiteUri()));

                // Display attributions if required.
//                CharSequence attributions = place.getAttributions();
//                if (!TextUtils.isEmpty(attributions)) {
//                    mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
//                } else {
//                    mPlaceAttribution.setText("");
//                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e("error", "Error: Status = " + status.toString());
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For example if
                // the user pressed the back button.
            }
        }
    }

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri, String city, String code, String add) {
        Log.e("data", res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri, city, code, address));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri, city, code, address));

    }

    public void getDateTimeTable() {
        ApiCalls call = ApiServiceGenerator.createService(ApiCalls.class);
        String url = "http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/timetablev2.php?postalCode=28014";
//        url = "http://192.168.101.1:8081/jeff/time.json";
        call.getTimeTable(url).enqueue(new Callback<DateTimeTable>() {
            @Override
            public void onResponse(Call<DateTimeTable> call, Response<DateTimeTable> response) {
                if (response.isSuccessful()) {
                    try {
                        dateTimeTable = response.body();
                        timePickerViewRecogida.initData(dateTimeTable.getTimetable(), true);
                        initInitialDateRecogida(dateTimeTable.getTimetable().get(0));
                        //llenamos el picker de entrega con el valor por defecto del de recogida
                        List<Timetable> timetable = getDeliveryDates(dateTimeTable.getTimetable()
                                .get(0).getDay(), Integer.parseInt(dateTimeTable.getTimetable().get(0).getMinTiming()));
                        timePickerViewEntrega.initData(timetable, false);
                        initInitialDateEntrega(timetable.get(0));
                    } catch (Exception e) {
                        onFailure(call, null);
                    }
                } else {
                    onFailure(call, null);
                }
            }

            @Override
            public void onFailure(Call<DateTimeTable> call, Throwable t) {
                MaterialDialog error_Dialog = new MaterialDialog.Builder(ActivityDireccion.this)
                        .title("No está conectado")
                        .content("Por favor revise sus ajustes y vuelva a intentarlo cuando tenga acceso a internet.")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                ActivityDireccion.this.finish();
                            }
                        })
                        .cancelable(false)
                        .positiveText("OK")
                        .show();
            }
        });
    }

    private void initInitialDateEntrega(Timetable timetable) {
        updateEntrega(timetable.getDropoffTimetable().split(";")[0], timetable.getDay());
    }

    private void updateEntrega(String hour, String day) {
        entrega_hora.setText(hour);
        entrega_dia.setText(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(day).toString("dd, MMM"));
    }


    private void initInitialDateRecogida(Timetable timetable) {
        updateRecogida(timetable.getPickupTimetable().split(";")[0], timetable.getDay());
    }

    public List<Timetable> getDeliveryDates(String date, int minDays) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(date);
        DateTime plus = dateTime.plusDays(minDays);
        if (plus.getDayOfWeek() == DateTimeConstants.SUNDAY)
            plus = plus.plusDays(1);
        String date_to_search = plus.toString("yyyy-MM-dd");
        List<Timetable> result = new ArrayList<>();
        for (int i = 0; i < dateTimeTable.getTimetable().size(); i++) {
            if (dateTimeTable.getTimetable().get(i).getDay().equals(date_to_search)) {
                result = dateTimeTable.getTimetable().subList(i, dateTimeTable.getTimetable().size());
                break;
            }
        }
        updateEntrega(result.get(0).getDropoffTimetable().split(";")[0],result.get(0).getDay());
        return result;
    }

    public void updateRecogida(String hour, String date) {
        recogida_hora.setText(hour);
        recogida_dia.setText(DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(date).toString("dd, MMM"));

    }

    private void generateDeliveryDates() {
        timePickerViewEntrega.initData(getDeliveryDates(timePickerViewRecogida.getDateSelected()
                , timePickerViewRecogida.getMin_day()), false);
    }
}
