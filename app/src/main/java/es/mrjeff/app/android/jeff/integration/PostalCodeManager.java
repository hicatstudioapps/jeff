package es.mrjeff.app.android.jeff.integration;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.PostalCodeApi;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.dao.JeffCacheDao;
import es.mrjeff.app.android.jeff.model.nuevo.postal.PostalCode;
import es.mrjeff.app.android.jeff.util.DialogManager;
import okhttp3.Call;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 06/02/2017.
 */

public class PostalCodeManager extends GenericManager {

    //pais-lista de codigos
    Map<String,Set<String>> postalCodes= new HashMap<>();
    //codigo-ciudad
    Map<String,String> city= new HashMap<>();


    private static PostalCodeManager mInstance = null;
    private static Context mCtx;

    private PostalCodeManager(Context context){
        mCtx = context;
    }

    public static synchronized PostalCodeManager getInstance(Context context){
        if(mInstance == null){
            mInstance = new PostalCodeManager(context);
            mInstance.loadCache();
        }
        else
            mCtx = context;
        return mInstance;
    }


    public void addPostaCode (String country, String cp, String city){
        Set<String> cps = postalCodes.get(country);
        if(cps == null){
            cps = new HashSet<String>();
        }

        cps.add(cp);
        postalCodes.put(country, cps);
        this.city.put(cp,city);
    }

    public boolean find(String country, String cp){
        Boolean result = Boolean.FALSE;

        Boolean haveCountry = postalCodes.get(country) !=null &&  !postalCodes.get(country).isEmpty();
        if(haveCountry){
            result = postalCodes.get(country).contains(cp);
        }

        return result;

    }

    public String getCity(String cp){
        return city.get(cp);
    }

    public int count(){
        return postalCodes.size();
    }



    @Override
    public void save() {
        JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = dao.load(1L);
        jeffCache.setPostal_codes(new Gson().toJson(this, PostalCodeManager.class));
        dao.updateInTx(jeffCache);
    }

    @Override
    public void loadCache() {
        try {
            JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
            JeffCache jeffCache = dao.load(1L);
            String json = jeffCache.getPostal_codes();
            if(json==null || TextUtils.isEmpty(json))
                throw new Exception("");
            PostalCodeManager offline = new Gson().fromJson(json, PostalCodeManager.class);
            postalCodes= offline.postalCodes;
            city= offline.city;
        } catch (Exception e) {
                Observable.create(subscriber -> {
                retrofit2.Call<ResponseBody> call=ApiServiceGenerator.createService(PostalCodeApi.class).getPostalCodes();
                try {
                    Response<ResponseBody> responseBodyResponse= call.execute();
                    String json= responseBodyResponse.body().string();
                    List<PostalCode> list = new Gson().fromJson(json,new TypeToken<List<PostalCode>>(){}.getType());
                    for (PostalCode postalCode :
                            list) {
                        addPostaCode(postalCode.getPais(), postalCode.getCodigo(), postalCode.getCiudad());
                    }
                    save();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(o -> {});
            e.printStackTrace();
            return;
        }
    }
}
