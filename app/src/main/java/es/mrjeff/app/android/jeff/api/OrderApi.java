package es.mrjeff.app.android.jeff.api;

import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.NuevoPedidoHorarios;
import es.mrjeff.app.android.jeff.integration.legacy.ResultCoupon;
import es.mrjeff.app.android.jeff.integration.legacy.ResultSearchCoupon;
import es.mrjeff.app.android.jeff.integration.legacy.ResultServiceCountCoupon;
import es.mrjeff.app.android.jeff.model.nuevo.ChargeStripe;
import es.mrjeff.app.android.jeff.model.nuevo.order.Oder;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product_;
import es.mrjeff.app.android.jeff.model.nuevo.product.SendProducts;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by admin on 08/02/2017.
 */

public interface OrderApi {

    @POST("https://mrjeffapp.com/wc-api/v2/orders?filter[meta]=true")
    public Call<Oder> createOrder(@Query("consumer_key") String consumer_key, @Query("consumer_secret")String consumer_secret, @Body Oder order);

    @POST("https://mrjeffapp.com/wc-api/v2/products")
    public Call<SendProducts> createMinProduct(@Query("consumer_key") String consumer_key, @Query("consumer_secret")String consumer_secret, @Body SendProducts order);

    @POST
    public Call<ResponseBody> paidStripe(@Url String url, @Body ChargeStripe body);

    @PUT("https://mrjeffapp.com/wc-api/v2/orders/{id}")
    public Observable<Oder> updateOrder(@Path("id") String id, @Query("consumer_key") String consumer_key, @Query("consumer_secret")String consumer_secret, @Body Oder order);

    @GET("https://mrjeffapp.com/wc-api/v2/coupons/code/{coupon}")
    public Call<ResultSearchCoupon> getCoupon(@Path("coupon") String coupon, @Query("consumer_key") String consumer_key, @Query("consumer_secret")String consumer_secret);

    @POST("http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/couponperuserv2.php")
    public Call<ResultServiceCountCoupon> getCouponUse(@Field("ID") String userID, @Field("Cupon") String coupon);

}
