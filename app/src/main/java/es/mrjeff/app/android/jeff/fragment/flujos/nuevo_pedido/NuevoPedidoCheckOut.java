package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.NuevoPedidoActivity;
import es.mrjeff.app.android.jeff.adapter.CheckOutAdapter;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.OrderApi;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.integration.SuscriptionB2BManager;
import es.mrjeff.app.android.jeff.integration.legacy.CouponWorker;
import es.mrjeff.app.android.jeff.integration.legacy.ResultCoupon;
import es.mrjeff.app.android.jeff.model.nuevo.order.Oder;
import es.mrjeff.app.android.jeff.model.nuevo.order.Order;
import es.mrjeff.app.android.jeff.util.DialogManager;
import retrofit2.Call;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class NuevoPedidoCheckOut extends Fragment implements Step, Observer {

    Handler handler = new Handler();
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.editText7)
    EditText cupon;
    @BindView(R.id.textView14)
    TextView textView14;
    @BindView(R.id.descuento)
    TextView descuento;
    @BindView(R.id.total_i)
    TextView totalI;
    @BindView(R.id.textView30)
    TextView textView30;
    @BindView(R.id.bottom)
    LinearLayout bottom;
    @BindView(R.id.imageView19)
    ImageView imageView19;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.finish)
    RelativeLayout finish;
    @BindView(R.id.cupon_container)
    LinearLayout cuponContainer;
    private NuevoPedidoActivity listener;
    private MaterialDialog dialog;
    private int type;

    public NuevoPedidoCheckOut() {
        // Required empty public constructor
    }

    public static NuevoPedidoCheckOut newInstance(int param1) {
        NuevoPedidoCheckOut fragment = new NuevoPedidoCheckOut();
        Bundle args = new Bundle();
        args.putInt("type", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type= getArguments().getInt("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nuevo_pedido_check_out, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void init() {
        Order order = OrderManager.getFinalOrder();
        if(type==1){
            cuponContainer.setVisibility(View.INVISIBLE);
            BigDecimal totall= new BigDecimal(OrderManager.getFinalOrder().getTotal()).setScale(2,BigDecimal.ROUND_HALF_EVEN);
            totall= totall.subtract(new BigDecimal(SuscriptionB2BManager.getInstance(null).getSuscriptionB2b().getDiscountNormalOrder()
                ).setScale(2, RoundingMode.HALF_EVEN));
            total.setText("€ " + totall.floatValue());
        }else
            total.setText("€ " + (Float.parseFloat(order.getTotal()) - Float.parseFloat(order.getTotalDiscount())));
        cupon.setTypeface(JeffAplication.rr);
        textView30.setTypeface(JeffAplication.rr);
        total.setTypeface(JeffAplication.rb);
        totalI.setTypeface(JeffAplication.rb);
        descuento.setTypeface(JeffAplication.rb);
        textView14.setTypeface(JeffAplication.rb);
        list.setAdapter(new CheckOutAdapter(order.getLineItems(), this));

        //Toast.makeText(getActivity(),order.getId(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (NuevoPedidoActivity) context;
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        init();
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @OnClick(R.id.finish)
    public void onClickPagar() {
        listener.nextPage();
    }

    @Override
    public void update(Observable observable, Object data) {
        if (data instanceof ResultCoupon) {
            switch (((ResultCoupon) data).getCodeResponse()) {
                case CouponWorker.RESULT_INIT:
                    break;
                case CouponWorker.RESULT_ERROR_COUPON_NOT_FOUND:
                    showErrorCouponNotFound(R.string.couponnotfound);
                    break;
                case CouponWorker.RESULT_ERROR_APPLY_DISCOUNT:
                    showErrorCouponNotFound(R.string.couponnotvalid);
                    break;
                case CouponWorker.RESULT_ERROR_RECOVERY_ORDER:
                    showErrorCouponNotFound(R.string.couponnotvalid);
                    break;
                case CouponWorker.RESULT_ERROR_EXPIRED:
                    showErrorCouponNotFound(R.string.couponexpired);
                    break;
                case CouponWorker.RESULT_ERROR_AMOUNT:
                    showErrorCouponNotFoundVariable(R.string.couponamount, ((ResultCoupon) data).getAmount());
                    break;
                case CouponWorker.RESULT_ERROR_USAGE_LIMIT:
                    showErrorCouponNotFound(R.string.couponusagelimit);
                    break;
                case CouponWorker.RESULT_ERROR_USAGE_LIMIT_USER:
                    showErrorCouponNotFound(R.string.couponusagelimituser);
                    break;
                case CouponWorker.RESULT_ERROR_USER_NOT_VALID:
                    showErrorCouponNotFound(R.string.couponusernotvalid);
                    break;
                case CouponWorker.RESULT_ERROR_PRODUCT_NOT_VALID:
                    showErrorCouponNotFound(R.string.couponproductnotvalid);
                    break;
                case CouponWorker.RESULT_ERROR_REFERRAL_NOT_VALID:
                    showErrorCouponNotFound(R.string.couponreferraltnotvalid);
                    break;
                case CouponWorker.RESULT_SUCCESS:
                    OrderManager orderManager = OrderManager.getInstance(null);
                    orderManager.setCoupon((ResultCoupon) data);
                    orderManager.save();
                    Thread threadProgress = new Thread(() -> {
                        handler.post(() -> {
                            init();
                            cupon.setEnabled(false);
                            textView30.setEnabled(false);
                            dialog.dismiss();
                        });
                    });
                    threadProgress.start();
                    break;
                default:
                    break;
            }
        }
    }

    private void showErrorCouponNotFound(int couponusagelimit) {
        Thread threadProgress = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        new MaterialDialog.Builder(getContext())
                                .content(getString(couponusagelimit))
                                .positiveText("Ok").show();
                    }
                });
            }
        });
        threadProgress.start();
    }

    private void showErrorCouponNotFoundVariable(final int idMessage, Object value) {
        Thread threadProgress = new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        final String message = getResources().getText(idMessage).toString().replace("$$", value.toString());
                        new MaterialDialog.Builder(getContext())
                                .content(message)
                                .positiveText("Ok").show();
                    }
                });
            }
        });
        threadProgress.start();

    }

    @OnClick(R.id.textView30)
    public void onClick() {
        if (TextUtils.isEmpty(cupon.getText().toString())) {
            new MaterialDialog.Builder(getContext())
                    .content("Por favor introduzca un codigo de cupón.")
                    .positiveText("Ok")
                    .show();
        } else {
            dialog = DialogManager.getLoadingDialog(getContext(), "Calculando descuento...");
            dialog.show();
            CouponWorker couponWorker = new CouponWorker(cupon.getText().toString());
            couponWorker.addObserver(this);
            Thread thread = new Thread(couponWorker);
            thread.start();
        }
    }

    public void deleteCoupon() {
        dialog = DialogManager.getLoadingDialog(getContext(), "Eliminando cupón...");
        dialog.show();
        rx.Observable.create((rx.Observable.OnSubscribe<Boolean>) subscriber -> {
            Oder oder = new Oder(OrderManager.getFinalOrder().getOrderNoCopuon());
            String json = new Gson().toJson(oder, Oder.class);
            Call<Oder> call = ApiServiceGenerator.createService(OrderApi.class).createOrder(JeffConstants.TOKEN_consumer_key, JeffConstants.TOKEN_consumer_secret, oder);
            try {
                oder = call.execute().body();
                if (oder != null) {
                    OrderManager.saveFinalOrder(oder.getOrder());
                    OrderManager orderManager = OrderManager.getInstance(null);
                    orderManager.setCoupon(null);
                    orderManager.save();
                    subscriber.onNext(true);
                    subscriber.onCompleted();
                } else throw new Exception("sdsds");
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
                subscriber.onCompleted();
            }

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        showCouponDeleteError();
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            init();
                            cupon.setEnabled(true);
                            textView30.setEnabled(true);
                            cupon.setText("");
                            dialog.dismiss();
                        }
                    }
                });
    }

    void showCouponDeleteError() {
        new MaterialDialog.Builder(getContext())
                .content("Ha ocurrido un error al eliminar el cupón.")
                .positiveText("Volver a intentar")
                .negativeText("Cancelar")
                .onPositive((dialog1, which) -> deleteCoupon())
                .onNegative((dialog1, which) -> dialog1.dismiss()).show();
    }


}
