package es.mrjeff.app.android.jeff.model;

import java.io.Serializable;

/**
 * Created by CQ on 05/11/2016.
 */

public class Pedido implements Serializable{

    String accion;
    boolean incidencia;
    boolean entregado;
    boolean recogido;

    public Pedido(String accion, boolean incidencia, boolean entregado, boolean recogido) {
        this.accion = accion;
        this.incidencia = incidencia;
        this.entregado = entregado;
        this.recogido = recogido;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public boolean isIncidencia() {
        return incidencia;
    }

    public void setIncidencia(boolean incidencia) {
        this.incidencia = incidencia;
    }

    public boolean isEntregado() {
        return entregado;
    }

    public void setEntregado(boolean entregado) {
        this.entregado = entregado;
    }

    public boolean isRecogido() {
        return recogido;
    }

    public void setRecogido(boolean recogido) {
        this.recogido = recogido;
    }
}
