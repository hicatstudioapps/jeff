package es.mrjeff.app.android.jeff.fragment.flujos.anadir_al_pedido;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.activity.ActivityFlujoScanner;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.util.Utils;
import es.mrjeff.app.android.jeff.widget.SlideToUnlock;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnadirAlPedidoFirma extends Fragment implements SlideToUnlock.OnUnlockListener{


    public AnadirAlPedidoFirma() {
        // Required empty public constructor
    }

    @BindView(R.id.slidetounlock)
    SlideToUnlock slideToUnlock;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout=inflater.inflate(R.layout.fragment_anadir_al_pedido_firma, container, false);
        ButterKnife.bind(this,layout);
        slideToUnlock.setOnUnlockListener(this);
        return layout;
    }

    @Override
    public void onUnlock() {
        final Dialog d= new Dialog(getContext(),R.style.MaterialDialogSheetCenter);
        d.setContentView(R.layout.dialog_succes);
        d.setCancelable(false);
        d.getWindow ().setLayout ((int) (Utils.getScreenWidth(getActivity())*0.6), LinearLayout.LayoutParams.WRAP_CONTENT);
        d.getWindow ().setGravity (Gravity.CENTER_HORIZONTAL);
        d.show();
        new Handler().postDelayed(new Runnable() {
           @Override
           public void run() {
               Intent result= new Intent();
               Bundle data= new Bundle();
               data.putSerializable("key",((ActivityFlujoScanner)getActivity()).getPedido());
               data.putInt("position",ActivityFlujoScanner.position);
               result.putExtras(data);
               getActivity().setResult(Activity.RESULT_OK,result);
               getActivity().finish();
               d.dismiss();
           }
       },1500);
    }
}
