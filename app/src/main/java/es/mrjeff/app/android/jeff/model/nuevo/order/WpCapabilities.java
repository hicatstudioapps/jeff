
package es.mrjeff.app.android.jeff.model.nuevo.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WpCapabilities {

    @SerializedName("customer")
    @Expose
    private Boolean customer;

    public Boolean getCustomer() {
        return customer;
    }

    public void setCustomer(Boolean customer) {
        this.customer = customer;
    }

}
