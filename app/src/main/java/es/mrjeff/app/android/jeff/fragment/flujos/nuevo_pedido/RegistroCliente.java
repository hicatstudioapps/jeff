package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.AppLaunchChecker;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.NuevoPedidoActivity;
import es.mrjeff.app.android.jeff.alarms.Sheduler;
import es.mrjeff.app.android.jeff.api.ApiCalls;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.CustomerApi;
import es.mrjeff.app.android.jeff.integration.CustomerManager;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.integration.SuscriptionB2BManager;
import es.mrjeff.app.android.jeff.model.b2b.SuscriptionB2b;
import es.mrjeff.app.android.jeff.model.nuevo.customer.Customer;
import es.mrjeff.app.android.jeff.model.nuevo.customer.Customer_;
import es.mrjeff.app.android.jeff.util.DialogManager;
import es.mrjeff.app.android.jeff.util.FormValidatorMrJeff;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegistroCliente extends Fragment implements Step {

    @BindView(R.id.textView11)
    TextView textView11;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.passw1)
    EditText passw1;
    @BindView(R.id.passw2)
    EditText passw2;
    @BindView(R.id.registro)
    LinearLayout registro;
    Dialog dialog;
    @BindView(R.id.textView13)
    TextView textView13;
    @BindView(R.id.email2)
    EditText email2;
    @BindView(R.id.buscar)
    LinearLayout buscar;
    NuevoPedidoActivity listener;
    VerificationError error = null;
    @BindView(R.id.textView31)
    TextView textView31;
    @BindView(R.id.imageView19)
    ImageView imageView19;
    @BindView(R.id.next)
    RelativeLayout next;
    @BindView(R.id.cupon)
    EditText cupon;
    private int type;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (NuevoPedidoActivity) context;
    }

    public static RegistroCliente newInstance(int type) {
        RegistroCliente registroCliente = new RegistroCliente();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        registroCliente.setArguments(bundle);
        return registroCliente;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type= getArguments().getInt("type");
    }

    public RegistroCliente() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registro_cliente, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void init() {
        textView11.setTypeface(JeffAplication.rb);
        textView31.setTypeface(JeffAplication.rb);
        textView13.setTypeface(JeffAplication.rb);
        name.setTypeface(JeffAplication.rr);
        email.setTypeface(JeffAplication.rr);
        passw1.setTypeface(JeffAplication.rr);
        passw2.setTypeface(JeffAplication.rr);
        email2.setTypeface(JeffAplication.rr);
        //mostrar dialogo de busqueda de cliente si no es una suscripcion
        if(type==0)
        new MaterialDialog.Builder(getActivity())
                .content(getString(R.string.registro_inicial_text))
                .negativeText(getString(R.string.registro_nuevo))
                .positiveText(getString(R.string.registro_existe))
                .onPositive((dialog1, which) -> {
                    dialog1.dismiss();
                    buscar.setAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left));
                    buscar.setVisibility(View.VISIBLE);
                })
                .onNegative((dialog1, which) -> {
                    dialog1.dismiss();
                    registro.setAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left));
                    registro.setVisibility(View.VISIBLE);
                }).build().show();
        else {
            textView13.setText("B2B");
            cupon.setVisibility(View.VISIBLE);
            buscar.setAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.slide_in_left));
            buscar.setVisibility(View.VISIBLE);
        }
        email.setOnFocusChangeListener((view, b) -> {
            if (!b && !FormValidatorMrJeff.isValidEmail(email.getText().toString())) {
                YoYo.with(Techniques.Shake).duration(1500).playOn(view);
                email.setError(getString(R.string.errorformatmail));
                error = new VerificationError("");
                verifyStep();
            }
        });
        name.setOnFocusChangeListener((view, b) -> {
            if (!b && !FormValidatorMrJeff.validRequiredMin(5, name.getText().toString())) {
                YoYo.with(Techniques.Shake).duration(1500).playOn(view);
                name.setError(getString(R.string.errorname));
            }
        });
        passw1.setOnFocusChangeListener((view, b) -> {
            if (!b && !FormValidatorMrJeff.validRequiredMin(6, passw1.getText().toString())) {
                YoYo.with(Techniques.Shake).duration(1500).playOn(view);
                passw1.setError(getString(R.string.errorformatpassword));
            }
        });
        passw2.setOnFocusChangeListener((view, b) -> {
            if (!b && !FormValidatorMrJeff.validRequiredMin(6, passw2.getText().toString())) {
                YoYo.with(Techniques.Shake).duration(1500).playOn(view);
                passw2.setError(getString(R.string.errorformatpassword));
            }
            if (!passw1.getText().toString().equals(passw2.getText().toString())) {
                YoYo.with(Techniques.Shake).duration(1500).playOn(view);
                passw2.setError(getString(R.string.errorformatrepass));
            }
        });
        email2.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE || i== EditorInfo.IME_ACTION_NEXT){
                if(type==1)
                    cupon.requestFocus();
                else
                    onClick();
            }
            return true;
        });
    }

    public void createWooComerceCustomer() {

        Observable.create(subscriber -> {
            CustomerApi customerApi = ApiServiceGenerator.createService(CustomerApi.class);
            Customer_ customer_ = new Customer_();
            customer_.setEmail(email.getText().toString().trim());
            customer_.setFirstName(name.getText().toString());
            customer_.setPassword(passw1.getText().toString());
            Customer customer = new Customer();
            customer.setCustomer(customer_);
            String data = new Gson().toJson(customer, Customer.class);
            Call<ResponseBody> call = customerApi.registerCustomer(JeffConstants.TOKEN_consumer_key, JeffConstants.TOKEN_consumer_secret, customer);
            try {
                Response<ResponseBody> body = call.execute();
                if (body.isSuccessful()) {
                    subscriber.onNext(new Gson().fromJson(body.body().string(), Customer.class).getCustomer());
                    subscriber.onCompleted();
                } else {
//                    Log.e("Error buscando customer",body.body().string());
                    subscriber.onError(new Throwable(getString(R.string.registro_mail_existente)));
                }
            } catch (IOException e) {
                subscriber.onError(new Throwable("Ha ocurrido un error en la conexión. Vuelva a intentarlo."));
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        DialogManager.showInfo(getActivity(), e.getMessage()).show();
                    }

                    @Override
                    public void onNext(Object o) {
                        Toast.makeText(getActivity(), "Cliente: " + ((Customer_) o).getFirstName()
                                + " Correo: " + ((Customer_) o).getEmail() + " ID: " + ((Customer_) o).getId(), Toast.LENGTH_LONG).show();
                        CustomerManager customerManager = CustomerManager.getInstance(getContext());
                        customerManager.setCustomer((Customer_) o);
                        OrderManager orderManager = OrderManager.getInstance(getContext());
                        orderManager.getOrder().setCustomerId(((Customer_) o).getId());
                        orderManager.save();
                        listener.nextPage();
                        dialog.dismiss();
                    }
                });
    }


    private void searchWooCommerceCustomer() {
        Observable.create(subscriber -> {
            CustomerApi customerApi = ApiServiceGenerator.createService(CustomerApi.class);
            Call<ResponseBody> call = customerApi.findCustomer(email2.getText().toString().trim(), JeffConstants.TOKEN_consumer_key, JeffConstants.TOKEN_consumer_secret);
            try {
                Response<ResponseBody> body = call.execute();
                if (body.isSuccessful()) {
                    subscriber.onNext(new Gson().fromJson(body.body().string(), Customer.class).getCustomer());
                    subscriber.onCompleted();
                } else {
                    subscriber.onError(new Throwable(getString(R.string.registro_mail_no_existe)));
                }
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(new Throwable("Ha ocurrido un error en la conexión. Vuelva a intentarlo."));
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .startWith(showDialog())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        dialog.dismiss();
                        DialogManager.showInfo(getActivity(), e.getMessage()).show();
                    }

                    @Override
                    public void onNext(Object o) {
                        CustomerManager customerManager = CustomerManager.getInstance(getContext());
                        customerManager.setCustomer((Customer_) o);
                        customerManager.save();
                        Toast.makeText(getActivity(), "Cliente: " + ((Customer_) o).getFirstName()
                                + " Correo: " + ((Customer_) o).getEmail() + " ID: " + ((Customer_) o).getId(), Toast.LENGTH_LONG).show();
                        if(type==0){
                        OrderManager orderManager = OrderManager.getInstance(getContext());
                        orderManager.getOrder().setCustomerId(((Customer_) o).getId());
                        orderManager.save();
                        dialog.dismiss();
                        listener.nextPage();
                        }else
                            getSuscription(((Customer_) o).getId(), cupon.getText().toString().trim());
                    }
                });
    }

    private void getSuscription(int id, String cup) {
        ApiServiceGenerator.createService(ApiCalls.class).getSuscription(String.valueOf(id), cup)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<SuscriptionB2b>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(ArrayList<SuscriptionB2b> suscriptionB2bb) {
                        SuscriptionB2b suscriptionB2b= suscriptionB2bb.get(0);
                        if(suscriptionB2b != null && suscriptionB2b.getCodigo().toLowerCase().equals("correcto")){
                            //guardamos suscripcion y creamos orden
                            SuscriptionB2BManager suscriptionB2BManager= SuscriptionB2BManager.getInstance(null);
                            suscriptionB2BManager.setSuscriptionB2b(suscriptionB2b);
                            suscriptionB2BManager.save();
                            OrderManager orderManager = OrderManager.getInstance(getContext());
                            orderManager.getOrder().setCustomerId(CustomerManager.getInstance(null).getCustomer().getCustomer().getId());
                            orderManager.save();
                            dialog.dismiss();
                            listener.nextPage();
                        }else {
                            dialog.dismiss();
                            DialogManager.showInfo(getContext(), suscriptionB2b.getMensaje()).show();
                        }
                    }
                });
    }

    Observable showDialog() {
        return Observable.create(subscriber -> {
            dialog = DialogManager.getLoadingDialog(getActivity(), getString(R.string.registro_buscando));
            dialog.show();
            subscriber.onCompleted();
        });
    }

    @Override
    public VerificationError verifyStep() {
        return error;
    }

    @Override
    public void onSelected() {
        init();
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @OnClick(R.id.next)
    public void onClick() {
        if(JeffAplication.testingLocal){
            listener.nextPage();
            return;
        }
        if (registro.getVisibility() == View.VISIBLE) {
            if (JeffAplication.testingLocal) {
                OrderManager orderManager = OrderManager.getInstance(getContext());
                orderManager.getOrder().setCustomerId(CustomerManager.getInstance(getContext()).getCustomer().getCustomer().getId());
                orderManager.save();
                listener.nextPage();
                return;
            }
            if (TextUtils.isEmpty(email.getText().toString()) || TextUtils.isEmpty(name.getText().toString())
                    || TextUtils.isEmpty(passw1.getText().toString()) || TextUtils.isEmpty(passw2.getText().toString()))
                DialogManager.showInfo(getActivity(), getString(R.string.modalErrorFormText)).show();
            else {
                dialog = DialogManager.getLoadingDialog(getActivity(), getString(R.string.registro_cliente_loading_registrando));
                dialog.show();
                createWooComerceCustomer();
            }
        } else {
            if (TextUtils.isEmpty(email2.getText().toString()))
                DialogManager.showInfo(getActivity(), getString(R.string.modalErrorFormText)).show();
            else if (!FormValidatorMrJeff.isValidEmail(email2.getText().toString())) {
                YoYo.with(Techniques.Shake).duration(1500).playOn(email2);
                email2.setError(getString(R.string.errorformatmail));
            } else if(type==1 && TextUtils.isEmpty(cupon.getText())){
                DialogManager.showInfo(getActivity(), getString(R.string.modalErrorFormText)).show();

            }else searchWooCommerceCustomer();
        }
    }
}
