package es.mrjeff.app.android.jeff.alarms.service;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;

import java.io.IOException;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.model.SendJeffPosition;
import es.mrjeff.app.android.jeff.util.RouteManager;
import okhttp3.ResponseBody;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
@SuppressWarnings("WrongConstant")
public class PositionService extends Service  {

//    private LocationManager mlocManager;
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Context context = getApplicationContext();
//
//        if(AuthenticationOpenBravoStorage.getInstance(context).isLogin()){
//            mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//            mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
//            Location lastLocation = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            sendLocation(lastLocation);
//        }
//        return super.onStartCommand(intent,flags,startId);
//    }
//
//    @Override
//    public void onLocationChanged(Location location) {
//        sendLocation(location);
//    }
//
//    private void sendLocation(Location location){
//        if(location==null)
//            return;
//        Context context = getApplicationContext();
//        if(AuthenticationOpenBravoStorage.getInstance(context).isLogin()) {
//            double latitude = location.getLatitude();
//            double longitude = location.getLongitude();
//            Log.d("position",latitude+""+longitude);
//            JeffPosition jeffPosition = new JeffPosition();
//            jeffPosition.setJeff(AuthenticationOpenBravoStorage.getInstance(context).getAuthenticationopenbravo().getJeff());
//            jeffPosition.setLongitude(longitude+"");
//            jeffPosition.setLatitude(latitude+"");
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ssZZZ");
//            jeffPosition.setCurDate(format.format(new Date()));
//            jeffPosition.setCity(AuthenticationOpenBravoStorage.getInstance(context).getAuthenticationopenbravo().getJeff().getCityJeff());
//            JeffPositionTask task = new JeffPositionTask(context);
//            task.execute(jeffPosition);
//        }
//    }
//
//    @Override
//    public void onStatusChanged(String provider, int status, Bundle extras) {
//
//    }
//
//    @Override
//    public void onProviderEnabled(String provider) {
//
//    }
//
//    @Override
//    public void onProviderDisabled(String provider) {
//
//    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(120000);
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(getApplicationContext());
        Subscription subscription = locationProvider.getUpdatedLocation(request)
                .onBackpressureBuffer()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Location>() {
                    @Override
                    public void onCompleted() {
                        Toast.makeText(getApplicationContext(),"Error on position",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"Error on position",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Location location) {
                        Toast.makeText(getApplicationContext(),"send on position",Toast.LENGTH_SHORT);
                           consumeLocationApi(location);
                        }

                });
        return 1;
    }

    void consumeLocationApi(Location location) {
        if (RouteManager.local) {
            Observable.create(new Observable.OnSubscribe<Object>() {
                @Override
                public void call(Subscriber<? super Object> subscriber) {
                    String autorization=getSharedPreferences("auth",MODE_PRIVATE).getString("data","");
                    Call<ResponseBody> call=   JeffAplication.apiCalls.sendJeffPosition(JeffAplication.loginResult.getId() + "",
                            new SendJeffPosition(40.299157, -3.922529),autorization);
                    try {
                        Response<ResponseBody> responseBody= call.execute();
                        subscriber.onNext(null);
                    } catch (IOException e) {
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                }
            }).subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Object>() {
                        @Override
                        public void onCompleted() {
                            Log.d("POSITION", "posicion enviada: ");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("POSITION", "error enviando posicion ");
                        }

                        @Override
                        public void onNext(Object o) {
                            Log.d("POSITION", "posicion enviada: ");
                        }
                    });
        } else if(location!=null){
            Observable.create(new Observable.OnSubscribe<Object>() {
                @Override
                public void call(Subscriber<? super Object> subscriber) {
                    String autorization=getSharedPreferences("auth",MODE_PRIVATE).getString("data","");
                    Call<ResponseBody> call=   JeffAplication.apiCalls.sendJeffPosition(JeffAplication.loginResult.getId() + "",
                            new SendJeffPosition(location.getLatitude(), location.getLongitude()),autorization);
                    try {
                        Response<ResponseBody> responseBody= call.execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<Object>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Object o) {

                        }
                    });
        }
    }

//    private static PendingIntent getPendingIntent(Context var0) {
//        return PendingIntent.getService(var0, 0, new Intent(var0, JsonUpdateService.class), PendingIntent.FLAG_UPDATE_CURRENT);
//    }
//
//    public static void stopSchedule(Context var0) {
//        PendingIntent var1 = getPendingIntent(var0);
//        Sheduler.getAlarmManager(var0).cancel(var1);
//    }
//
//    public static void scheduleService(final Context var0, final long var1) {
//
//        new Thread(new Runnable() {
//            public void run() {
//                stopSchedule(var0);
//                Calendar cal = Calendar.getInstance();
//                cal.setTimeInMillis(System.currentTimeMillis());
//                cal.add(Calendar.SECOND, 15);
//                Sheduler.getAlarmManager(var0).set(1, cal.getTimeInMillis(), getPendingIntent(var0));
//            }
//        }).start();
//    }

}
