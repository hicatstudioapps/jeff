package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.adapter.ProductAdapter;
import es.mrjeff.app.android.jeff.integration.ProductManager;


public class ProductFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.list)
    RecyclerView list;
    ProductAdapter.UpdateSumary updateSumary;

    public void setUpdateSumary(ProductAdapter.UpdateSumary updateSumary) {
        this.updateSumary = updateSumary;
    }

    // TODO: Rename and change types of parameters
    private String cat;
    private String mParam2;


    public ProductFragment() {
        // Required empty public constructor
    }


    public static ProductFragment newInstance(String cat, String param2) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, cat);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cat = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.cat)).setText(cat);
        list.setAdapter(new ProductAdapter(getActivity(),ProductManager.getInstance(null).getCatProduct(cat),updateSumary));

    }
}
