package es.mrjeff.app.android.jeff.integration.legacy;

/**
 * Created by linovm on 17/11/15.
 */
public class Coupon_lines {

    private String code;
    private String amount;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Coupon_lines() {
    }

    public Coupon_lines(Coupon_lines item) {
        super();
        if(item != null){
            this.code = item.getCode();
            this.amount = item.getAmount();
        }
    }
}
