package es.mrjeff.app.android.jeff.view.components;

import android.content.Context;
import android.support.annotation.IntegerRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.model.products.Product;

/**
 * Created by CQ on 12/10/2016.
 */

public class MrJeffItemProduct extends LinearLayout {

    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.minus)
    View btn_minus;
    @BindView(R.id.plus)
    View btn_plus;
    @BindView(R.id.count)
    TextView tv_count;
    @BindView(R.id.name)
    TextView tv_name;

    public double getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(int precioBase) {
        this.precioBase = precioBase;
    }

    double precioBase=24;

    public MrJeffItemProduct(Context context, Product product) {
        super(context);
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.mrjeff_item_product,this,true);
        ButterKnife.bind(this,view);
        precioBase= product.getPrice();
        tv_name.setText(product.getName());
    }

    public MrJeffItemProduct(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.mrjeff_item_product,this,true);
        ButterKnife.bind(this,view);
    }

    public MrJeffItemProduct(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @OnClick(R.id.plus)
    public void plusClick(){
        tv_count.setText((Integer.parseInt(tv_count.getText().toString())+1)+"");
        total.setText((Integer.parseInt(tv_count.getText().toString())* precioBase)+" €");
        btn_minus.setVisibility(VISIBLE);
        tv_count.setVisibility(VISIBLE);
        total.setVisibility(VISIBLE);
    }

    @OnClick(R.id.minus)
    public void minusClick(){
        int count=(Integer.parseInt(tv_count.getText().toString())-1);
        tv_count.setText((Integer.parseInt(tv_count.getText().toString())-1)+"");
        total.setText((Integer.parseInt(tv_count.getText().toString())* precioBase)+" €");
        if(count==0){
            total.setVisibility(INVISIBLE);
            btn_minus.setVisibility(INVISIBLE);
            tv_count.setVisibility(INVISIBLE);
        }

    }

    public int getProductCount(){
        return Integer.parseInt(tv_count.getText().toString());
    }

    public void setProductName(String name){
        tv_name.setText(name);
    }

    public double getPrecioTotal(){
        return Double.parseDouble(tv_count.getText().toString())*precioBase;
    }
}
