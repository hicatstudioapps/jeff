package es.mrjeff.app.android.jeff.integration;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.integration.legacy.ResultCoupon;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.dao.JeffCacheDao;
import es.mrjeff.app.android.jeff.model.nuevo.order.Order;

/**
 * Created by admin on 07/02/2017.
 */

public class OrderManager extends GenericManager {

    Order order;
    private Integer productDelete = null;

    public Integer getProductDelete() {
        return productDelete;
    }

    public void setProductDelete(Integer productDelete) {
        this.productDelete = productDelete;
    }

    public Integer getProductMinOrder() {
        return productMinOrder;
    }

    public void setProductMinOrder(Integer productMinOrder) {
        this.productMinOrder = productMinOrder;
    }

    public ResultCoupon getCoupon() {
        return coupon;
    }

    public void setCoupon(ResultCoupon coupon) {
        this.coupon = coupon;
    }

    private Integer productMinOrder = null;
    private ResultCoupon coupon = null;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    private static OrderManager mInstance = null;
    private static Context mCtx;

    private OrderManager(Context context){
        mCtx = context;
    }

    public static synchronized OrderManager getInstance(Context context){
        if(mInstance == null) {
            mInstance = new OrderManager(context);

        }
        else
            mCtx = context;
        mInstance.loadCache();
        return mInstance;
    }

    @Override
    public void save() {
        JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = dao.load(1L);
        jeffCache.setOrder(new Gson().toJson(this, OrderManager.class));
        dao.updateInTx(jeffCache);
    }

    @Override
    public void loadCache() {
        try {
            JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
            JeffCache jeffCache = dao.load(1L);
            String json = jeffCache.getOrder();
            if(json==null || TextUtils.isEmpty(json)){
                order= new Order();
                return;
            }
            OrderManager offline = new Gson().fromJson(json, OrderManager.class);
            order = offline.order;
            productDelete= offline.getProductDelete();
            coupon= offline.getCoupon();
            productMinOrder= offline.getProductMinOrder();
        } catch (Exception e) {
            e.printStackTrace();
            order= new Order();
            return;
        }
    }

    public static void saveFinalOrder(Order order){
        JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = dao.load(1L);
        jeffCache.setFinal_order(new Gson().toJson(order,Order.class));
        dao.updateInTx(jeffCache);
    }

    public static Order getFinalOrder(){
        JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = dao.load(1L);
        return new Gson().fromJson(jeffCache.getFinal_order(),Order.class);
    }
}
