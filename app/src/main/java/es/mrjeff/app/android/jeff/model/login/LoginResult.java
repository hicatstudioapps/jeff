
package es.mrjeff.app.android.jeff.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LoginResult {


    @SerializedName("id")
    @Expose
    private Integer jeff;
    @SerializedName("enabled")
    @Expose
    private boolean enable;
    @SerializedName("token_expired")
    @Expose
    private String token_expired;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("headquarter_id")
    @Expose
    private Integer headquarterId;

     /**
     *
     * @return
     *     The id
     */
    public Integer getId() {
        return jeff;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.jeff = id;
    }

    /**
     *
     * @return
     *     The enable
     */
    public boolean getEnable() {
        return enable;
    }

    /**
     *
     * @param enable
     *     The enable
     */
    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    /**
     *
     * @return
     *     The token_expired
     */
    public String getToken_expired() {
        return token_expired;
    }

    /**
     *
     * @param token_expired
     *     The token_expired
     */
    public void setToken_expired(String token_expired) {
        this.token_expired = token_expired;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     *     The last_name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     *     The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     *     The phone_number
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     *     The headquarterId
     */
    public Integer getHeadquarterId() {
        return headquarterId;
    }

    /**
     *
     * @param headquarterId
     *     The headquarter_id
     */
    public void setHeadquarterId(Integer headquarterId) {
        this.headquarterId = headquarterId;
    }

    public Integer getjeff() {
        return this.jeff;
    }

    public void setjeff(Integer jeff) {
        this.jeff = jeff;
    }

    public Integer getJeff() {
        return this.jeff;
    }

    public void setJeff(Integer jeff) {
        this.jeff = jeff;
    }


}
