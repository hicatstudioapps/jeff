package es.mrjeff.app.android.jeff.util;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.TravelMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import es.mrjeff.app.android.jeff.Dummy;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.OrdersActivity;
import es.mrjeff.app.android.jeff.model.Pedido;
import es.mrjeff.app.android.jeff.model.SendJeffPosition;
import es.mrjeff.app.android.jeff.model.route.Address;
import es.mrjeff.app.android.jeff.model.route.Stage;
import okhttp3.ResponseBody;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by CQ on 08/11/2016.
 * Clase que gestiona el calculo y la representacion de las rutas en el mapa
 * desde aqui podemos gestionar los dialogos de carga de info
 */

public class RouteManager {

    GoogleMap googleMap;
    float zoom_level;
    Context context;
    MaterialDialog info_dialog;
    DirectionsResult direction_result;
    List<Marker> markers= new ArrayList<>();
    Marker jeff_position=null;
    List<Stage> stages;
    int selectedMarker=-1;
    Location last_position;
    Stage routeData;
   public static boolean local=true;

    private static RouteManager instance;
    private LatLng start_location;

    public static RouteManager getInstance() {
        if(instance==null)
            return new RouteManager();
        else
        return null;
//        return instance == null ? instance = new RouteManager() : instance;
    }

    public void movoMapToPosition(int position){
        Marker marker=markers.get(position);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        marker.showInfoWindow();
    }

    public void updateMarkerColor(int position, int color){
        if(markers != null && markers.size()>0){
        markers.get(position).setIcon(BitmapDescriptorFactory.fromResource(color));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(markers.get(position).getPosition()));}
    }

   public void init(GoogleMap map, Context context,@Nullable MaterialDialog mProgress){
       map.clear();
       info_dialog= new MaterialDialog.Builder(context)
               .cancelable(false)
               .content("Obteniedo rutas..")
               .progress(true,0)
               .build();
        info_dialog.show();
        zoom_level= (float) (map.getMaxZoomLevel()*0.8);
        googleMap=map;
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Getting view from the layout file info_window_layout
                View v = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_custom_info, null);
                Stage current_stage= routeData;
                TextView name= ((TextView)v.findViewById(R.id.name));
                TextView address= ((TextView)v.findViewById(R.id.address));
                TextView phone= ((TextView)v.findViewById(R.id.phone));
                name.setText(current_stage.getCustomer().getName()+" "+current_stage.getCustomer().getLastName());
                address.setText(current_stage.getAddress().getAddressLine());
                phone.setText("Tel: "+current_stage.getCustomer().getPhoneNumber());
                return v;
            }
        });
        this.context=context;
        String autorization=context.getSharedPreferences("auth",MODE_PRIVATE).getString("data","");
        JeffAplication.apiCalls.getRoute(autorization)
               .subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(new Observer<Stage>() {
                   @Override
                   public void onCompleted() {
                       if(routeData!=null){
                        ((OrdersActivity)context).initList(routeData);
                           ((OrdersActivity)context).showMap();
                            getCurrentLocation();
//                           info_dialog.dismiss();
                       }
                      else {
                           info_dialog.dismiss();
                           ((OrdersActivity)context).hideMap();
                           ((OrdersActivity)context).initList(null);
                       }
                   }

                   @Override
                   public void onError(Throwable e) {
                       info_dialog.dismiss();
                       ((OrdersActivity)context).hideMap();
                   }

                   @Override
                   public void onNext(Stage routeData) {
                       RouteManager.this.routeData=routeData;
                   }
               });

    }

    class LoadRoute extends AsyncTask<Stage,Object,Integer>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            info_dialog.setContent("Calculando ruta...");
        }

        @Override
        protected void onPostExecute(Integer pedidos) {
            PolylineOptions polylineOptions= new PolylineOptions();
            super.onPostExecute(pedidos);
            Stream.of(routeData).forEach(new Consumer<Stage>() {
                @Override
                public void accept(Stage stage) {
                    MarkerOptions markerOptions= new MarkerOptions()
                            .position(new LatLng(stage.getAddress().getLocation().get(0),stage.getAddress().getLocation().get(1)))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_blue));
                    Marker marker= googleMap.addMarker(markerOptions);
                    Log.d("position", markers.size()+"");
                    marker.setTitle(markers.size()+"");
                    markers.add(marker);
                }
            });
            if(pedidos == 0){
                info_dialog.dismiss();
                Toast.makeText(context,"Fallo calculando rutas.",Toast.LENGTH_SHORT).show();
                return;
            }
            //por ahora suponemos que siempre hay rutas
            for (DirectionsLeg leg : direction_result.routes[0].legs) {
                for (DirectionsStep step :
                        leg.steps) {
                    for (com.google.maps.model.LatLng latLng:
                         step.polyline.decodePath()) {
                        polylineOptions.add(new LatLng(latLng.lat,latLng.lng));
                    }
                }}

            polylineOptions.color(context.getResources().getColor(R.color.colorPrimary));
            googleMap.addPolyline(polylineOptions);
            moveCameraToCurrentLocation();
            info_dialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
            if(values[0] instanceof Integer)
            info_dialog.setContent("Calculando rutas..");
            else
            RouteManager.this.direction_result= (DirectionsResult) values[0];
        }

        @Override
        protected Integer doInBackground(Stage... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress(0);
            List<String> markers= com.annimon.stream.Stream.of(voids[0]).map(Stage::getAddress).map(new Function<Address, String>() {
                @Override
                public String apply(Address address) {
                    return address.getLocation().get(0)+"," + address.getLocation().get(1);
                }
            }).collect(Collectors.toList());
            //
            Log.d("origin", markers.get(0));
            Log.d("destination", markers.get(markers.size()-1));
            String [] way_points= markers.toArray(new String[markers.size()]);
            DirectionsApiRequest directionsApiRequest= DirectionsApi.newRequest(new GeoApiContext().setApiKey(context.getString(R.string.google_maps_key)));
            if(!local){
            directionsApiRequest.origin(new com.google.maps.model.LatLng(start_location.latitude,start_location.longitude))
                    .destination(markers.get(0))
                    .mode(TravelMode.DRIVING);}
            else{
                directionsApiRequest.origin(new com.google.maps.model.LatLng(40.299157,-3.922529))
                    .destination(markers.get(0))
                    .mode(TravelMode.DRIVING);}
            try {
                int i=1;
                DirectionsResult directionsResult= directionsApiRequest.await();
                DirectionsRoute[] routes= directionsResult.routes;
                for (DirectionsRoute route :
                        routes) {
                    Log.d("Route", route.copyrights+"-"+route.summary);
                    for (DirectionsLeg leg :
                            route.legs) {
                        Log.d("Leg ",i+"");
                        Log.d("Leg Start Address","----"+leg.startAddress);
                        Log.d("Leg Start Address","----"+leg.startLocation.toString());
                        Log.d("Leg End Address","----"+leg.endAddress);
                        Log.d("Leg End Address","----"+leg.endLocation.toString());
                        Log.d("Leg Distance","----"+leg.distance.humanReadable);
                        Log.d("Leg Duration","----"+leg.duration.humanReadable);
                        Log.d("Leg Steps","----"+leg.steps.length);
                        for (DirectionsStep step :
                                leg.steps) {
                            int c=1;
                            Log.d("Step "+c+"Start Address","----"+step.startLocation.toString());
                            Log.d("Step "+c+" End Address","----"+step.endLocation.toString());
                            Log.d("Step "+c+"Distance","----"+step.distance.humanReadable);
                            Log.d("Step "+c+" Duration","----"+step.duration.humanReadable);
                            c++;
                        }
                        i++;
                    }
                }
                publishProgress(directionsResult);
                return 1;
             } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

    private void moveCameraToCurrentLocation() {
        LatLng target = new LatLng(40.299157, -3.922529);
        if(!local)
        target = new LatLng(last_position.getLatitude(),last_position.getLongitude());
        jeff_position = googleMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_over))
                .position(target));
        googleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(target, zoom_level));
    }

    Marker marker;
    boolean isMoved=false;

    public void getCurrentLocation(){
        info_dialog.setContent("Obteniendo posición actual...");
        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(1);
        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context);
        Subscription subscription = locationProvider.getUpdatedLocation(request)
                .onBackpressureBuffer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Location>() {
                    @Override
                    public void onCompleted() {
                        Toast.makeText(context,"Error on position",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(context,"Error on position",Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Location location) {
                        last_position=location;
                        if(!isMoved){
                            start_location= new LatLng(location.getLatitude(),location.getLongitude());
//                            marker=   googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker())
//                                    .position(new LatLng(location.getLatitude(),location.getLongitude())));
//                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),location.getLongitude()),
//                                    (float) (googleMap.getMaxZoomLevel()*0.7)));
////                            JeffAplication.routeManager.init(googleMap,OrdersActivity.this,rutasAdapter.getPedidos(),last_position);
                          moveCameraToCurrentLocation();
                            isMoved=true;
                         new LoadRoute().execute(routeData);
                        }
                    }
                });
        }

    public void sendLocation(){
        Observable.interval(5, TimeUnit.SECONDS)
                .subscribe(aLong -> {

                });
    }


}
