package es.mrjeff.app.android.jeff.fragment.flujos.validar_pedido;

import android.support.v4.app.Fragment;

import me.panavtec.wizard.WizardPage;

/**
 * Created by CQ on 20/10/2016.
 */

public class ValidarPedidoW2 extends WizardPage<Fragment>{
    @Override
    public Fragment createFragment() {
        return ValidarPedidoFirmaFragment.newInstance("","");
    }
}
