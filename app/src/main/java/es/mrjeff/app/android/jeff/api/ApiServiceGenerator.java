package es.mrjeff.app.android.jeff.api;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Leo on 22/03/2016.
 */

public class ApiServiceGenerator {
    public static final String BASE_URL = "http://dev.backend.mrjeffapp.net/route-management-service/v1/";
    //    public static final String BASE_URL = "http://192.168.101.1:8081/race/";

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)

                    .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
    private static OkHttpClient httpClient =
            new OkHttpClient.Builder().connectTimeout(10000, TimeUnit.SECONDS)
    .readTimeout(10000,TimeUnit.SECONDS).build();

    public static <S> S createService(Class<S> serviceClass) {
        builder.client(httpClient);
        Retrofit retrofit = builder.build();

        return retrofit.create(serviceClass);
    }
}
