
package es.mrjeff.app.android.jeff.model.nuevo.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaxLine {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("rate_id")
    @Expose
    private String rateId;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("compound")
    @Expose
    private Boolean compound;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRateId() {
        return rateId;
    }

    public void setRateId(String rateId) {
        this.rateId = rateId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Boolean getCompound() {
        return compound;
    }

    public void setCompound(Boolean compound) {
        this.compound = compound;
    }

}
