package es.mrjeff.app.android.jeff.integration.legacy;

import android.util.Log;

import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.OrderApi;
import retrofit2.Call;

/**
 * Created by linovm on 21/10/15.
 */
public class SearchCouponService {



    public ResultSearchCoupon getCoupon(String coupon){
        ResultSearchCoupon result = null;

        try {
            Call<ResultSearchCoupon> call =ApiServiceGenerator.createService(OrderApi.class).getCoupon(coupon, JeffConstants.TOKEN_consumer_key,JeffConstants.TOKEN_consumer_secret);
                    ResultSearchCoupon response= call.execute().body();
            if (response!= null) {
                result = response;
            } else {
                Log.d("JSON", "Failed to download file");
            }
        } catch (Exception e) {
            Log.d("readJSONFeed", e.getLocalizedMessage());
        }

        return result;
    }


}
