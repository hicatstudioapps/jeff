package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.NuevoPedidoActivity;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.OrderApi;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.integration.OrderProductManager;
import es.mrjeff.app.android.jeff.integration.SuscriptionB2BManager;
import es.mrjeff.app.android.jeff.integration.legacy.ResultCoupon;
import es.mrjeff.app.android.jeff.model.nuevo.ChargeStripe;
import es.mrjeff.app.android.jeff.model.nuevo.order.Oder;
import es.mrjeff.app.android.jeff.model.nuevo.order.Order;
import es.mrjeff.app.android.jeff.util.DialogManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class NuevoPedidoPaid extends Fragment implements Step, Observer {

    public static final String PUBLIC_KEY_STRIPE_PRO = "pk_live_KZt8jaDgEIAKqCA0oGqYvWXH";
    //public static final String PUBLIC_KEY_STRIPE_PRO = "pk_test_dhOBW1MdFjP5txXGqUIJL5AT";
    public static final String PUBLIC_KEY_STRIPE_TEST = "pk_test_dhOBW1MdFjP5txXGqUIJL5AT";

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.card_number)
    EditText cardNumber;
    @BindView(R.id.expire)
    EditText expire;
    @BindView(R.id.cvc)
    EditText cvc;
    @BindView(R.id.textView29)
    TextView textView29;
    @BindView(R.id.finish)
    RelativeLayout finish;
    @BindView(R.id.textView34)
    TextView textView34;
    @BindView(R.id.icon_s)
    ImageView iconS;
    @BindView(R.id.icon_p)
    ImageView iconP;
    @BindView(R.id.stripe_container)
    LinearLayout stripeContainer;
    @BindView(R.id.imageView18)
    ImageView imageView18;
    @BindView(R.id.pay_pal_container)
    RelativeLayout payPalContainer;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Stripe stripe;
    private Dialog dialog;
    private NuevoPedidoActivity listener;
    private int type;
    private SuscriptionB2BManager suscriptionB2BManager;


    public NuevoPedidoPaid() {
        // Required empty public constructor
    }


    public static NuevoPedidoPaid newInstance(int param1) {
        NuevoPedidoPaid fragment = new NuevoPedidoPaid();
        Bundle args = new Bundle();
        args.putInt("type", param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           type= getArguments().getInt("type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nuevo_pedido_paid, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (NuevoPedidoActivity) context;
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        init();
    }

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId("AQSRU0WE-ANsrlZYy8lqHkKGApRW9qj1YquHW-NSkHS0uAo_TUDoCBpHNhHiHI8nU2vXh7gGFQ33M1GQ");

    private void init() {
        if(type==1){
            suscriptionB2BManager= SuscriptionB2BManager.getInstance(null);
        }
        //init stripe
        cardNumber.setTypeface(JeffAplication.rr);
        cvc.setTypeface(JeffAplication.rr);
        expire.setTypeface(JeffAplication.rr);
        textView34.setTypeface(JeffAplication.rb);
        try {
            Intent intent = new Intent(getContext(), PayPalService.class);
            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
            getContext().startService(intent);
            if (JeffAplication.testingLocal)
                stripe = new Stripe(getContext(),PUBLIC_KEY_STRIPE_TEST);
            else
                stripe = new Stripe(getContext(),PUBLIC_KEY_STRIPE_TEST);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        cvc.setOnEditorActionListener((textView, i, keyEvent) -> {
            if(i== EditorInfo.IME_ACTION_DONE)
                onClick();
            return true;
        });
        cardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        expire.addTextChangedListener(new ExpireWatcher());
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    private boolean validCard(String cardNumber) {
        Card card = new Card(
                cardNumber,
                12,
                2026,
                "123"
        );

        return card.validateCard();
    }

    private boolean validCvc(String cardNumber, String cvc) {
        Card card = new Card(
                cardNumber,
                12,
                2026,
                cvc
        );

        return card.validateCVC();
    }

    private boolean validDate(String cardNumber, String cvc, String date) {
        String[] split = date.split("/");
        if (split.length != 2 || split[1].length() != 2) {
            return false;
        }

        Integer moth = Integer.valueOf(split[0]);
        if (moth.intValue() < 1 || moth > 12) {
            return false;
        }

        Integer year = Integer.valueOf("20" + split[1]);

        Card card = new Card(
                cardNumber,
                moth,
                year,
                cvc
        );

        return card.validateCard();
    }

    @OnClick(R.id.finish)
    public void onClick() {
        String card = cardNumber.getText().toString().replace(" ", "");
        String[] part = expire.getText().toString().split("/");
        String expMon = "";
        String expY = "";
        if (part.length > 1) {
            expMon = part[0];
            expY = "20" + part[1];
        }
        String cv = cvc.getText().toString();
        if (JeffAplication.testingLocal) {
            paid(new Card(card, Integer.parseInt(expMon), Integer.parseInt(expY), cv));
            return;
        }
        if (!validCard(card)) {
            DialogManager.showInfo(getContext(), "El número de tarjeta introducido es inválido.").show();
            return;
        }
        if (!validCvc(card, cv)) {
            DialogManager.showInfo(getContext(), "CVC incorrecto.").show();
            return;
        }
        if (!validDate(card, cv, expire.getText().toString())) {
            DialogManager.showInfo(getContext(), "La fecha de expiración introducida es incorrecta. Debe ser en el formato MM/YY.").show();
            return;
        }
        paid(new Card(card, Integer.parseInt(expMon), Integer.parseInt(expY), cv));
    }

    private void paid(Card card) {

        dialog = DialogManager.getLoadingDialog(getContext(), "Realizando el pago...");
        dialog.show();
        try {
            stripe.createToken(
                    card,
                    new TokenCallback() {
                        public void onSuccess(final Token token) {
                            ChargeStripe stripe = getChargeStripe(token);
                            if (stripe != null) {
                                paidStripe(stripe);
                                Toast.makeText(getContext(), "Pagar con stripe", Toast.LENGTH_SHORT).show();
                            }
                        }

                        public void onError(Exception error) {
                            error.printStackTrace();
                            dialog.dismiss();
                            showError();
                        }
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void paidStripe(ChargeStripe stripe) {
        String couponCode = null;
        String discount = null;
        stripe.setHaveSuscription(0);
        stripe.setIdSuscription("0");
        stripe.setSuscripcion("0");
        Integer totalAmount = Integer.valueOf(stripe.getStripeAmount());
        stripe.setEmail(OrderManager.getFinalOrder().getCustomer().getEmail());
        OrderManager orderManager = OrderManager.getInstance(null);
        ResultCoupon resultCoupon = orderManager.getCoupon();
        //lo de los cupones
        if (resultCoupon != null ) {
            Float discF = resultCoupon.getDiscount() != null ? resultCoupon.getDiscount() * -100f : 0f;
            discount = String.valueOf(discF.intValue());
            totalAmount = totalAmount + Integer.valueOf(discF.intValue());
            stripe.setStripeAmount(totalAmount.toString());
            stripe.setDiscount(discount.toString());
        }
        Observable.create(subscriber -> {
            String url = "";
            if (JeffAplication.testingLocal) {
                url = "https://mrjeffapp.com/cgi-bin/subscripcionv2test.cgi";
            } else {
                url = "https://mrjeffapp.com/cgi-bin/subscripcionv2test.cgi";
            }
            OrderApi orderApi = ApiServiceGenerator.createService(OrderApi.class);
            String json = new Gson().toJson(stripe, ChargeStripe.class);
            Call<ResponseBody> call = orderApi.paidStripe(url, stripe);
            try {
                Response<ResponseBody> response = call.execute();
                String data = response.body().string();
                if (response.isSuccessful() && data.trim().toLowerCase().contains("succeeded")) {
                    subscriber.onNext(null);
                    subscriber.onCompleted();
                } else {
                    DialogManager.showInfo(getContext(), "No se ha podido relizar el cobro. Por favor vuelva a intentarlo").show();
                    subscriber.onCompleted();
                }
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        DialogManager.showInfo(getContext(), "Ha ocurrido un error enviando los datos para el pago. Por favor vuelva a intentarlo.").show();
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(Object o) {
                        updateOrder();
                    }
                });

    }

    private void updateOrder() {
        Oder oder = new Oder();
        Order order = new Order();
        order.setStatus("processing");
        oder.setOrder(order);
        ApiServiceGenerator.createService(OrderApi.class).updateOrder(OrderManager.getFinalOrder().getId() + "",
                JeffConstants.TOKEN_consumer_key, JeffConstants.TOKEN_consumer_secret, oder)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Oder>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        e.printStackTrace();
                        DialogManager.showInfo(getContext(), "Ha ocurrido un error enviado los datos para el pago. Por favor vuelva a intentarlo").show();
                    }

                    @Override
                    public void onNext(Oder oder) {
                        if (oder != null) {
                            dialog.dismiss();
                            OrderProductManager.getInstance(null).clear();
                            new MaterialDialog.Builder(getContext()).
                                    content("¡El pedido se ha creado con éxito! ¿Qué desea hacer?")
                                    .positiveText("Ir a rutas")
                                    .negativeText("Crear otro pedido")
                                    .onPositive((dialog1, which) -> {
                                        listener.gotoRoute();
                                    })
                                    .onNegative((dialog1, which) -> {
                                        listener.restart();
                                    })
                                    .cancelable(false)
                                    .build().show();
                        }
                    }
                });
    }

    private void showError() {
        DialogManager.showInfo(getContext(), "Ha ocurrido un error realizando el cobro.").show();
    }

    private ChargeStripe getChargeStripe(Token token) {
        ChargeStripe stripe = null;
        if (OrderManager.getFinalOrder() != null &&
                OrderManager.getFinalOrder().getTotal() != null) {
            try {
                BigDecimal totall= new BigDecimal(OrderManager.getFinalOrder().getTotal()).setScale(2,BigDecimal.ROUND_HALF_EVEN);
                if(type==1){
                    totall= totall.subtract(new BigDecimal(suscriptionB2BManager.getSuscriptionB2b().getDiscountNormalOrder()
                    ).setScale(2, RoundingMode.HALF_EVEN));
                }
                Float total = totall.floatValue();
                total = total * 100;
                stripe = new ChargeStripe();
                stripe.setStripeAmount(String.valueOf(total.intValue()));
                stripe.setStripeCurrency("eur");
                stripe.setStripeToken(token.getId());
                stripe.setStripeDescription("Compra efectuada por App Android. Pedido: " + OrderManager.getFinalOrder().getId());
            } catch (Exception e) {

            }
        }

        return stripe;
    }

    @Override
    public void update(java.util.Observable observable, Object o) {

    }

    @OnClick({R.id.icon_s, R.id.icon_p})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.icon_s:
                iconS.setImageResource(R.drawable.tarjeta_activado);
                iconP.setImageResource(R.drawable.paypal_desactivado);
                break;
            case R.id.icon_p:
                iconS.setImageResource(R.drawable.tarjeta_desactivado);
                iconP.setImageResource(R.drawable.paypal_activado);
                PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
                Intent intent = new Intent(getContext(), PaymentActivity.class);
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
                startActivityForResult(intent, 1);
                break;
        }
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        Order order = OrderManager.getFinalOrder();
        BigDecimal total= new BigDecimal(order.getTotal()).setScale(2,BigDecimal.ROUND_HALF_EVEN);
        if(type==1){
            total= total.subtract(new BigDecimal(suscriptionB2BManager.getSuscriptionB2b().getDiscountNormalOrder()
            ).setScale(2, RoundingMode.HALF_EVEN));
        }
        PayPalPayment payment = new PayPalPayment(total, "EUR", "MrJeff",
                paymentIntent);
        payment.custom("Compra efectuada por App Android. Pedido: " + order.getId());
        return payment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                dialog= DialogManager.getLoadingDialog(getContext(),"Actualizando su pedido...");
                dialog.show();
                updateOrder();
            } else if (resultCode == RESULT_CANCELED) {
                showError();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                showError();
            }
        } else {

        }
    }

    public static class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }

    public static class ExpireWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = '/';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Remove spacing char
            if (s.length() > 0 && (s.length() % 3) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 3) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 1) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }
        }
    }


}
