package es.mrjeff.app.android.jeff.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import es.mrjeff.app.android.jeff.view.components.MrJeffProduct;

/**
 * Created by CQ on 13/10/2016.
 */

public class ProductJefAdapter extends RecyclerView.Adapter<ProductJefAdapter.ViewHolder> {

    Context context;

    public ProductJefAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= new MrJeffProduct(context,null);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new ProductJefAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder  extends  RecyclerView.ViewHolder{

        View root;

        public ViewHolder(View itemView) {
            super(itemView);
            root=itemView;
        }
    }
}
