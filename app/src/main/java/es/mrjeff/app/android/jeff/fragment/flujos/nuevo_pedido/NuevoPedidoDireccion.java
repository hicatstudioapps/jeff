package es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.NuevoPedidoActivity;
import es.mrjeff.app.android.jeff.integration.CustomerManager;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.integration.PostalCodeManager;
import es.mrjeff.app.android.jeff.integration.SuscriptionB2BManager;
import es.mrjeff.app.android.jeff.model.nuevo.order.BillingAddress;
import es.mrjeff.app.android.jeff.model.nuevo.order.Oder;
import es.mrjeff.app.android.jeff.model.nuevo.order.ShippingAddress;
import es.mrjeff.app.android.jeff.util.DialogManager;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class NuevoPedidoDireccion extends Fragment implements Step {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.textView31)
    TextView textView;
    @BindView(R.id.bottom)
    RelativeLayout bottom;
    @BindView(R.id.edificio)
    EditText edificio;
    @BindView(R.id.piso)
    EditText piso;
    @BindView(R.id.nota)
    EditText nota;
    @BindView(R.id.edificio2)
    EditText edificio2;
    @BindView(R.id.piso2)
    EditText piso2;
    @BindView(R.id.imageView20)
    ImageView imageView20;
    @BindView(R.id.textView32)
    TextView textView32;
    @BindView(R.id.imageView21)
    ImageView imageView21;
    @BindView(R.id.textView33)
    TextView textView33;
    @BindView(R.id.imageView19)
    ImageView imageView19;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.mapView)
    MapView mMapView;
    private GoogleMap googleMap;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 2323;
    private static final int REQUEST_CODE_AUTOCOMPLETE2 = 2324;
    @BindView(R.id.direccion)
    EditText direccion;
    @BindView(R.id.direccion2)
    EditText direccion2;
    @BindView(R.id.direccion2_container)
    View direccion2_container;
    @BindView(R.id.ly_radio)
    View ly_radio;
    @BindView(R.id.textView10)
    TextView entrega_text;
    @BindView(R.id.textView12)
    TextView recogida_text;

    BillingAddress billingAddress = new BillingAddress();
    ShippingAddress shippingAddress = new ShippingAddress();
    private NuevoPedidoActivity listener;
    private int type;

    public NuevoPedidoDireccion() {
        // Required empty public constructor
    }

    public static NuevoPedidoDireccion newInstance(int param) {
        NuevoPedidoDireccion fragment = new NuevoPedidoDireccion();
        Bundle args = new Bundle();
        args.putInt("type", param);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       type= getArguments().getInt("type");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wizar_page3, container, false);
        ButterKnife.bind(this, view);
        mMapView.onCreate(savedInstanceState);
        return view;
    }

    void init() {
        entrega_text.setTypeface(JeffAplication.rb);
        recogida_text.setTypeface(JeffAplication.rb);
        direccion.setTypeface(JeffAplication.rr);
        direccion2.setTypeface(JeffAplication.rr);
        edificio2.setTypeface(JeffAplication.rr);
        edificio.setTypeface(JeffAplication.rr);
        piso.setTypeface(JeffAplication.rr);
        piso2.setTypeface(JeffAplication.rr);
        textView32.setTypeface(JeffAplication.rr);
        textView33.setTypeface(JeffAplication.rr);
        textView.setTypeface(JeffAplication.rb);
        PostalCodeManager.getInstance(getContext());
        billingAddress = CustomerManager.getInstance(getContext()).getCustomer().getCustomer().getBillingAddress();
        shippingAddress = CustomerManager.getInstance(getContext()).getCustomer().getCustomer().getShippingAddress();
        if (!JeffAplication.isJeff) {
            entrega_text.setVisibility(View.INVISIBLE);
            ly_radio.setVisibility(View.INVISIBLE);
            recogida_text.setText("Direccion de llegada");
        }
        mMapView.onResume(); // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(mMap -> {
            googleMap = mMap;
            // For showing a move to my location button
            //googleMap.setMyLocationEnabled(true);
            new Handler().postDelayed(() -> gotoLastCustomerLocation(),500);

        });

    }

    private void gotoLastCustomerLocation() {
        BillingAddress billingAddress= CustomerManager.getInstance(null).getCustomer().getCustomer().getBillingAddress();
        if(type==1){
            SuscriptionB2BManager suscriptionB2BManager= SuscriptionB2BManager.getInstance(null);
            NuevoPedidoDireccion.this.billingAddress.setPostcode(suscriptionB2BManager.getSuscriptionB2b().getPostalCode());
            shippingAddress.setPostcode(suscriptionB2BManager.getSuscriptionB2b().getPostalCode());
            NuevoPedidoDireccion.this.billingAddress.setCity(suscriptionB2BManager.getSuscriptionB2b().getCity());
            shippingAddress.setCity(suscriptionB2BManager.getSuscriptionB2b().getCity());
            NuevoPedidoDireccion.this.billingAddress.setAddress1(suscriptionB2BManager.getSuscriptionB2b().getAddress()+","+suscriptionB2BManager.getSuscriptionB2b().getNumber());
            shippingAddress.setAddress1(suscriptionB2BManager.getSuscriptionB2b().getAddress()+","+suscriptionB2BManager.getSuscriptionB2b().getNumber());
            //move to location
            searchAddress(withoutFloor(shippingAddress.getAddress1()) + ", " +
                    shippingAddress.getPostcode() + ", " +
                    shippingAddress.getCity());
            direccion.setText(suscriptionB2BManager.getSuscriptionB2b().getAddress());
            edificio.setText(suscriptionB2BManager.getSuscriptionB2b().getNumber());
            direccion.setOnClickListener(null);
            edificio.setEnabled(false);
            piso.setEnabled(false);
            ly_radio.setVisibility(View.INVISIBLE);
            entrega_text.setVisibility(View.INVISIBLE);
        }else if
        (billingAddress!= null){
            searchAddress(withoutFloor(billingAddress.getAddress1()) + ", " +
                    billingAddress.getPostcode() + ", " +
                    billingAddress.getCity());
            String[] part = billingAddress.getAddress1().split(",");
            direccion.setText(part.length > 0 ? part[0] : billingAddress.getAddress1());
            edificio.setText(part.length > 1 ? part[1] : "");
        }
    }

    public String withoutFloor(String addressName){
        String address = "";
        if(addressName != null){
            String [] split = addressName.split(",");
            if(split.length >=2) {
                address= split[0] + "," + split[1];
            } else{
                address = addressName;
            }
        }
        return  address;
    }

    public boolean searchAddress(String addressName) {
        boolean result = false;
        Geocoder geoCoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> address = geoCoder.getFromLocationName(addressName, 1);

            if (address.size() > 0) {
                result = true;
                Double lat = (double) (address.get(0).getLatitude());
                Double lon = (double) (address.get(0).getLongitude());

                final LatLng user = new LatLng(lat, lon);
                googleMap.clear();
//                MarkerOptions options = new MarkerOptions();
//                options.position(user);
//                options.draggable(true);
//                options.alpha(0f);
                //options.icon(BitmapDescriptorFactory.fromResource(R.drawable.mrjeff_geolocalization));
//                Marker hamburg = mMap.addMarker(options);
               // googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(user,16));
                //googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);

            }


        } catch (Exception e) {

        }

        return result;
    }

    @OnClick(R.id.direccion)
    public void launchSearch() {
               openAutocompleteActivity(REQUEST_CODE_AUTOCOMPLETE);
    }

    @OnClick(R.id.direccion2)
    public void launchSearch2() {
        openAutocompleteActivity(REQUEST_CODE_AUTOCOMPLETE2);
    }

    @OnClick(R.id.rb_misma)
    public void mismaClick() {
        imageView20.setImageDrawable(getResources().getDrawable(R.drawable.checked));
        imageView21.setImageDrawable(getResources().getDrawable(R.drawable.unchecked));
        direccion2_container.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.rb_otra)
    public void otraClick() {
        imageView21.setImageDrawable(getResources().getDrawable(R.drawable.checked));
        imageView20.setImageDrawable(getResources().getDrawable(R.drawable.unchecked));
        direccion2_container.setVisibility(View.VISIBLE);
    }

    private void openAutocompleteActivity(int code) {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setBoundsBias(new LatLngBounds(new LatLng(40.417160, -3.703561), new LatLng(40.417160, -3.703561)))
                    .setFilter(new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).setCountry("ES").build())
                    .build(getActivity());
            startActivityForResult(intent, code);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (resultCode == RESULT_OK) {
            // Get the user's selected place from the Intent.
            Place place = PlaceAutocomplete.getPlace(getActivity(), data);
            Log.i("Place Selected", "Place Selected: " + place.getName());
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                List<Address> list = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1);

                if (!list.isEmpty()) {
                    //validamos codigo postal
                    if (PostalCodeManager.getInstance(getContext()).getCity(list.get(0).getPostalCode()) == null) {
                        DialogManager.showInfo(getContext(), getString(R.string.help5TextTitle)).show();
                        return;
                    }
                    String addressName = list.get(0).getAddressLine(0);
                    String postalCode = list.get(0).getPostalCode();
                    String city = list.get(0).getLocality();
                    if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
                        direccion.setText(addressName.split(",")[0]);
                        edificio.setText("1");
                        if (direccion2_container.getVisibility()== View.INVISIBLE) {
                            direccion2.setText(addressName.split(",")[0]);
                            edificio2.setText("1");
                            shippingAddress.setCity(city);
                            shippingAddress.setPostcode(postalCode);
                        }
                        billingAddress.setPostcode(postalCode);
                        billingAddress.setCity(city);
                    } else {
                        edificio2.setText("1");
                        direccion2.setText(addressName.split(",")[0]);
                        shippingAddress.setCity(city);
                        shippingAddress.setPostcode(postalCode);
                    }
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(list.get(0).getLatitude(), list.get(0).getLongitude()), 16));
                    formatPlaceDetails(getResources(), place.getName(), place.getId(), place.getAddress(), place.getPhoneNumber(),
                            place.getWebsiteUri(), city, postalCode, addressName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(getActivity(), data);
            Log.e("error", "Error: Status = " + status.toString());
        } else if (resultCode == RESULT_CANCELED) {
            // Indicates that the activity closed before a selection was made. For example if
            // the user pressed the back button.
        }
    }

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri, String city, String code, String add) {
        Log.e("data", res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri, city, code, address));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri, city, code, address));

    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @OnClick(R.id.bottom)
    public void onClick() {
        // validamos si es la misma direccion
        //Validamos dierccion
        if (JeffAplication.testingLocal) {
            listener.nextPage();
            return;
        }
        if (TextUtils.isEmpty(direccion.getText().toString().trim())) {
            DialogManager.showInfo(getContext(), "El campo dirección de recogida es obligatorio").show();
            return;
        } else if (TextUtils.isEmpty(edificio.getText().toString().trim())) {
            new MaterialDialog.Builder(getContext())
                    .content(R.string.stree_number)
                    .positiveText(R.string.continue1)
                    .negativeText(R.string.cancel)
                    .onNegative((dialog, which) -> {
                        dialog.dismiss();
                    })
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            edificio.setText("S/N");
                        }
                    }).build().show();
            return;
        }
        if (direccion2_container.getVisibility()==View.VISIBLE) {
            if (TextUtils.isEmpty(direccion2.getText().toString().trim())) {
                DialogManager.showInfo(getContext(), "El campo dirección de entrega es obligatorio").show();
                return;
            } else if (TextUtils.isEmpty(edificio2.getText().toString().trim())) {
                new MaterialDialog.Builder(getContext())
                        .content(R.string.stree_number)
                        .positiveText(R.string.continue1)
                        .negativeText(R.string.cancel)
                        .onNegative((dialog, which) -> {
                            dialog.dismiss();
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                edificio.setText("S/N");
                            }
                        }).build().show();
                return;
            }
        }
        save();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (NuevoPedidoActivity) context;
    }

    private void save() {
       if(type != 1){
        billingAddress.setAddress1(direccion.getText().toString() + " ," + edificio.getText().toString());
        shippingAddress.setAddress1(direccion2.getText().toString() + " ," + edificio2.getText().toString());
        }
        OrderManager orderManager = OrderManager.getInstance(getContext());
        orderManager.getOrder().setBillingAddress(billingAddress);
        orderManager.getOrder().setShippingAddress(shippingAddress);
        orderManager.save();
        Oder oder = new Oder();
        oder.setOrder(OrderManager.getInstance(getContext()).getOrder());
        String actualJson = new Gson().toJson(oder, Oder.class);
        Log.d("Actual Json", "save: " + actualJson);
        listener.nextPage();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        init();
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
