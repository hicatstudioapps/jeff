package es.mrjeff.app.android.jeff.api;

import com.google.gson.reflect.TypeToken;

import java.util.List;

import es.mrjeff.app.android.jeff.model.nuevo.postal.PostalCode;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by admin on 06/02/2017.
 */

public interface PostalCodeApi {
    @GET("http://ec2-52-51-106-207.eu-west-1.compute.amazonaws.com/codigopostalv2.php")
    public Call<ResponseBody> getPostalCodes();
}
