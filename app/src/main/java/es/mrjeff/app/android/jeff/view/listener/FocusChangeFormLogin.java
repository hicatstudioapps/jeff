package es.mrjeff.app.android.jeff.view.listener;

import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;

import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.util.FormValidatorMrJeff;


/**
 * Created by linovm on 25/10/15.
 */
public class FocusChangeFormLogin implements View.OnFocusChangeListener {

    private final EditText editText;

    public FocusChangeFormLogin(EditText editText){
        this.editText = editText;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String text = ((EditText)v).getText().toString();
        if(!hasFocus){
            switch (editText.getId()){
                case R.id.user_email_login:
                    if(!FormValidatorMrJeff.isValidEmail(text)){
                        editText.setError(getHtmlError(R.string.errorformatmail));
                    } else {
                        editText.setError(null);
                    }
                    break;
                case R.id.user_pass_login:
                    if(!FormValidatorMrJeff.validRequiredMin(6, text)) {
                        editText.setError(getHtmlError(R.string.errorformatpassword));
                    } else {
                        editText.setError(null);
                    }
                    break;
            }
        }
    }

    private Spanned getHtmlError(int resource){
        String textError = editText.getContext().getResources().getString(resource);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(editText.getContext().getResources().getColor(R.color.redmrjeff));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(textError);
        ssbuilder.setSpan(fgcspan, 0, textError.length(), 0);

        return ssbuilder;

    }

}
