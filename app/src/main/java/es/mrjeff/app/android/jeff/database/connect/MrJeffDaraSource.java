package es.mrjeff.app.android.jeff.database.connect;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by linovm on 18/10/15.
 */
public class MrJeffDaraSource {

    protected MrJeffReaderDbHelper openHelper;
    protected SQLiteDatabase database;

    public MrJeffDaraSource(Context context) {
        //Creando una instancia hacia la base de datos
        openHelper = new MrJeffReaderDbHelper(context);
        database = openHelper.getWritableDatabase();
    }
}
