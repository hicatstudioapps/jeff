
package es.mrjeff.app.android.jeff.model.nuevo.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Product {

    @SerializedName("products")
    @Expose
    private List<Product_> product;

    public List<Product_> getProduct() {
        return product;
    }

    public void setProduct(List<Product_> product) {
        this.product = product;
    }

}
