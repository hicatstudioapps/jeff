package es.mrjeff.app.android.jeff.adapter;

import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.fragment.flujos.nuevo_pedido.NuevoPedidoCheckOut;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.nuevo.order.LineItem;

/**
 * Created by admin on 09/02/2017.
 */

public class CheckOutAdapter extends RecyclerView.Adapter<CheckOutAdapter.ViewHolder> {

    List<LineItem> lineItems;
    NuevoPedidoCheckOut listener;

    public CheckOutAdapter(List<LineItem> lineItems, NuevoPedidoCheckOut listener) {
        this.lineItems = lineItems;
        this.listener= listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.check_out_item, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.productName.setTypeface(JeffAplication.rb);
        holder.productPrice.setTypeface(JeffAplication.rb);
        holder.productQuantity.setTypeface(JeffAplication.rb);
        LineItem lineItem= lineItems.get(position);
        holder.productName.setText(lineItem.getName());
        DecimalFormat decimalFormat= new DecimalFormat("#0.00");
        holder.productPrice.setText(decimalFormat.format(Double.parseDouble(lineItem.getTotal())+Double.parseDouble(lineItem.getTotalTax()))+" €");
        holder.productQuantity.setText(""+lineItem.getQuantity());
        if(lineItem.getName().toLowerCase().contains("pedido")){
            holder.container.setBackgroundResource(R.drawable.pedido_minimo);
            holder.productPrice.setTextColor(Color.parseColor("#f44336"));
            holder.productName.setTextColor(Color.parseColor("#f44336"));
            holder.productQuantity.setBackgroundResource(R.drawable.circle_check_2);
            holder.trash.setVisibility(View.GONE);
        }else if(Float.parseFloat(lineItem.getTotal())<0){
            holder.container.setBackgroundResource(R.drawable.cupon_checkout_item);
            holder.productPrice.setTextColor(Color.parseColor("#FFC107"));
            holder.productName.setTextColor(Color.parseColor("#FFC107"));
            holder.productQuantity.setBackgroundResource(R.drawable.circle_check_3);
            holder.trash.setVisibility(View.VISIBLE);
        }
        else{
            if(lineItem.getName().toLowerCase().contains("pedido")){
                holder.container.setBackgroundResource(R.drawable.check_out_root_item);
                holder.productPrice.setTextColor(Color.parseColor("#3fa2e2"));
                holder.productName.setTextColor(Color.parseColor("#999999"));
                holder.productQuantity.setBackgroundResource(R.drawable.circle_check_2);
                holder.trash.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return lineItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.product_price)
        TextView productPrice;
        @BindView(R.id.product_quantity)
        TextView productQuantity;
        @BindView(R.id.container)
        View container;
        @BindView(R.id.trash)
        View trash;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            trash.setOnClickListener(view -> listener.deleteCoupon());
        }
    }
}
