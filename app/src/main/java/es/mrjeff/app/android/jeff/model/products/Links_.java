
package es.mrjeff.app.android.jeff.model.products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links_ {

    @SerializedName("self")
    @Expose
    private Self_ self;
    @SerializedName("productType")
    @Expose
    private ProductType__ productType;
    @SerializedName("products")
    @Expose
    private Products products;

    public Self_ getSelf() {
        return self;
    }

    public void setSelf(Self_ self) {
        this.self = self;
    }

    public ProductType__ getProductType() {
        return productType;
    }

    public void setProductType(ProductType__ productType) {
        this.productType = productType;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

}
