package es.mrjeff.app.android.jeff.integration;

import android.content.Context;
import android.content.Intent;

import com.annimon.stream.Collector;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.ProductApi;
import es.mrjeff.app.android.jeff.model.dao.DaoSession;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.dao.JeffCacheDao;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product;
import es.mrjeff.app.android.jeff.model.nuevo.product.Product_;
import okhttp3.ResponseBody;
import retrofit2.Call;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by admin on 30/01/2017.
 */

public class ProductManager extends GenericManager{

    HashMap<String, HashMap<Integer, Product_>> cache;

    public HashMap<String, HashMap<Integer, Product_>> getCache() {
        return cache;
    }

    public void setCache(HashMap<String, HashMap<Integer, Product_>> cache) {
        this.cache = cache;
    }

    private static ProductManager mInstance = null;
    private static Context mCtx;

    private ProductManager(Context context) {
        mCtx = context;
    }

    public static synchronized ProductManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ProductManager(context);
            mInstance.loadCache();
        } else
            mCtx = context;
        return mInstance;
    }

    public static Call<ResponseBody> getProduct(Observable loading) {
        Call<ResponseBody> data = ApiServiceGenerator.createService(ProductApi.class).getProducts();
        return data;
    }

    public void createCache(List<Product_> product_s) {
        HashMap<String, HashMap<Integer, Product_>> cacheTemp = new HashMap<>();
        for (Product_ product_ :
                product_s) {
            for (String keyProduct : product_.getTags()) {
                HashMap<Integer, Product_> productsKey = cacheTemp.get(keyProduct);
                if (productsKey == null) {
                    productsKey = new HashMap<>();
                }
                productsKey.put(product_.getId(), product_);
                cacheTemp.put(keyProduct, productsKey);
            }
        }
        cache = cacheTemp;
        save();
    }

    public List<Product_> getCatProduct(String cat){
        List<Product_> products= new LinkedList<>();
        HashMap<Integer,Product_> map= ProductManager.getInstance(null).getCache().get(cat);
        Iterator<Map.Entry<Integer, Product_>> it= map.entrySet().iterator();
        Stream.of(map.entrySet()).forEach(new Consumer() {
            @Override
            public void accept(Object o) {
                Map.Entry<Integer, Product_>entry = (Map.Entry<Integer, Product_>) o;
                products.add(entry.getValue());
            }
        });
        return products;
    }

    public void save() {
        JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
        JeffCache jeffCache = dao.load(1L);
        jeffCache.setProducts(new Gson().toJson(this, ProductManager.class));
        dao.updateInTx(jeffCache);
    }

    public void loadCache() {
        try {
            JeffCacheDao dao = JeffAplication.getDaoSession().getJeffCacheDao();
            JeffCache jeffCache = dao.load(1L);
            String json = jeffCache.getProducts();
            ProductManager offline = new Gson().fromJson(json, ProductManager.class);
            cache = offline.cache;
        } catch (Exception e) {
            e.printStackTrace();
            cache=null;
            return;
        }
    }
}
