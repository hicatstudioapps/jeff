package es.mrjeff.app.android.jeff.model.b2b;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 21/03/2017.
 */

public class SuscriptionB2b {

    @SerializedName("codigo")
    @Expose
    private String codigo;
    @SerializedName("residenceName")
    @Expose
    private String residenceName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("pickupDay")
    @Expose
    private String pickupDay;
    @SerializedName("pickupHour")
    @Expose
    private String pickupHour;
    @SerializedName("dropoffDay")
    @Expose
    private String dropoffDay;
    @SerializedName("dropoffHour")
    @Expose
    private String dropoffHour;
    @SerializedName("discountNormalOrder")
    @Expose
    private String discountNormalOrder;
    @SerializedName("needPayment")
    @Expose
    private String needPayment;
    @SerializedName("suscriptionDescription")
    @Expose
    private String suscriptionDescription;
    @SerializedName("coupon")
    @Expose
    private String coupon;

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @SerializedName("mensaje")
    @Expose
    private String mensaje;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getResidenceName() {
        return residenceName;
    }

    public void setResidenceName(String residenceName) {
        this.residenceName = residenceName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPickupDay() {
        return pickupDay;
    }

    public void setPickupDay(String pickupDay) {
        this.pickupDay = pickupDay;
    }

    public String getPickupHour() {
        return pickupHour;
    }

    public void setPickupHour(String pickupHour) {
        this.pickupHour = pickupHour;
    }

    public String getDropoffDay() {
        return dropoffDay;
    }

    public void setDropoffDay(String dropoffDay) {
        this.dropoffDay = dropoffDay;
    }

    public String getDropoffHour() {
        return dropoffHour;
    }

    public void setDropoffHour(String dropoffHour) {
        this.dropoffHour = dropoffHour;
    }

    public String getDiscountNormalOrder() {
        return discountNormalOrder;
    }

    public void setDiscountNormalOrder(String discountNormalOrder) {
        this.discountNormalOrder = discountNormalOrder;
    }

    public String getNeedPayment() {
        return needPayment;
    }

    public void setNeedPayment(String needPayment) {
        this.needPayment = needPayment;
    }

    public String getSuscriptionDescription() {
        return suscriptionDescription;
    }

    public void setSuscriptionDescription(String suscriptionDescription) {
        this.suscriptionDescription = suscriptionDescription;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

}
