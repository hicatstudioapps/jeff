package es.mrjeff.app.android.jeff.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.activity.ActivityDireccion;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.alarms.Sheduler;
import es.mrjeff.app.android.jeff.model.products.ProductInfo;
import es.mrjeff.app.android.jeff.model.products.ProductType;
import es.mrjeff.app.android.jeff.util.Utils;
import es.mrjeff.app.android.jeff.view.components.MrJeffProduct;
import me.panavtec.wizard.Wizard;
import me.panavtec.wizard.WizardListener;
import me.panavtec.wizard.WizardPage;
import me.panavtec.wizard.WizardPageListener;
import retrofit2.Call;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnadirFragment extends Fragment implements MrJeffProduct.MrProductActions,WizardPageListener, WizardListener {


    private Context context;
    private Wizard wizard;
    private MaterialDialog dialog;
    private Subscriber<? super ProductInfo> subcriber;

    public AnadirFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.product_container)
    LinearLayout product_container;
    @BindView(R.id.sumary_container)
    View sumary_container;
    @BindView(R.id.rb)
    RadioGroup radioGroup;
    LayoutInflater inflater;

    //Aqui guardamos el precio total a cobrar
    @BindView(R.id.total)
    TextView total_sumary;

    //listado de productos
    List<MrJeffProduct> productos= new ArrayList<>();
    Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflater= inflater;
        View layout= inflater.inflate(R.layout.fragment_anadir, container, false);
        ButterKnife.bind(this,layout);
        toolbar = (Toolbar) layout.findViewById(R.id.toolbar);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(view -> {
            ((PedidoDetalleActivity)getActivity()).mostrarAnadir();
        });
        initComponents();
        startLoad();
        return layout;
    }

    private void createSubscriber() {
        subcriber= new Subscriber<ProductInfo>() {
            @Override
            public void onCompleted() {
                subcriber.unsubscribe();
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
                MaterialDialog error_dialg = new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content("Ha ocurrido al cargar los productos.")
                        .positiveText("Reintentar")
                        .negativeText("Cancelar")
                        .onPositive((dialog1, which) -> {
                            subcriber.unsubscribe();
                            dialog1.dismiss();
                            startLoad();
                        }).build();
                error_dialg.show();
            }

            @Override
            public void onNext(ProductInfo productInfo) {
                loadProductInToView(productInfo);
                dialog.dismiss();
            }
        };
    }

    private void startLoad() {
        dialog.show();
        createSubscriber();
        Observable.create((Observable.OnSubscribe<ProductInfo>) subscriber -> {
            String autorization=context.getSharedPreferences("auth",MODE_PRIVATE).getString("data","");
            Call<ProductInfo> call = JeffAplication.apiCalls.getProduct(autorization);
            try {
                ProductInfo info = call.execute().body();
                if (info != null)
                    subscriber.onNext(info);
                else
                    subscriber.onError(null);
                subscriber.onCompleted();
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(subcriber);
    }


    @BindView(R.id.rb_nuevo_pedido)
    RadioButton rb_nuevo_pedido;;
    @BindView(R.id.rb_en_este_pedido)
    RadioButton rb_en_este_pedido;
    private void initComponents() {
        //si es de recogida, deshabilitamos nuevo_pedido y si es entrega deshabilitamos anadir_al_pedido
        if(((PedidoDetalleActivity)getActivity()).getPedido().getType().toLowerCase().equals("pickup")){
            rb_nuevo_pedido.setEnabled(false);
         }else
        rb_en_este_pedido.setEnabled(false);

        dialog = new MaterialDialog.Builder(getActivity())
                .cancelable(false)
                .content("Cargando productos...")
                .progress(true, 0)
                .build();


    }

    private void loadProductInToView(ProductInfo productInfo) {
        Stream.of(productInfo.getEmbedded().getProductTypes())
                .filter(value -> (value.getProducts()!=null && value.getProducts().size()>0))
                .map(new Function<ProductType, ProductType>() {
                    @Override
                    public ProductType apply(ProductType productType) {
                        MrJeffProduct p1= new MrJeffProduct(context,productType.getName(),productType.getProducts());
                        p1.setMrProductActions(AnadirFragment.this);
                        productos.add(p1);
                        product_container.addView(p1);
                        return productType;
                    }
                }).count();
//        for(int i=0;i < 3; i++){
//            MrJeffProduct p1=new MrJeffProduct(context);
//            p1.setMrProductActions(this);
//            productos.add(p1);
//            product_container.addView(p1);
//        }
        product_container.setEnabled(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    @OnCheckedChanged(R.id.rb_nuevo_pedido)
    public void check(RadioButton radio){
        if(radio.isChecked()){
        View view = inflater.inflate (R.layout.dialog_cambiar_direccion, null);
        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout ((int) (Utils.getScreenWidth(getActivity())*0.8), LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.BOTTOM);
        mBottomSheetDialog.show ();
            view.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBottomSheetDialog.dismiss();
                    product_container.setEnabled(false);
                }
            });
            view.findViewById(R.id.mismo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBottomSheetDialog.dismiss();
                    product_container.setEnabled(true);
                }
            });
            view.findViewById(R.id.horario).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivityForResult(new Intent(getContext(), ActivityDireccion.class),1);
                    mBottomSheetDialog.dismiss();
                }
            });
        }
    }

    @OnCheckedChanged(R.id.rb_en_este_pedido)
    public void anadir_al_pedido_check(RadioButton radio){
        if(radio.isChecked()){
            product_container.setEnabled(true);
        }
    }

    @Override
    public void guardarCambios(int totalImporte) {
       //aqui recogemos todo los cambios en los productos y actualizamos la vista y gestionamos las acciones necesarias
        //por ahora solo gestionamos el precio total a cobrar
        int precio=0;
        for (MrJeffProduct product :
                productos) {
            precio+=product.getTotalPrice();
        }
        total_sumary.setText(precio+" €");
        if(precio>0)
        if(sumary_container.getVisibility()!= View.VISIBLE)
            sumary_container.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.next)
    public void next(){
        MaterialDialog recogida_dialog= new MaterialDialog.Builder(context)
                .content("Has acabado de añadir productos? Empieza ahora el escaneo.")
                .negativeText("No")
                .positiveText("Si")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        int selected_id= radioGroup.getCheckedRadioButtonId();
                        if(selected_id== R.id.rb_en_este_pedido)
                            ((PedidoDetalleActivity)getActivity()).setEs_para_nuevo_pedido(false);
                        else
                            ((PedidoDetalleActivity)getActivity()).setEs_para_nuevo_pedido(true);
                        ((PedidoDetalleActivity)context).anadirAlPedidoWizard();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).build();

        recogida_dialog.show();
    }

    @Override
    public void onWizardFinished() {

    }

    @Override
    public void onPageChanged(int currentPageIndex, WizardPage page) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //recogemos el result de los cambios en direccion y horario
        if(resultCode== Activity.RESULT_OK && requestCode==1){
            Toast.makeText(getContext(),"Todo ok",Toast.LENGTH_SHORT).show();
            product_container.setEnabled(true);
        }
    }
}
