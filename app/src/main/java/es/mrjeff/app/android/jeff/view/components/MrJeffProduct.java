package es.mrjeff.app.android.jeff.view.components;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.model.products.Product;

/**
 * Created by CQ on 13/10/2016.
 */

public class MrJeffProduct extends LinearLayout {


    public void setMrProductActions(MrProductActions mrProductActions) {
        this.mrProductActions = mrProductActions;
    }

    //interfaz que controla las acciones sobre el componente y las comunica al contenedor
    MrProductActions mrProductActions;

    @BindView(R.id.arrow)
    View arrow;
    @BindView(R.id.product_container)
    LinearLayout product_container;
    @BindView(R.id.cointainer_parent)
    View container_parent;
    @BindView(R.id.category)
    TextView tv_category;


    List<MrJeffItemProduct> lista_productos= new ArrayList<>();

    public MrJeffProduct(Context context, String category, List<Product> products) {
        super(context);
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.mrjeff_product,this,true);
        ButterKnife.bind(this,view);
        tv_category.setText(category);
        Stream.of(products).map(new Function<Product, Product>() {
            @Override
            public Product apply(Product product) {
                MrJeffItemProduct item= new MrJeffItemProduct(context,product);
                lista_productos.add(item);
                product_container.addView(item);
                return null;
            }
        }).count();
    }

    public MrJeffProduct(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view= ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.mrjeff_product,this,true);
        ButterKnife.bind(this,view);
    }

    public int getTotalPrice(){
        int total=0;
        for (MrJeffItemProduct item :
                lista_productos) {
            total+= item.getPrecioTotal();
        }
        return total;
    }

    @OnClick(R.id.header)
    public void animOut(){

        if (container_parent.getVisibility()== View.VISIBLE)
            container_parent.setVisibility(GONE);
        else
            container_parent.setVisibility(VISIBLE);
    }

    @OnClick(R.id.button2)
    public void guardarCambios(){
        mrProductActions.guardarCambios(getTotalPrice());
    }

   public interface MrProductActions{
        void guardarCambios(int totalImporte);
    }
}
