package es.mrjeff.app.android.jeff.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CQ on 05/12/2016.
 */

public class JeffStageFinish {
    @SerializedName("jeffId")
    @Expose
    private String jeffId;

    public JeffStageFinish(String jeffId) {
        this.jeffId = jeffId;
    }

    /**
     *
     * @return
     * The jeffId
     */
    public String getJeffId() {
        return jeffId;
    }

    /**
     *
     * @param jeffId
     * The jeff_id
     */
    public void setJeffId(String jeffId) {
        this.jeffId = jeffId;
    }
}
