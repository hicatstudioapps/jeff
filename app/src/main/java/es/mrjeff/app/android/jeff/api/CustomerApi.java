package es.mrjeff.app.android.jeff.api;

import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.model.route.Customer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by admin on 26/01/2017.
 */

public interface CustomerApi {

    @POST("https://mrjeffapp.com/wc-api/v2/customers")
    public Call<ResponseBody> registerCustomer(@Query("consumer_key") String consumer_key, @Query("consumer_secret")String consumer_secret, @Body es.mrjeff.app.android.jeff.model.nuevo.customer.Customer customer);
    @GET("https://mrjeffapp.com/wc-api/v2/customers/email/{mail}")
    public Call<ResponseBody> findCustomer(@Path("mail") String mail, @Query("consumer_key") String consumer_key, @Query("consumer_secret")String consumer_secret);
}
