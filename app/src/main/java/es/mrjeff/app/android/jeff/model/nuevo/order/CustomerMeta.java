
package es.mrjeff.app.android.jeff.model.nuevo.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerMeta {

    @SerializedName("nickname")
    @Expose
    private String nickname;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("rich_editing")
    @Expose
    private String richEditing;
    @SerializedName("comment_shortcuts")
    @Expose
    private String commentShortcuts;
    @SerializedName("admin_color")
    @Expose
    private String adminColor;
    @SerializedName("use_ssl")
    @Expose
    private String useSsl;
    @SerializedName("show_admin_bar_front")
    @Expose
    private String showAdminBarFront;
    @SerializedName("wp_capabilities")
    @Expose
    private WpCapabilities wpCapabilities;
    @SerializedName("wp_user_level")
    @Expose
    private String wpUserLevel;
    @SerializedName("dismissed_wp_pointers")
    @Expose
    private String dismissedWpPointers;
    @SerializedName("billing_first_name")
    @Expose
    private String billingFirstName;
    @SerializedName("billing_last_name")
    @Expose
    private String billingLastName;
    @SerializedName("billing_email")
    @Expose
    private String billingEmail;
    @SerializedName("billing_phone")
    @Expose
    private String billingPhone;
    @SerializedName("billing_address_1")
    @Expose
    private String billingAddress1;
    @SerializedName("billing_address_2")
    @Expose
    private String billingAddress2;
    @SerializedName("billing_city")
    @Expose
    private String billingCity;
    @SerializedName("billing_state")
    @Expose
    private String billingState;
    @SerializedName("billing_postcode")
    @Expose
    private String billingPostcode;
    @SerializedName("billing_country")
    @Expose
    private String billingCountry;
    @SerializedName("shipping_first_name")
    @Expose
    private String shippingFirstName;
    @SerializedName("shipping_last_name")
    @Expose
    private String shippingLastName;
    @SerializedName("shipping_address_1")
    @Expose
    private String shippingAddress1;
    @SerializedName("shipping_address_2")
    @Expose
    private String shippingAddress2;
    @SerializedName("shipping_city")
    @Expose
    private String shippingCity;
    @SerializedName("shipping_state")
    @Expose
    private String shippingState;
    @SerializedName("shipping_postcode")
    @Expose
    private String shippingPostcode;
    @SerializedName("shipping_country")
    @Expose
    private String shippingCountry;
    @SerializedName("wp_nav_menu_recently_edited")
    @Expose
    private String wpNavMenuRecentlyEdited;
    @SerializedName("shipping_email")
    @Expose
    private String shippingEmail;
    @SerializedName("shipping_phone")
    @Expose
    private String shippingPhone;
    @SerializedName("wpseo_title")
    @Expose
    private String wpseoTitle;
    @SerializedName("wpseo_metadesc")
    @Expose
    private String wpseoMetadesc;
    @SerializedName("wpseo_metakey")
    @Expose
    private String wpseoMetakey;
    @SerializedName("wpseo_excludeauthorsitemap")
    @Expose
    private String wpseoExcludeauthorsitemap;
    @SerializedName("genesis_admin_menu")
    @Expose
    private String genesisAdminMenu;
    @SerializedName("genesis_seo_settings_menu")
    @Expose
    private String genesisSeoSettingsMenu;
    @SerializedName("genesis_import_export_menu")
    @Expose
    private String genesisImportExportMenu;
    @SerializedName("genesis_author_box_single")
    @Expose
    private String genesisAuthorBoxSingle;
    @SerializedName("genesis_author_box_archive")
    @Expose
    private String genesisAuthorBoxArchive;
    @SerializedName("headline")
    @Expose
    private String headline;
    @SerializedName("intro_text")
    @Expose
    private String introText;
    @SerializedName("doctitle")
    @Expose
    private String doctitle;
    @SerializedName("meta_description")
    @Expose
    private String metaDescription;
    @SerializedName("meta_keywords")
    @Expose
    private String metaKeywords;
    @SerializedName("noindex")
    @Expose
    private String noindex;
    @SerializedName("nofollow")
    @Expose
    private String nofollow;
    @SerializedName("noarchive")
    @Expose
    private String noarchive;
    @SerializedName("layout")
    @Expose
    private String layout;
    @SerializedName("billing_company")
    @Expose
    private String billingCompany;
    @SerializedName("shipping_company")
    @Expose
    private String shippingCompany;
    @SerializedName("googleplus")
    @Expose
    private String googleplus;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("paying_customer")
    @Expose
    private String payingCustomer;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRichEditing() {
        return richEditing;
    }

    public void setRichEditing(String richEditing) {
        this.richEditing = richEditing;
    }

    public String getCommentShortcuts() {
        return commentShortcuts;
    }

    public void setCommentShortcuts(String commentShortcuts) {
        this.commentShortcuts = commentShortcuts;
    }

    public String getAdminColor() {
        return adminColor;
    }

    public void setAdminColor(String adminColor) {
        this.adminColor = adminColor;
    }

    public String getUseSsl() {
        return useSsl;
    }

    public void setUseSsl(String useSsl) {
        this.useSsl = useSsl;
    }

    public String getShowAdminBarFront() {
        return showAdminBarFront;
    }

    public void setShowAdminBarFront(String showAdminBarFront) {
        this.showAdminBarFront = showAdminBarFront;
    }

    public WpCapabilities getWpCapabilities() {
        return wpCapabilities;
    }

    public void setWpCapabilities(WpCapabilities wpCapabilities) {
        this.wpCapabilities = wpCapabilities;
    }

    public String getWpUserLevel() {
        return wpUserLevel;
    }

    public void setWpUserLevel(String wpUserLevel) {
        this.wpUserLevel = wpUserLevel;
    }

    public String getDismissedWpPointers() {
        return dismissedWpPointers;
    }

    public void setDismissedWpPointers(String dismissedWpPointers) {
        this.dismissedWpPointers = dismissedWpPointers;
    }

    public String getBillingFirstName() {
        return billingFirstName;
    }

    public void setBillingFirstName(String billingFirstName) {
        this.billingFirstName = billingFirstName;
    }

    public String getBillingLastName() {
        return billingLastName;
    }

    public void setBillingLastName(String billingLastName) {
        this.billingLastName = billingLastName;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingPhone() {
        return billingPhone;
    }

    public void setBillingPhone(String billingPhone) {
        this.billingPhone = billingPhone;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getBillingPostcode() {
        return billingPostcode;
    }

    public void setBillingPostcode(String billingPostcode) {
        this.billingPostcode = billingPostcode;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getShippingFirstName() {
        return shippingFirstName;
    }

    public void setShippingFirstName(String shippingFirstName) {
        this.shippingFirstName = shippingFirstName;
    }

    public String getShippingLastName() {
        return shippingLastName;
    }

    public void setShippingLastName(String shippingLastName) {
        this.shippingLastName = shippingLastName;
    }

    public String getShippingAddress1() {
        return shippingAddress1;
    }

    public void setShippingAddress1(String shippingAddress1) {
        this.shippingAddress1 = shippingAddress1;
    }

    public String getShippingAddress2() {
        return shippingAddress2;
    }

    public void setShippingAddress2(String shippingAddress2) {
        this.shippingAddress2 = shippingAddress2;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public String getShippingPostcode() {
        return shippingPostcode;
    }

    public void setShippingPostcode(String shippingPostcode) {
        this.shippingPostcode = shippingPostcode;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getWpNavMenuRecentlyEdited() {
        return wpNavMenuRecentlyEdited;
    }

    public void setWpNavMenuRecentlyEdited(String wpNavMenuRecentlyEdited) {
        this.wpNavMenuRecentlyEdited = wpNavMenuRecentlyEdited;
    }

    public String getShippingEmail() {
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        this.shippingEmail = shippingEmail;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getWpseoTitle() {
        return wpseoTitle;
    }

    public void setWpseoTitle(String wpseoTitle) {
        this.wpseoTitle = wpseoTitle;
    }

    public String getWpseoMetadesc() {
        return wpseoMetadesc;
    }

    public void setWpseoMetadesc(String wpseoMetadesc) {
        this.wpseoMetadesc = wpseoMetadesc;
    }

    public String getWpseoMetakey() {
        return wpseoMetakey;
    }

    public void setWpseoMetakey(String wpseoMetakey) {
        this.wpseoMetakey = wpseoMetakey;
    }

    public String getWpseoExcludeauthorsitemap() {
        return wpseoExcludeauthorsitemap;
    }

    public void setWpseoExcludeauthorsitemap(String wpseoExcludeauthorsitemap) {
        this.wpseoExcludeauthorsitemap = wpseoExcludeauthorsitemap;
    }

    public String getGenesisAdminMenu() {
        return genesisAdminMenu;
    }

    public void setGenesisAdminMenu(String genesisAdminMenu) {
        this.genesisAdminMenu = genesisAdminMenu;
    }

    public String getGenesisSeoSettingsMenu() {
        return genesisSeoSettingsMenu;
    }

    public void setGenesisSeoSettingsMenu(String genesisSeoSettingsMenu) {
        this.genesisSeoSettingsMenu = genesisSeoSettingsMenu;
    }

    public String getGenesisImportExportMenu() {
        return genesisImportExportMenu;
    }

    public void setGenesisImportExportMenu(String genesisImportExportMenu) {
        this.genesisImportExportMenu = genesisImportExportMenu;
    }

    public String getGenesisAuthorBoxSingle() {
        return genesisAuthorBoxSingle;
    }

    public void setGenesisAuthorBoxSingle(String genesisAuthorBoxSingle) {
        this.genesisAuthorBoxSingle = genesisAuthorBoxSingle;
    }

    public String getGenesisAuthorBoxArchive() {
        return genesisAuthorBoxArchive;
    }

    public void setGenesisAuthorBoxArchive(String genesisAuthorBoxArchive) {
        this.genesisAuthorBoxArchive = genesisAuthorBoxArchive;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getIntroText() {
        return introText;
    }

    public void setIntroText(String introText) {
        this.introText = introText;
    }

    public String getDoctitle() {
        return doctitle;
    }

    public void setDoctitle(String doctitle) {
        this.doctitle = doctitle;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getNoindex() {
        return noindex;
    }

    public void setNoindex(String noindex) {
        this.noindex = noindex;
    }

    public String getNofollow() {
        return nofollow;
    }

    public void setNofollow(String nofollow) {
        this.nofollow = nofollow;
    }

    public String getNoarchive() {
        return noarchive;
    }

    public void setNoarchive(String noarchive) {
        this.noarchive = noarchive;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getBillingCompany() {
        return billingCompany;
    }

    public void setBillingCompany(String billingCompany) {
        this.billingCompany = billingCompany;
    }

    public String getShippingCompany() {
        return shippingCompany;
    }

    public void setShippingCompany(String shippingCompany) {
        this.shippingCompany = shippingCompany;
    }

    public String getGoogleplus() {
        return googleplus;
    }

    public void setGoogleplus(String googleplus) {
        this.googleplus = googleplus;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getPayingCustomer() {
        return payingCustomer;
    }

    public void setPayingCustomer(String payingCustomer) {
        this.payingCustomer = payingCustomer;
    }

}
