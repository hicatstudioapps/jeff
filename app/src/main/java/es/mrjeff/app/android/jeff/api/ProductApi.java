package es.mrjeff.app.android.jeff.api;

import es.mrjeff.app.android.jeff.model.nuevo.product.Product;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by admin on 30/01/2017.
 */

public interface ProductApi {

    @GET("https://mrjeffapp.com/wc-api/v1/products?consumer_key=ck_04ef92d3a3ff36ccd8427d24f289c47c&consumer_secret=cs_0b609d5cc4cd3d199cd3ba927b4b5ce7")
    public Call<ResponseBody> getProducts();

}
