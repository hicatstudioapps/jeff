package es.mrjeff.app.android.jeff.adapter.time_picker;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.mrjeff.app.android.jeff.R;

/**
 * Created by CQ on 10/11/2016.
 */

public class DateTimePickerHourAdapter extends RecyclerView.Adapter<DateTimePickerHourAdapter.Holder> {

    String[] data;
    Context context;
    int selected_position=0;
    Holder last_selected=null;
    SelectedHour listener;

    public String getValue() {
        return value;
    }

    String value;

    public DateTimePickerHourAdapter(String[] data, Context context,SelectedHour listener) {
        this.data = data;
        this.context = context;
        selected_position=0;
        this.listener=listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.time_picker_hour_item, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if(last_selected==null){
            last_selected=holder;
            listener.onHourSelected(data[position]);
        }
        if(position==selected_position){
            holder.root.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
            holder.hour.setTextColor(Color.WHITE);
        }else{
            holder.root.setBackgroundColor(Color.WHITE);
            holder.hour.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }
        holder.hour.setText(data[position]);
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.hour)
        TextView hour;
        View root;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            root=itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    root.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    hour.setTextColor(Color.WHITE);
                    last_selected.root.setBackgroundColor(Color.WHITE);
                    last_selected.hour.setTextColor(context.getResources().getColor(R.color.colorAccent));
                    last_selected=Holder.this;
                    listener.onHourSelected(data[getAdapterPosition()]);
                    selected_position=getAdapterPosition();
                    value= data[getAdapterPosition()];
                }
            });
        }
    }

    public void setData(String[] values) {
        data = values;
    }


    public void resetFlags(){
        last_selected=null;
        selected_position=0;
    }

    public interface SelectedHour{
        void onHourSelected(String data);
    }

}

