package es.mrjeff.app.android.jeff.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.activity.PedidoDetalleActivity;
import es.mrjeff.app.android.jeff.fragment.ProductoDetalleFragment;

/**
 * Created by CQ on 12/01/2017.
 */

public class PedidoDetalleAdapter extends RecyclerView.Adapter<PedidoDetalleAdapter.ViewHolder> {


    private final Context context;

    public PedidoDetalleAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(((LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.fragment_producto_detalle_item, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (((PedidoDetalleActivity) context).getPedido().getType().toLowerCase().equals("pickup")) {
            holder.btn_escanear.setVisibility(View.VISIBLE);
        } else
            holder.btn_escanear.setVisibility(View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btn_escanear)
        TextView btn_escanear;
        @BindView(R.id.producto)
        TextView producto;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.btn_escanear)
        public void click() {
            if (btn_escanear.getText().toString().equals("Escaneado")) {
                Toast.makeText(context, "Este producto ya ha sido escaneado", Toast.LENGTH_SHORT).show();
            } else {
                btn_escanear.setText("Escaneado");
                ((ProductoDetalleFragment.ScanerProdutAction) context).showScanner();
            }
        }
    }
}
