package es.mrjeff.app.android.jeff.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.afollestad.materialdialogs.MaterialDialog;

import es.mrjeff.app.android.jeff.JeffAplication;
import es.mrjeff.app.android.jeff.JeffConstants;
import es.mrjeff.app.android.jeff.R;
import es.mrjeff.app.android.jeff.alarms.AlarmUpdate;
import es.mrjeff.app.android.jeff.api.ApiServiceGenerator;
import es.mrjeff.app.android.jeff.api.OrderApi;
import es.mrjeff.app.android.jeff.integration.OrderManager;
import es.mrjeff.app.android.jeff.integration.OrderProductManager;
import es.mrjeff.app.android.jeff.model.dao.JeffCache;
import es.mrjeff.app.android.jeff.model.nuevo.order.Oder;
import es.mrjeff.app.android.jeff.model.nuevo.order.Order;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class MainActivity extends AppCompatActivity{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.preloader);
       // setAlarms();
      //  updateOrder();
        init();
    }


    private void init(){
        if(JeffAplication.getDaoSession().getJeffCacheDao().load(1L)== null){
            JeffCache jeffCache= new JeffCache();
            jeffCache.setId(1L);
            jeffCache.setOrder("order");
            jeffCache.setProducts("product");
            JeffAplication.getDaoSession().getJeffCacheDao().insertOrReplaceInTx(jeffCache);
        }
        Intent myIntent = new Intent(this,JeffAplication.loginResult != null ? OrdersActivity.class:LoggingActivity.class);
        startActivity(myIntent);
//        new MaterialDialog.Builder(this).content("Por favor seleccione el rol con que desea usar la app.")
//                .positiveText("Jeff")
//                .negativeText("Limpieza")
//                .onPositive((dialog, which) -> {
//                    Intent myIntent = new Intent(this,JeffAplication.loginResult != null ? OrdersActivity.class:LoggingActivity.class);
//                    startActivity(myIntent);
//                    finish();
//                })
//                .onNegative(((dialog, which) -> {
//                    JeffAplication.isJeff=false;
//                    Intent myIntent = new Intent(this,JeffAplication.loginResult != null ? OrdersActivity.class:LoggingActivity.class);
//                    startActivity(myIntent);
//                    finish();
//                })).
//                cancelable(false).build().show();

    }

    private void setAlarms(){
        Long timeInterval = 30000L;
        Intent intentBroad = new Intent(getApplicationContext(), AlarmUpdate.class);
        PendingIntent intent = PendingIntent.getBroadcast(getApplicationContext(),0,intentBroad,0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), timeInterval, intent);
    }


}
