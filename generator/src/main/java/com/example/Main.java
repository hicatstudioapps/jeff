package com.example;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class Main {

    public static void main(String [] arg){
        Schema schema= new Schema(1,"es.mrjeff.app.android.jeff.model.dao");
        schema.enableKeepSectionsByDefault();
        Entity entity= schema.addEntity("JeffCache");
        entity.addIdProperty();
        entity.addStringProperty("products");
        entity.addStringProperty("order");
        entity.addStringProperty("jeff_data");

        try {
            DaoGenerator daoGenerator= new DaoGenerator();
            daoGenerator.generateAll(schema,"./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
